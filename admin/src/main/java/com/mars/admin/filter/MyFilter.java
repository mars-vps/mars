package com.mars.admin.filter;

import com.mars.core.utils.Loggers;
import org.slf4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MyFilter implements Filter {

    protected final Logger logger = Loggers.adminLogger;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        Object admin = session.getAttribute("admin");
        String userName = "";
        boolean isFlag = false;
        if(admin != null){
            isFlag= true;
            /*userName = ((TAdmin)admin).getName();
            UserPermissionRepository userPermissionRepository = SpringContextUtils.getBean(UserPermissionRepository.class);
            UserPermission byUserName = userPermissionRepository.findByUserName(userName);
            String system_menu = byUserName.getSystem_menu();
            String[] split = system_menu.split(",");
            for(String str : split){
                if(str.contains(req.getRequestURI())){
                    logger.info("contains");
                }
            }*/
        }
        logger.info("MyFilter: " + req.getRequestURI());
        if(isFlag || userName.equalsIgnoreCase("admin") ||
                req.getRequestURI().equalsIgnoreCase("/admin/login") ||
                req.getRequestURI().equalsIgnoreCase("/admin/error") ||
                req.getRequestURI().equalsIgnoreCase("/admin/index")) {
            logger.info("admin: " + userName);
            chain.doFilter(request, response);//这步使得请求能够继续传导下去，如果没有的话，请求就在此结束
        }else{

            /*CustomSettings customSettings = SpringContextUtils.getBean(CustomSettings.class);
            String type = customSettings.getType();
            logger.info("error: " + type)1;*/
            
            // 进行截取
            String requestURI = req.getRequestURI();
            String[] split = requestURI.split("/");
            StringBuffer stringBuffer = new StringBuffer();
            for(int i=3; i<split.length;i++){
                    stringBuffer.append("../");
            }
            String error = stringBuffer.append("error").toString();
            logger.info("error: " + error);
            ((HttpServletResponse) response).sendRedirect("../admin/login");//跳转到信息提示页面！！
        }
    }

    @Override
    public void destroy() {

    }
}