package com.mars.admin.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
public class WebConfig {
    @Bean
    public FilterRegistrationBean<MyFilter> abcFilter() {
        FilterRegistrationBean<MyFilter> filterRegBean = new FilterRegistrationBean<>();
        filterRegBean.setFilter(new MyFilter());
        filterRegBean.addUrlPatterns("/admin/*");
        filterRegBean.addUrlPatterns("/lineInfo/*");
        filterRegBean.addUrlPatterns("/version/*");
        filterRegBean.addUrlPatterns("/notice/*");
        filterRegBean.addUrlPatterns("/code/*");
        filterRegBean.addUrlPatterns("/shop/*");
        filterRegBean.addUrlPatterns("/order/*");
        filterRegBean.addUrlPatterns("/user/*");
        filterRegBean.addUrlPatterns("/line/*");
        filterRegBean.addUrlPatterns("/promoCode/*");
        filterRegBean.setOrder(Ordered.LOWEST_PRECEDENCE -1);
        return filterRegBean;
    }
}