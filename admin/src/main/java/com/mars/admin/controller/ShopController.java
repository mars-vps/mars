package com.mars.admin.controller;

import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TShop;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.service.AdminService;
import com.mars.core.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller()
public class ShopController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private ShopService shopService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/shop/getShopList")
	public String getShopList(Model model, HttpSession httpSession) {
		model.addAttribute("data",shopService.getAllData());
		return "statistics/shop";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/shop/shopEdit")
	public String shopEdit(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("data",new TShop());
		List<JSONObject> ls = new ArrayList<>();
		for(VIPLevelEnum item:VIPLevelEnum.values()){
			JSONObject obj = new JSONObject();
			obj.put("id",item.getId());
			obj.put("name",item.getName());
			ls.add(obj);
		}
		model.addAttribute("vips",ls);
		if(id!=null && id>0){
			TShop shop = shopService.getShopById(id);
			if(shop!=null){
				model.addAttribute("data",shop);
			}
		}
		return "statistics/shopEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/shop/deleteShop")
	public String deleteShop(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			shopService.deleteCode(id);
		}
		return "redirect:getShopList";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/shop/shopEdit")
	public String shopEdit(Model model, TShop shop , HttpSession httpSession) {
		if(shop.getChargeReward()<=0 || shop.getRewardType()<=0 || shop.getRmb()<=0 || shop.getShopType()<=0){
			return "error";
		}
		if(shop.getId()==null){
			return "error";
		}
		if(shop.getShopDes()==null || shop.getShopDes().equals("")){
			return "error";
		}
		if(shop.getShopName()==null || shop.getShopName().equals("")){
			return "error";
		}
		System.out.println("-----------------------------------------"+shop);
		shopService.saveOrUpdate(shop);
		return "redirect:getShopList";
	}

}
