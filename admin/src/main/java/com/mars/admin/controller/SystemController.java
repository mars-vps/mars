package com.mars.admin.controller;

import com.mars.core.bean.TSystem;
import com.mars.core.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller()
public class SystemController {

	@Autowired
	private SystemService systemService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/version/getData")
	public String lines(Model model, HttpSession httpSession) {
		List<TSystem> allVersionData = systemService.getAllVersionData();
		model.addAttribute("datas",allVersionData);
		return "statistics/versionData";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/version/dataEdit")
	public String lineEditGet(Model model, Integer id, HttpSession httpSession) {
		model.addAttribute("data",new TSystem());
		if(id!=null && id>0){
                        TSystem dataById = systemService.getDataById(id);
                        if(dataById!=null){
				model.addAttribute("data",dataById);
			}
		}
		return "statistics/dataEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/version/deleteData")
	public String deleteLine(Model model, Integer id, HttpSession httpSession) {
		if(id!=null && id>0){
			systemService.deleteDataById(id);
		}
		return "redirect:getData";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/version/dataEdit")
	public String lineEditPost(Model model, TSystem info , HttpSession httpSession) {
		systemService.saveOrUpdate(info);
		return "redirect:getData";
	}


}
