package com.mars.admin.controller;

import com.mars.core.bean.TLine;
import com.mars.core.service.LineService;
import com.mars.core.utils.Base64;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller()
public class LineController {

	@Autowired
	private LineService lineService;
	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/line/improtLine")
	public String improtLine(String vmesses,String remark) {
		String[] vmessList = vmesses.split("vmess://");
		int i=0;
		for (String vmess:vmessList) {
			if(StringUtils.isEmpty(vmess)){
				continue;
			}
			i++;
			TLine line = UserController.getTLine(new String(Base64.decode(vmess)),i,remark);
			if(line!=null){
				lineService.saveOrUpdate(line);
			}
		}
		return "redirect:/lineInfo/lines";
	}


	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/line/reimportLine")
	public String reimportLine() {
		return "statistics/importLines";
	}



	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/line/delLines")
	public String delLines(String lines) {
		if(!StringUtils.isEmpty(lines)){
			String[] split = lines.split(",");
			for (String s:split) {
				lineService.deleteLine(Long.parseLong(s));
			}
		}
		return "statistics/importLines";
	}

}
