package com.mars.admin.controller;

import com.mars.core.bean.TAdmin;
import com.mars.core.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class AdminController {

	@Autowired
	private AdminService adminService;



	@ModelAttribute
	public void setCommonModelAttribute(HttpServletRequest request, Model model){

	}

	/**
	 * 登录跳转
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/login")
	public String loginGet(Model model, HttpSession httpSession) {
		httpSession.setAttribute("admin", null);
		return "login";
	}

	/**
	 * 登录跳转
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/loginOut")
	public String loginOut(Model model, HttpSession httpSession) {
		httpSession.setAttribute("admin", null);
		return "login";
	}

	/**
	 * 登录
	 * 
	 * @param admin
	 * @param model
	 * @param httpSession
	 * @return
	 */
	@PostMapping("/admin/login")
	public String loginPost(TAdmin admin, Model model, HttpSession httpSession) {
		TAdmin adminRes = adminService.findByNameAndPassword(admin);
		if (adminRes != null) {
			httpSession.setAttribute("admin", adminRes);
			return "redirect:index";
		} else {
			model.addAttribute("error", "用户名或密码错误，请重新登录！");
			return "login";
		}
	}

	@GetMapping("/admin/index")
	public String index(TAdmin admin, Model model, HttpServletRequest request) {
			return "back";
	}

	/**
	 * 注册
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/register")
	public String register(Model model) {
		return "register";
	}

	/**
	 * 仪表板页面
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/dashboard")
	public String dashboard(Model model) {
		return "dashboard";
	}

	/**
	 * 登录跳转
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/error")
	public String error(Model model) {
		return "error";
	}

	@GetMapping("/error")
	public String error2(Model model) {
		return "error";
	}

}
