package com.mars.admin.controller;

import com.mars.core.bean.TOrder;
import com.mars.core.service.AdminService;
import com.mars.core.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller()
public class OrderController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private OrderService orderService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/order/getOrderList")
	public String getCodeList(Model model, HttpSession httpSession, String orderId, String roleId, Integer platformUid, Integer state) {
		roleId = roleId==null?"":roleId;
		orderId = orderId==null?"":orderId;
		platformUid = platformUid==null?0:platformUid;
		state = state==null?-1:state;
		model.addAttribute("orderId",orderId);
		model.addAttribute("roleId",roleId);
		model.addAttribute("platformUid",platformUid);
		model.addAttribute("state",state);

		List<TOrder> orderList = new ArrayList<>();

		if(orderId!=null && !"".equals(orderId)){
			TOrder orderById = orderService.getOrderById(orderId);
			if(orderById!=null){
				orderList.add(orderById);
			}
		}
		else if(roleId!=null && !"".equals(roleId)){
			List<TOrder> orderById = orderService.getOrderByRoleId(roleId);
			if(orderById!=null){
				orderList.addAll(orderById);
			}
		}

		else if(platformUid==null || platformUid<=0){
			if(state==null || state<0){
				List<TOrder> allData = orderService.getAllData();
				if(allData!=null){
					orderList.addAll(allData);
				}
			}
			else{
				List<TOrder> allData = orderService.getOrderByState(state);
				if(allData!=null){
					orderList.addAll(allData);
				}
			}
		}
		else {
			String temp = "";
			switch (platformUid){
				case 1: temp = "ALIPAY_WEB";break;
				case 2: temp = "ALIPAY_WAP";break;
				case 3: temp = "WXPAY";break;
				default:break;
			}

			if(state==null || state<0){
				List<TOrder> allData = orderService.getOrderByPlatformUid(temp);
				if(allData!=null){
					orderList.addAll(allData);
				}
			}
			else{
				List<TOrder> allData = orderService.getOrderByPlatformUidAndState(temp,state);
				if(allData!=null){
					orderList.addAll(allData);
				}
			}
		}

		model.addAttribute("data",orderList);
		return "statistics/order";
	}

}
