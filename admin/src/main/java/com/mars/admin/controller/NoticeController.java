package com.mars.admin.controller;

import com.mars.core.bean.TNotice;
import com.mars.core.service.AdminService;
import com.mars.core.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller()
public class NoticeController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private NoticeService noticeService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/notice/getNoticeList")
	public String getNoticeList(Model model, HttpSession httpSession) {
		List<TNotice> allData = noticeService.getAllData();
		model.addAttribute("data",allData);
		return "statistics/notice";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/notice/noticeEdit")
	public String noticeEditGet(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("data",new TNotice());
		if(id!=null && id>0){
			TNotice line = noticeService.getTNoticeById(id);
			if(line!=null){
				model.addAttribute("data",line);
			}
		}
		return "statistics/noticeEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/notice/deleteNotice")
	public String deleteLine(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			noticeService.deleteLine(id);
		}
		return "redirect:getNoticeList";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/notice/noticeEdit")
	public String lineEditPost(Model model, TNotice line , HttpSession httpSession) {
		noticeService.saveOrUpdate(line);
		return "redirect:getNoticeList";
	}


}
