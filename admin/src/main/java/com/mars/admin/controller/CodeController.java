package com.mars.admin.controller;

import com.mars.core.bean.TCode;
import com.mars.core.service.AdminService;
import com.mars.core.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller()
public class CodeController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private CodeService codeService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/code/getCodeList")
	public String getCodeList(Model model, HttpSession httpSession, String code, Integer type, Integer status) {
		type = type==null?0:type;
		status = status==null?0:status;
		model.addAttribute("code",code);
		model.addAttribute("type",type);
		model.addAttribute("status",status);
		List<TCode> addData = new ArrayList<>();
		if(code!=null && !"".equalsIgnoreCase(code)){
			TCode codeByCode = codeService.getCodeByCode(code);
			if(codeByCode!=null){
				addData.add(codeByCode);
			}
		}
		else if(type==0 && status==0){
			addData = codeService.getAllData();
		}
		else if(type>0 && status >0){
			addData = codeService.getDataByTypeAndStatus(type,status);
		}
		else if(type==0 && status >0){
			addData = codeService.getDataByStatus(status);
		}else {
			addData = codeService.getDataByType(type);
		}
		model.addAttribute("data",addData);
		return "statistics/code";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/code/codeEdit")
	public String noticeEditGet(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("data",new TCode());
		if(id!=null && id>0){
			TCode line = codeService.getTCodeById(id);
			if(line!=null){
				model.addAttribute("data",line);
			}
		}
		return "statistics/codeEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/code/deleteCode")
	public String deleteCode(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			codeService.deleteCode(id);
		}
		return "redirect:getCodeList";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/code/codeEdit")
	public String noticeEdit(Model model, TCode code , HttpSession httpSession) {
		if(code.getCreateTime()<=0){
			code.setCreateTime(System.currentTimeMillis());
		}
		if(code.getCode()==null || "".equals(code.getCode())){
			String cc = createBigSmallLetterStrOrNumberRadom();
			TCode codeByCode = codeService.getCodeByCode(cc);
			while(codeByCode!=null){
				cc = createBigSmallLetterStrOrNumberRadom();
				codeByCode = codeService.getCodeByCode(cc);
			}
			code.setCode(cc);
		}
		codeService.saveOrUpdate(code);
		return "redirect:getCodeList";
	}

	public static String createBigSmallLetterStrOrNumberRadom() {
		int num = 8;
		String str = "";
		for(int i=0;i < num;i++){
			int intVal=(int)(Math.random()*58+65);
			if(intVal >= 91 && intVal <= 96){
				i--;
			}
			if(intVal < 91 || intVal > 96){
				if(intVal%2==0){
					str += (char)intVal;
				}else{
					str += (int)(Math.random()*10);
				}
			}
		}
		return str;
	}

}
