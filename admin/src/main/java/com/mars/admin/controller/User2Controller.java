package com.mars.admin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TLine;
import com.mars.core.bean.TUser;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.service.AdminService;
import com.mars.core.service.LineService;
import com.mars.core.service.TemplateService;
import com.mars.core.service.UserService;
import com.mars.core.template.LineTemplate;
import com.mars.core.utils.Base64;
import com.mars.core.utils.HttpRequest;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//@Controller()
public class User2Controller {

	@Autowired
	private AdminService adminService;
	@Autowired
	private LineService lineService;
	@Autowired
	private UserService userService;
	@Autowired
	private TemplateService templateService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/lines")
	public String lines(Model model, HttpSession httpSession) {
		List<TLine> allLines = lineService.getAllLines();
		model.addAttribute("lines",allLines);
		return "statistics/lines";
	}



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/lineEdit")
	public String lineEditGet(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("line",new TLine());
		List<JSONObject> ls = new ArrayList<>();
		for(VIPLevelEnum item:VIPLevelEnum.values()){
			JSONObject obj = new JSONObject();
			obj.put("id",item.getId());
			obj.put("name",item.getName());
			ls.add(obj);
		}
		model.addAttribute("vips",ls);
		if(id!=null && id>0){
			TLine line = lineService.findLineById(id);
			if(line!=null){
				model.addAttribute("line",line);
			}
		}
		return "statistics/lineEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/lineInfo/deleteLine")
	public String deleteLine(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			lineService.deleteLine(id);
		}
		return "redirect:lines";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/lineInfo/lineEdit")
	public String lineEditPost(Model model, TLine line , HttpSession httpSession) {
		lineService.saveOrUpdate(line);
		return "redirect:lines";
	}

	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/lineInfo/linkV2ray")
	public void linkV2ray(HttpServletResponse response) {
		try {
			lineService.v2rayList(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ;
	}

	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/user/getUsers")
	public String getUsers(Model model, @RequestParam(required = false,value = "userName") String userName,@RequestParam(required = false,value = "vipLv") Integer vipLv,@RequestParam(required = false,value = "userId") Integer userId) {
		model.addAttribute("userName",userName);
		model.addAttribute("vipLv",vipLv);
		model.addAttribute("userId",userId);
		if(vipLv == null){
			vipLv = -1;
		}
		if(userId!=null && userId>0){
			List<TUser> ls = new ArrayList<>();
			TUser userByUserId = userService.findUserByUserId(userId);
			if(userByUserId!=null){
				ls.add(userByUserId);
			}
			model.addAttribute("users",ls);
			return "statistics/users";
		}
		if(StringUtils.isEmpty(userName)){
			List<TUser> ls = userService.findUserByVipLv(vipLv);
			if(ls!=null && !ls.isEmpty()){
				model.addAttribute("users",ls);
			}
			else {
				model.addAttribute("users",new ArrayList<TUser>());
			}
			return "statistics/users";
		}
		else {
			List<TUser> ls = new ArrayList<>();
			List<TUser> result = userService.findUserByNameAndVipLv(userName,vipLv);
			if(result!=null && !result.isEmpty()){
				ls.addAll(result);
			}
			model.addAttribute("users",ls);
		}
		return "statistics/users";
	}





	@RequestMapping(value = "/lineInfo/upload", method = RequestMethod.POST)
	public String uploadFile(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
		//TODO dosomething
		StringBuffer sb = new StringBuffer();
		readToBuffer(sb,file.getInputStream());
		JSONArray jsonArray = JSONArray.parseArray(sb.toString());
		Map<Integer, LineTemplate> integerLineTemplateMap = templateService.initData(LineTemplate.class, jsonArray);
		for (LineTemplate item:integerLineTemplateMap.values()) {
			TLine line = new TLine();
			line.setName(item.getName());
			line.setHost(item.getHost());
			line.setPassword(item.getPassword());
			line.setType(item.getType());
			line.setLv(item.getLv());
			line.setStatus(item.getStatus());
			line.setEncrypt(item.getEncrypt());
			line.setPort(item.getPort());
			line.setRemark(item.getRemark());
			line.setWeight(item.getWeight());
			lineService.saveOrUpdate(line);
		}
		return "redirect:lines";
	}


	public static void readToBuffer(StringBuffer buffer, InputStream is) throws IOException {
		String line; // 用来保存每行读取的内容
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		line = reader.readLine(); // 读取第一行
		while (line != null) { // 如果 line 为空说明读完了
			buffer.append(line); // 将读到的内容添加到 buffer 中
			buffer.append("\n"); // 添加换行符
			line = reader.readLine(); // 读取下一行
		}
		reader.close();
		is.close();
	}


	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/importLInes")
	public String importLInes(Model model, HttpSession httpSession) {
		try {
			Map<Integer, LineTemplate> fileData = templateService.getFileData(LineTemplate.class, "lines.json");
			for (LineTemplate item:fileData.values()) {
				TLine line = new TLine();
				line.setName(item.getName());
				line.setHost(item.getHost());
				line.setPassword(item.getPassword());
				line.setType(item.getType());
				line.setLv(item.getLv());
				line.setStatus(item.getStatus());
				line.setEncrypt(item.getEncrypt());
				line.setPort(item.getPort());
				line.setRemark(item.getRemark());
				line.setWeight(item.getWeight());
				lineService.saveOrUpdate(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:lines";
	}


	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/updateLines")
	public String updateLines(Model model,@RequestParam(required = false)String urlLink, HttpSession httpSession) {
		if(StringUtils.isEmpty(urlLink)){
			urlLink = "https://marss.buzz/link/cWRE3MD9m9iLYCEf?sub=3";
		}
		List<TLine> ls = lineService.getLinesByRemark("JOY");
		lineService.deleteAllLine();
		if(ls != null && !ls.isEmpty()){
			for (TLine line:ls ) {
				lineService.saveOrUpdate(line);
			}
		}
		String result = HttpRequest.sendGet(urlLink, "");
		try {
			if(Base64.isBase64(result)){
				String vmesses = new String(Base64.decode(result));
				String[] vmessList = vmesses.split("vmess://");
				int i=0;
				for (String vmess:vmessList) {
					if(StringUtils.isEmpty(vmess)){
						continue;
					}
					i++;
					TLine line = getTLine(new String(Base64.decode(vmess)),i,"通过订阅地址批量更新线路");
					if(line!=null){
						lineService.saveOrUpdate(line);
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return "redirect:lines";
	}

	public static TLine getTLine(String vmess,int weight,String remark) {
		JSONObject jsonObject = JSONObject.parseObject(vmess);
		if(isImport(jsonObject)){
			TLine line = new TLine();
			String name = jsonObject.getString("ps");
			if(name.contains("香港")){
				name = "\uD83C\uDDED\uD83C\uDDF0" + name;
				weight = 1000 + weight;
			}
			else if(name.contains("台湾")){
				name = "\uD83C\uDDF9\uD83C\uDDFC" + name;
				weight = 2000 + weight;
			}
			else if(name.contains("日本")){
				name = "\uD83C\uDDEF\uD83C\uDDF5" + name;
				weight = 3000 + weight;
			}
			else if(name.contains("新加坡")){
				name = "\uD83C\uDDF8\uD83C\uDDEC" + name;
				weight = 4000 + weight;
			}
			else if(name.contains("韩国")){
				name = "\uD83C\uDDF0\uD83C\uDDF7" + name;
				weight = 5000 + weight;
			}
			else if(name.contains("法国")){
				name = "\uD83C\uDDEB\uD83C\uDDF7" + name;
				weight = 6000 + weight;
			}
			else if(name.contains("美国")){
				name = "\uD83C\uDDFA\uD83C\uDDF8" + name;
				weight = 7000 + weight;
			}
			else if(name.contains("俄罗斯")){
				name = "\uD83C\uDDF7\uD83C\uDDFA" + name;
				weight = 8000 + weight;
			}
			else if(name.contains("英国")){
				name = "\uD83C\uDDEC\uD83C\uDDE7" + name;
				weight = 9000 + weight;
			}
			if(name.contains(" | 琥珀酱")){
				name = name.replace(" | 琥珀酱","");
			}
			line.setName(name);
			line.setHost(jsonObject.getString("add"));
			line.setPassword(getPassword(jsonObject));
			line.setType(1);//0:ss   1:v2ray
			line.setLv(getLineLv(jsonObject));
			line.setStatus("1");//0:停用   1:启用
			line.setEncrypt("auto");//chacha20  aes-128-cfb  aes-256-cfb   auto   rc4-md5
			line.setPort(jsonObject.getIntValue("port"));
			line.setRemark(remark);
			line.setWeight(weight);
			return  line;
		}
		else {
			return null;
		}
	}

	private static boolean isImport(JSONObject jsonObject) {
		if(jsonObject==null || StringUtils.isEmpty(jsonObject.getString("ps"))){
			return false;
		}
		String ps = jsonObject.getString("ps").toUpperCase();
		if(ps.contains("VIP") || ps.contains("FREE"))
			return true;
		else
			return false;
	}

	private static String getLineLv(JSONObject jsonObject) {
		if(jsonObject==null || StringUtils.isEmpty(jsonObject.getString("ps"))){
			return null;
		}
		String ps = jsonObject.getString("ps").toUpperCase();
		if(ps.contains("FREE")){
			return "0";
		}
		else if(ps.contains("VIP1")){
			return "1";
		}
		else if(ps.contains("VIP2")){
			return "2";
		}
		else if(ps.contains("VIP3")){
			return "3";
		}
		return "0";
	}

	private static String getPassword(JSONObject jsonObject) {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(jsonObject.getString("id"))){
			sb.append(jsonObject.getString("id"));
		}
		sb.append("::");
		if( jsonObject.getIntValue("aid")>0){
			sb.append(jsonObject.getIntValue("aid"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("net"))){
			sb.append(jsonObject.getString("net"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("type"))){
			sb.append(jsonObject.getString("type"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("host"))){
			sb.append(jsonObject.getString("host"));
		}
		sb.append("::");
		if(!StringUtils.isEmpty(jsonObject.getString("path"))){
			sb.append(jsonObject.getString("path"));
		}
		sb.append("::");
		if(!StringUtils.isEmpty(jsonObject.getString("tls"))){
			sb.append(jsonObject.getString("tls"));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String code = "{\"v\":2,\"ps\":\"\uD83C\uDDF8\uD83C\uDDEC新加坡 亚马逊 | VIP1-A\",\"add\":\"gdyd.ambersauce.live\"," +
				"\"port\":22013,\"id\":\"92663cc2-ee98-37b1-90d6-eec1ad1631b9\",\"aid\":16,\"net\":\"ws\"," +
				"\"type\":\"none\",\"host\":\"\",\"path\":\"/ambersauce\",\"tls\":\"\"}\n";
		JSONObject obj = JSONObject.parseObject(code);
		System.out.println(getLineLv(obj));
	}

}
