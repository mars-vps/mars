package com.mars.admin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TLine;
import com.mars.core.bean.TPromoCode;
import com.mars.core.bean.TUser;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.service.*;
import com.mars.core.template.LineTemplate;
import com.mars.core.utils.Base64;
import com.mars.core.utils.HttpRequest;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller()
public class PromoCodeController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private PromoCodeService promoCodeService;




	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/promoCode/promoCode")
	public String promoCode(Model model, HttpSession httpSession,String code, Integer status) {
		List<TPromoCode> allData = promoCodeService.getAllData();
		model.addAttribute("data",allData);
		return "statistics/promoCode";
	}



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/promoCode/promoCodeEdit")
	public String promoCodeEdit(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("data",new TPromoCode());
		if(id!=null && id>0){
			TPromoCode line = promoCodeService.getTPromoCodeById(id);
			if(line!=null){
				model.addAttribute("data",line);
			}
		}
		return "statistics/promoCodeEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/promoCode/deletepromoCode")
	public String deleteLine(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			promoCodeService.deleteCode(id);
		}
		return "redirect:promoCode";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/promoCode/promoCodeEdit")
	public String lineEditPost(Model model, TPromoCode line , HttpSession httpSession) {
		promoCodeService.saveOrUpdate(line);
		return "redirect:promoCode";
	}



}
