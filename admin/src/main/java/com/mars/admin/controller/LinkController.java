package com.mars.admin.controller;

import com.mars.core.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller()
public class LinkController {

	@Autowired
	private LineService lineService;


	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/link/linkV2ray")
	public void linkV2ray(HttpServletResponse response) {
		try {
			lineService.v2rayList(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
