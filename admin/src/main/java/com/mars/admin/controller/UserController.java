package com.mars.admin.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TLine;
import com.mars.core.bean.TUser;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.service.AdminService;
import com.mars.core.service.LineService;
import com.mars.core.service.TemplateService;
import com.mars.core.service.UserService;
import com.mars.core.template.LineTemplate;
import com.mars.core.utils.Base64;
import com.mars.core.utils.HttpRequest;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller()
public class UserController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private LineService lineService;
	@Autowired
	private UserService userService;
	@Autowired
	private TemplateService templateService;



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/lines")
	public String lines(Model model, HttpSession httpSession) {
		List<TLine> allLines = lineService.getAllLines();
		model.addAttribute("lines",allLines);
		return "statistics/lines";
	}



	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/lineEdit")
	public String lineEditGet(Model model, Long id, HttpSession httpSession) {
		model.addAttribute("line",new TLine());
		List<JSONObject> ls = new ArrayList<>();
		for(VIPLevelEnum item:VIPLevelEnum.values()){
			JSONObject obj = new JSONObject();
			obj.put("id",item.getId());
			obj.put("name",item.getName());
			ls.add(obj);
		}
		model.addAttribute("vips",ls);
		if(id!=null && id>0){
			TLine line = lineService.findLineById(id);
			if(line!=null){
				model.addAttribute("line",line);
			}
		}
		return "statistics/lineEdit";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/lineInfo/deleteLine")
	public String deleteLine(Model model, Long id, HttpSession httpSession) {
		if(id!=null && id>0){
			lineService.deleteLine(id);
		}
		return "redirect:lines";
	}
	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@PostMapping("/lineInfo/lineEdit")
	public String lineEditPost(Model model, TLine line , HttpSession httpSession) {
		lineService.saveOrUpdate(line);
		return "redirect:lines";
	}

	/**
	 *
	 *
	 * @return
	 */
	@RequestMapping("/lineInfo/linkV2ray")
	public void linkV2ray(HttpServletResponse response) {
		try {
			lineService.v2rayList(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ;
	}

	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/user/getUsers")
	public String getUsers(Model model, @RequestParam(required = false,value = "userName") String userName,@RequestParam(required = false,value = "vipLv") Integer vipLv,@RequestParam(required = false,value = "userId") Integer userId) {
		model.addAttribute("userName",userName);
		model.addAttribute("vipLv",vipLv);
		model.addAttribute("userId",userId);
		if(vipLv == null){
			vipLv = -1;
		}
		if(userId!=null && userId>0){
			List<TUser> ls = new ArrayList<>();
			TUser userByUserId = userService.findUserByUserId(userId);
			if(userByUserId!=null){
				ls.add(userByUserId);
			}
			model.addAttribute("users",ls);
			return "statistics/users";
		}
		if(StringUtils.isEmpty(userName)){
			List<TUser> ls = userService.findUserByVipLv(vipLv);
			if(ls!=null && !ls.isEmpty()){
				model.addAttribute("users",ls);
			}
			else {
				model.addAttribute("users",new ArrayList<TUser>());
			}
			return "statistics/users";
		}
		else {
			List<TUser> ls = new ArrayList<>();
			List<TUser> result = userService.findUserByNameAndVipLv(userName,vipLv);
			if(result!=null && !result.isEmpty()){
				ls.addAll(result);
			}
			model.addAttribute("users",ls);
		}
		return "statistics/users";
	}





	@RequestMapping(value = "/lineInfo/upload", method = RequestMethod.POST)
	public String uploadFile(@RequestParam(value = "file", required = false) MultipartFile file) throws Exception {
		//TODO dosomething
		StringBuffer sb = new StringBuffer();
		readToBuffer(sb,file.getInputStream());
		JSONArray jsonArray = JSONArray.parseArray(sb.toString());
		Map<Integer, LineTemplate> integerLineTemplateMap = templateService.initData(LineTemplate.class, jsonArray);
		for (LineTemplate item:integerLineTemplateMap.values()) {
			TLine line = new TLine();
			line.setName(item.getName());
			line.setHost(item.getHost());
			line.setPassword(item.getPassword());
			line.setType(item.getType());
			line.setLv(item.getLv());
			line.setStatus(item.getStatus());
			line.setEncrypt(item.getEncrypt());
			line.setPort(item.getPort());
			line.setRemark(item.getRemark());
			line.setWeight(item.getWeight());
			lineService.saveOrUpdate(line);
		}
		return "redirect:lines";
	}


	public static void readToBuffer(StringBuffer buffer, InputStream is) throws IOException {
		String line; // 用来保存每行读取的内容
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		line = reader.readLine(); // 读取第一行
		while (line != null) { // 如果 line 为空说明读完了
			buffer.append(line); // 将读到的内容添加到 buffer 中
			buffer.append("\n"); // 添加换行符
			line = reader.readLine(); // 读取下一行
		}
		reader.close();
		is.close();
	}


	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/importLInes")
	public String importLInes(Model model, HttpSession httpSession) {
		try {
			Map<Integer, LineTemplate> fileData = templateService.getFileData(LineTemplate.class, "lines.json");
			for (LineTemplate item:fileData.values()) {
				TLine line = new TLine();
				line.setName(item.getName());
				line.setHost(item.getHost());
				line.setPassword(item.getPassword());
				line.setType(item.getType());
				line.setLv(item.getLv());
				line.setStatus(item.getStatus());
				line.setEncrypt(item.getEncrypt());
				line.setPort(item.getPort());
				line.setRemark(item.getRemark());
				line.setWeight(item.getWeight());
				lineService.saveOrUpdate(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:lines";
	}


	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/updateLines")
	public String updateLines(Model model,@RequestParam(required = false)String urlLink, HttpSession httpSession) {
		if(StringUtils.isEmpty(urlLink)){
			//urlLink = "https://marss.buzz/link/cWRE3MD9m9iLYCEf?sub=3";
			urlLink = "https://a.whales.monster/subLink";
		}
		List<TLine> ls = lineService.getLinesByRemark("networm");
		lineService.deleteAllLine();
		if(ls != null && !ls.isEmpty()){
			for (TLine line:ls ) {
				lineService.saveOrUpdate(line);
			}
		}
		String result = HttpRequest.sendGet(urlLink, "");
		try {
			if(Base64.isBase64(result)){
				String vmesses = new String(Base64.decode(result));
				String[] vmessList = vmesses.split("\n|\r");
				int i=1000;
				boolean isNormal = true;
				for (String temp:vmessList) {
					TLine line = null;
					if(temp.contains("vmess://")){
						line = getVmessLine(temp.substring(8),i,"whale");
					}else if(temp.contains("vless://")){
						line = getVlessLine(temp.substring(8),i,"whale");
					}
					else if(temp.contains("ss://")){
						String substring = temp.substring(5);
						line = getSSLine(substring,i,"whale");
					}
					i++;
					if(line!=null&&!line.getLv().equals("0")){
						isNormal = false;
					}
					if(line!=null){
						if(line.getLv().equals("0")){
							if(isNormal){
								lineService.saveOrUpdate(line);
							}
						}else {
							lineService.saveOrUpdate(line);
						}
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}

		/*urlLink = "https://lines.xwhales.cyou/link/8ScoJiTCE5Co2RHQ?sub=3";
		result = HttpRequest.sendGet(urlLink, "");
		try {
			if(Base64.isBase64(result)){
				String vmesses = new String(Base64.decode(result));
				String[] vmessList = vmesses.split("\n|\r");
				int i=0;
				for (String temp:vmessList) {
					TLine line = null;
					if(temp.contains("vmess://")){
						String s = new String(Base64.decode(temp.substring(8)));
						if(!s.contains("黄金")){
							continue;
						}
						line = getTLine(s,i,"通过订阅地址批量更新线路");//getVmessLine(temp.substring(8),i,"通过订阅地址批量更新线路");
					}
					else if(temp.contains("ss://")){
						String substring = temp.substring(5);
						line = getSSLine(substring,i,"通过订阅地址批量更新线路");
					}
					i++;
					if(line!=null){
						lineService.saveOrUpdate(line);
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}*/



		return "redirect:lines";
	}


	/**
	 *
	 *
	 * @param model
	 * @return
	 */
	@GetMapping("/lineInfo/updateLinesNetWorm")
	public String updateLinesNetWorm(Model model,@RequestParam(required = false)String urlLink, HttpSession httpSession) {
		List<TLine> ls = lineService.getLinesByRemark("whale");
		lineService.deleteAllLine();
		if(ls != null && !ls.isEmpty()){
			for (TLine line:ls ) {
				lineService.saveOrUpdate(line);
			}
		}
		try {
			//https://marss.buzz/api/v1/client/subscribe?token=155b973e72c4607098135ed4eb0e6833
			//dealLine("https://marss.buzz/api/v1/client/subscribe","token=155b973e72c4607098135ed4eb0e6833");
			//https://www.networm.workers.dev/api/v1/client/subscribe?token=4fd1a2210a6852d97f7b71af4939eabd
			//https://www.networm.workers.dev/api/v1/client/subscribe?token=5a51ab9b027e25b58b1fd6b3873ad09b
			//https://www.networm.workers.dev/api/v1/client/subscribe?token=52c132429d56476cb0e5ee740574b64c
			dealLine("https://www.networm.workers.dev/api/v1/client/subscribe","token=5a51ab9b027e25b58b1fd6b3873ad09b","1");
			dealLine("https://www.networm.workers.dev/api/v1/client/subscribe","token=52c132429d56476cb0e5ee740574b64c","2");
			dealLine("https://www.networm.workers.dev/api/v1/client/subscribe","token=4fd1a2210a6852d97f7b71af4939eabd","0");
		}catch (Exception e){
			e.printStackTrace();
		}
		return "redirect:lines";
	}

	private void dealLine(String urlLink,String urlParam,String lv){
		String result = HttpRequest.sendGet(urlLink, urlParam);
		if(Base64.isBase64(result)){
			String vmesses = new String(Base64.decode(result));
			String[] vmessList = vmesses.split("\n|\r");
			int i=0;
			boolean isNormal = true;
			for (String temp:vmessList) {
				if(StringUtils.isEmpty(temp)){
					continue;
				}
				TLine line = new TLine();
				if(temp.contains("vmess://")){
					String lineStr = new String(Base64.decode(temp.substring(8)));
					JSONObject jsonObject = JSONObject.parseObject(lineStr);
					String name = jsonObject.getString("ps");
					line.setName(name);
					line.setHost(jsonObject.getString("add"));
					line.setPassword(getPassword(jsonObject));
					line.setType(1);//0:ss   1:v2ray
					line.setLv(String.valueOf(getLineLvNew(name,lv)));
					line.setStatus("1");//0:停用   1:启用
					line.setEncrypt("auto");//chacha20  aes-128-cfb  aes-256-cfb   auto   rc4-md5
					line.setPort(jsonObject.getIntValue("port"));
					line.setRemark("networm");
					line.setWeight(i);
				}else if(temp.contains("vless://")){
					String UUID =temp.substring(8).split("\\@")[0];
					String subString = temp.substring(8).split("\\@")[1];
					String url = subString.split("\\?")[0];
					String ip = url.split("\\:")[0];
					String port = url.split("\\:")[1];
					String param = subString.split("\\?")[1];
					String canshu = param.split("\\#")[0];
					String name = param.split("\\#")[1];
					String[] split = canshu.split("\\&");
					JSONObject obj = new JSONObject();
					for(String s : split){
						String[] split1 = s.split("\\=");
						if(split1.length==1){
							obj.put(split1[0],"");
						}else{
							obj.put(split1[0],split1[1]);
						}
					}
					String password = UUID+"::"+obj.getString("flow")+"::tcp::none::"+obj.getString("host")+"::"+obj.getString("path")+"::"+obj.getString("security");
					line.setType(5);
					line.setEncrypt("none");
					line.setPort(Integer.parseInt(port));
					line.setPassword(password);
					line.setHost(ip);
					line.setName(name);
					line.setWeight(i);
					line.setRemark("networm");
					line.setLv(String.valueOf(getLineLvNew(name,lv)));
					line.setStatus("1");
				}
				else if(temp.contains("ss://")){
					String[] split = temp.substring(5).split("#");
					String key = new String(Base64.decode(split[0]));
					String[] split1 = key.split(":");
					line.setType(0);
					line.setEncrypt(split1[0]);
					line.setPort(Integer.parseInt(split1[2]));
					String[] split2 = split1[1].split("@");
					line.setPassword(split2[0]);
					line.setHost(split2[1]);
					line.setName(split[1]);
					line.setWeight(i);
					line.setRemark("networm");
					line.setLv(String.valueOf(getLineLvNew(split[1],lv)));
					line.setStatus("1");
				}
				i++;
				if(line!=null&&!line.getLv().equals("0")){
					isNormal = false;
				}
				if(line!=null){
					if(line.getLv().equals("0")){
						if(isNormal){
							lineService.saveOrUpdate(line);
						}
					}else {
						lineService.saveOrUpdate(line);
					}
				}
			}
		}
	}

	private int getLineLvNew(String name,String lv) {
		if(!StringUtils.isEmpty(lv)){
			return Integer.parseInt(lv);
		}
		if(name.contains("VIP1")){
			return 1;
		}else if(name.contains("VIP2")){
			return 2;
		}else if(name.contains("VIP3")){
			return 3;
		}else {
			return 0;
		}
	}


	public static TLine getTLine(String vmess,int weight,String remark) {

		JSONObject jsonObject = JSONObject.parseObject(vmess);

		String lineLv = getLineLv(jsonObject);
		if(lineLv.equals("-1")){
			return null;
		}
		//if(isImport(jsonObject)){
		TLine line = new TLine();
		String name = jsonObject.getString("ps");
		System.out.println(name);
			/*if(name.contains("香港")){
				name = "\uD83C\uDDED\uD83C\uDDF0" + name;
				weight = 1000 + weight;
			}
			else if(name.contains("台湾")){
				name = "\uD83C\uDDF9\uD83C\uDDFC" + name;
				weight = 2000 + weight;
			}
			else if(name.contains("日本")){
				name = "\uD83C\uDDEF\uD83C\uDDF5" + name;
				weight = 3000 + weight;
			}
			else if(name.contains("新加坡")){
				name = "\uD83C\uDDF8\uD83C\uDDEC" + name;
				weight = 4000 + weight;
			}
			else if(name.contains("韩国")){
				name = "\uD83C\uDDF0\uD83C\uDDF7" + name;
				weight = 5000 + weight;
			}
			else if(name.contains("法国")){
				name = "\uD83C\uDDEB\uD83C\uDDF7" + name;
				weight = 6000 + weight;
			}
			else if(name.contains("美国")){
				name = "\uD83C\uDDFA\uD83C\uDDF8" + name;
				weight = 7000 + weight;
			}
			else if(name.contains("俄罗斯")){
				name = "\uD83C\uDDF7\uD83C\uDDFA" + name;
				weight = 8000 + weight;
			}
			else if(name.contains("英国")){
				name = "\uD83C\uDDEC\uD83C\uDDE7" + name;
				weight = 9000 + weight;
			}*/

		if(lineLv.equals("0")){
			String substring = name.substring(5);
			String newName = substring.replaceAll("-", " | ");
			line.setName(newName + " | free");
		}else{
			String substring = name.substring(6);
			String newName = substring.replaceAll("-", " | ");
			line.setName(newName + " | VIP"+ lineLv);
		}
		line.setHost(jsonObject.getString("add"));
		line.setPassword(getPassword(jsonObject));
		line.setType(1);//0:ss   1:v2ray
		line.setLv(lineLv);
		line.setStatus("1");//0:停用   1:启用
		line.setEncrypt("auto");//chacha20  aes-128-cfb  aes-256-cfb   auto   rc4-md5
		line.setPort(jsonObject.getIntValue("port"));
		line.setRemark(remark);
		line.setWeight(weight);
		return  line;
		/*}
		else {
			return null;
		}*/
	}

	private static boolean isImport(JSONObject jsonObject) {
		if(jsonObject==null || StringUtils.isEmpty(jsonObject.getString("ps"))){
			return false;
		}
		String ps = jsonObject.getString("ps").toUpperCase();
		if(ps.contains("VIP") || ps.contains("FREE"))
			return true;
		else
			return false;
	}

	private static String getLineLv(JSONObject jsonObject) {
		if(jsonObject==null || StringUtils.isEmpty(jsonObject.getString("ps"))){
			return null;
		}
		String ps = jsonObject.getString("ps").toUpperCase();
		if(ps.contains("大众")){
			return "0";
		}
		else if(ps.contains("青铜")){
			return "1";
		}
		else if(ps.contains("白银")){
			return "2";
		}
		else if(ps.contains("黄金")){
			return "3";
		}
		return "-1";
	}

	private static String getPassword(JSONObject jsonObject) {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(jsonObject.getString("id"))){
			sb.append(jsonObject.getString("id"));
		}
		sb.append("::");
		if( jsonObject.getString("aid").length()>0){
			sb.append(jsonObject.getString("aid"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("net"))){
			sb.append(jsonObject.getString("net"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("type"))){
			sb.append(jsonObject.getString("type"));
		}
		sb.append("::");

		if(!StringUtils.isEmpty(jsonObject.getString("host"))){
			sb.append(jsonObject.getString("host"));
		}
		sb.append("::");
		if(!StringUtils.isEmpty(jsonObject.getString("path"))){
			sb.append(jsonObject.getString("path"));
		}
		sb.append("::");
		if(!StringUtils.isEmpty(jsonObject.getString("tls"))){
			sb.append(jsonObject.getString("tls"));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String urlLink = "https://a.whales.monster/subLink";
		String result = HttpRequest.sendGet(urlLink, "");
		try {
			if(Base64.isBase64(result)){
				String vmesses = new String(Base64.decode(result));
				String[] vmessList = vmesses.split("\n|\r");
				int i=0;
				for (String temp:vmessList) {
//					TLine line = null;
//					if(temp.contains("vmess://")){
//						line = getVmessLine(temp.substring(8),i,"通过订阅地址批量更新线路");
//					}else if(temp.contains("vless://")){
//						line = getVlessLine(temp.substring(8),i,"通过订阅地址批量更新线路");
//					}
//					else if(temp.contains("ss://")){
//						String substring = temp.substring(5);
//						line = getSSLine(substring,i,"通过订阅地址批量更新线路");
//					}
//					i++;
					if(temp.contains("vmess://")){
						TLine a = getVmessLine(temp.substring(8), i, "通过订阅地址批量更新线路");
						if(a.getName().equals("伦敦 | 线路 | VIP1")){
							System.out.println(a.getName());
						}
						System.out.println(a.getName());
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}

	}

	public static TLine getVmessLine(String vmess,int weight,String remark) {
		String s = new String(Base64.decode(vmess));
		return  getTLine(s,weight,remark);
	}

	public static TLine getSSLine(String ss,int weight,String remark) {
		try{
			//Y2hhY2hhMjA6eHdoYWxlcy4xMjNAbmMub294Yy5jYzoxMTcxMA==#白银VIP-香港-IPLC專線-游戏型
			//chacha20:xwhales.123@nc.ooxc.cc:11710
			String[] split = ss.split("#");
			String ssLineLv = getSSLineLv(split[1]);
			if(ssLineLv.equals("-1")){
				return null;
			}
			String key = new String(Base64.decode(split[0]));
			String[] split1 = key.split(":");
			TLine tLine = new TLine();
			tLine.setType(0);
			if(!split1[0].equals("chacha20")){
				return null;
			}
			tLine.setEncrypt(split1[0]);
			tLine.setPort(Integer.parseInt(split1[2]));
			String[] split2 = split1[1].split("@");
			tLine.setPassword(split2[0]);
			tLine.setHost(split2[1]);

			if(ssLineLv.equals("0")){
				String substring = split[1].substring(5);
				String newName = substring.replaceAll("-", " | ");
				tLine.setName(newName + " | free");
			}else{
				String substring = split[1].substring(6);
				String newName = substring.replaceAll("-", " | ");
				tLine.setName(newName + " | VIP"+ ssLineLv);
			}
			tLine.setWeight(weight);
			tLine.setRemark(remark);
			tLine.setLv(ssLineLv);
			tLine.setStatus("1");
			return tLine;
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public static TLine getVlessLine(String vless,int weight,String remark) {
		try{
			//UUID::flow::headerType::network::requestHost::path::streamSecurity
			//126d2012-ee8e-4c22-bb40-c8c164bd6b0a@
			// z-gz3.ooxc.cc:36441?security=xtls&flow=xtls-rprx-direct&path=/jj&host=a1.ooxc.cc#黄金VIP-新加坡-中转线路
			String UUID =vless.split("\\@")[0];
			String subString = vless.split("\\@")[1];
			String url = subString.split("\\?")[0];
			String ip = url.split("\\:")[0];
			String port = url.split("\\:")[1];
			String param = subString.split("\\?")[1];
			String canshu = param.split("\\#")[0];
			String name = param.split("\\#")[1];
			String lv = getSSLineLv(name);
			if(lv.equals("-1")){
				return null;
			}
			String[] split = canshu.split("\\&");
			JSONObject obj = new JSONObject();
			for(String s : split){
				String[] split1 = s.split("\\=");
				if(split1.length==1){
					obj.put(split1[0],"");
				}else{
					obj.put(split1[0],split1[1]);
				}
			}
			String substring = "";
			if(lv.equals("0")){
				substring = name.substring(5);
			}else{
				substring = name.substring(6);
			}
			String newName = substring.replaceAll("-", " | ");
			String password = UUID+"::"+obj.getString("flow")+"::tcp::none::"+obj.getString("host")+"::"+obj.getString("path")+"::"+obj.getString("security");

			TLine tLine = new TLine();
			tLine.setType(5);
			tLine.setEncrypt("none");
			tLine.setPort(Integer.parseInt(port));
			tLine.setPassword(password);
			tLine.setHost(ip);
			if(lv.equals("0")){
				tLine.setName(newName + " | free");
			}else{
				tLine.setName(newName + " | VIP"+ lv);
			}
			tLine.setWeight(weight);
			tLine.setRemark(remark);
			tLine.setLv(lv);
			tLine.setStatus("1");
			return tLine;
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	private static String getSSLineLv(String s) {
		if(s.contains("青铜")){
			return "1";
		}
		else if(s.contains("白银")){
			return "2";
		}
		else if(s.contains("黄金")){
			return "3";
		}
		else if(s.contains("大众")){
			return "0";
		}
		else {
			return "-1";
		}
	}

}