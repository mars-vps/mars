package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TLine;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_line")
@Qualifier("lineDao")
public interface TLineDao extends BaseEnityCrud<TLine,Long> {

    @Query("select max(id) from TLine where id <> null")
    Long getMaxId();
    @Query("from TLine where id <> null and id = :lineId")
    TLine getLineById(@Param("lineId") long lineId);
    @Query("from TLine order by lv asc,weight")
    List<TLine> getLines();
    @Query("from TLine where id <> null order by lv asc,weight")
    List<TLine> getAllLine();
    @Query("from TLine where id <> null and remark like %:key% order by lv asc,weight")
    List<TLine> getLinesByRemark(String key);
}