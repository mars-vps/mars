package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TShop;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_shop")
@Qualifier("shopDao")
public interface TShopDao extends BaseEnityCrud<TShop,Long> {

    @Query("from TShop where status=:status order by shopType,rmb asc")
    List<TShop> getAllShops(@Param("status") int status);

    @Query("from TShop where id=:id")
    TShop getShopById(@Param("id") long id);

    @Query("from TShop")
    List<TShop> getAllData();
}