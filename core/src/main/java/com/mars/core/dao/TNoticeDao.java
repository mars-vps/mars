package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TNotice;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_line")
@Qualifier("lineDao")
public interface TNoticeDao extends BaseEnityCrud<TNotice,Long> {

    @Query("select max(id) from TNotice where id <> null")
    Long getMaxId();
    @Query("from TNotice where id <> null and id = :id")
    TNotice getTNoticeById(@Param("id") long id);
    @Query("from TNotice")
    List<TNotice> getAllData();
}