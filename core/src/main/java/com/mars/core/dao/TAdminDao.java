package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TAdmin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

@Repository
@Table(name="t_admin")
@Qualifier("adminDao")
public interface TAdminDao extends BaseEnityCrud<TAdmin,Long> {

    @Query("from TAdmin where id <> null and name=:name and password=:password")
    TAdmin findByNameAndPassword(@Param("name") String name, @Param("password") String password);

}