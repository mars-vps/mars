package com.mars.core.dao;


import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TOrder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_order")
@Qualifier("orderDao")
public interface TOrderDao extends BaseEnityCrud<TOrder,String> {


    @Query("from TOrder where roleId=:roleId order by createTime desc")
    List<TOrder> findByRoleId(@Param("roleId") long roleId);

    @Query("from TOrder where id=:id")
    TOrder getOrderById(@Param("id") String id);

    @Query("from TOrder order by createTime desc")
    List<TOrder> getAllData();

    @Query("from TOrder where platformUid=:platformUid order by createTime desc")
    List<TOrder> getOrderByPlatformUid(@Param("platformUid") String platformUid);

    @Query("from TOrder where state=:state order by createTime desc")
    List<TOrder> getOrderByState(@Param("state") int state);

    @Query("from TOrder where state=:state and platformUid=:platformUid order by createTime desc")
    List<TOrder> getOrderByPlatformUidAndState(@Param("platformUid") String platformUid,@Param("state") int state);
    @Query("from TOrder where roleId=:roleId")
    List<TOrder> getOrderByRoleId(@Param("roleId")long roleId);
    @Query("from TOrder where platformUid='alipay' and state=0")
    List<TOrder> getOrders();
}