package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TCode;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_code")
@Qualifier("codeDao")
public interface TCodeDao extends BaseEnityCrud<TCode,Long> {

    @Query("from TCode where status=:status")
    List<TCode> getCodeByStatus(@Param("status") int status);

    @Query("from TCode where type=:type")
    List<TCode> getCodeByType(@Param("type") int type);


    @Query("from TCode where type=:type and status=:status")
    List<TCode> getCodeByStatusAndType(@Param("type") int type, @Param("status") int status);


    @Query("from TCode where code=:code")
    TCode getCodeByCode(@Param("code") String code);

    @Query("from TCode where userId=:userId")
    List<TCode> getCodeByUserId(@Param("userId") long userId);

    @Query("from TCode")
    List<TCode> getAllData();
    @Query("from TCode where id = :id")
    TCode getTCodeById(@Param("id")Long id);
}