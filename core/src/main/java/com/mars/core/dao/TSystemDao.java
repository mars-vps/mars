package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TSystem;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_system")
@Qualifier("systemDao")
public interface TSystemDao extends BaseEnityCrud<TSystem,Integer> {
    @Query("from TSystem where id =(select max(id) from TSystem where id <> null)")
    TSystem getVersionData();
    @Query("select max(id) from TSystem where id <> null")
    Integer getMaxId();
    @Query("from TSystem where id <> null and id = :id")
    TSystem getDataById(@Param("id") int id);
    @Query("from TSystem  where id <> null")
    List<TSystem> getAllVersionData();
}