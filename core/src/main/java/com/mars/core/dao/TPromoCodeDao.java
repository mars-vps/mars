package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TCode;
import com.mars.core.bean.TPromoCode;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_promo_code")
@Qualifier("promoCodeDao")
public interface TPromoCodeDao extends BaseEnityCrud<TPromoCode,Long> {

    @Query("from TPromoCode where status=:status")
    List<TPromoCode> getPromoCodeByStatus(@Param("status") int status);

    @Query("from TPromoCode where promoCode=:code")
    TPromoCode getPromoCodeByCode(@Param("code") String code);


    @Query("from TPromoCode")
    List<TPromoCode> getAllData();

    @Query("from TPromoCode where id = :id")
    TPromoCode getTPromoCodeById(@Param("id")Long id);
}