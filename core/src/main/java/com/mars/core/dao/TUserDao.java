package com.mars.core.dao;

import com.mars.core.bean.BaseEnityCrud;
import com.mars.core.bean.TUser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;
import java.util.List;

@Repository
@Table(name="t_user")
@Qualifier("userDao")
public interface TUserDao extends BaseEnityCrud<TUser,Long> {

    @Query("select max(id) from TUser where id <> null")
    Long getMaxId();

    @Query("from TUser where username =:username  ")
    TUser getTUserByName(@Param("username") String username);

    @Query("from TUser where id = :id")
    TUser getTUserById(@Param("id") long id);

    @Query("from TUser where inviteCode = :inviteCode")
    TUser getTUserByInviteCode(@Param("inviteCode") String inviteCode);

    @Query("from TUser where vipLv = :vipLv order by id desc ")
    List<TUser> findUserByVipLv(@Param("vipLv")int vipLv);

    @Query("from TUser order by id desc ")
    List<TUser> getAllUser();
    @Query("from TUser where username like CONCAT('%',CONCAT(:userName,'%'))")
    List<TUser> findUserByNameLike(@Param("userName")String userName);

    @Query("from TUser where username like CONCAT('%',CONCAT(:userName,'%')) and vipLv=:vipLv")
    List<TUser> findUserByNameAndVipLv(@Param("userName")String userName, @Param("vipLv")int vipLv);
}