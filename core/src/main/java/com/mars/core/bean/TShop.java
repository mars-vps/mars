package com.mars.core.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "t_shop")
public class TShop implements BaseEntity<Long> {

    public static final int STATE_ONLINE = 0;
    public static final int STATE_OFFLINE = 1;

    @Id
    private long id;
    /** 创建时间 */
    @Column(name = "shopName",nullable = false)
    private String shopName;
    @Column(name = "shopDes",nullable = false)
    private String shopDes;
    @Column(name = "shopType",nullable = false)
    private int shopType;
    @Column(name = "rmb",nullable = false)
    private float rmb;
    @Column(name = "chargeReward",nullable = false)
    private int chargeReward;
    @Column(name = "rewardType",nullable = false)
    private int rewardType;
    @Column(name = "status")
    private int status;



    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TShop";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }
}
