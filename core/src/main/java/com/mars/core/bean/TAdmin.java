package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "t_admin")
public class TAdmin implements BaseEntity<Long> {
    /** 1id */
    @Id
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TLine";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }

    public String toJson(){
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        obj.put("name",name);
        obj.put("password",password);
        return obj.toString();
    }
}
