package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "t_system")
public class TSystem implements BaseEntity<Integer> {


    @Id
    private int id;
    @Column(name = "versionCode")
    private int versionCode;
    @Column(name = "versionName")
    private String versionName;
    @Column(name = "urlString")
    private String urlString;
    @Column(name = "content")
    private String content;
    @Column(name = "title")
    private String title;
    @Column(name = "md5String")
    private String md5String;
    @Column(name = "forceString")
    private int forceString;

    @Column(name = "inviteReward")
    private String inviteReward;
    @Column(name = "adsReward")
    private String adsReward;
    @Column(name = "shareUrl")
    private String shareUrl;

    @Column(name = "inviteTimes",columnDefinition = "int(11) default 0")
    private int inviteTimes;
    @Column(name = "adsTimes",columnDefinition = "int(11) default 0")
    private int adsTimes;

    @Column(name = "bucha")
    private String bucha;


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TSystem";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }

    public String toJson(){
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        obj.put("versionCode",versionCode);
        obj.put("versionName",versionName==null?"":versionName);
        obj.put("url",urlString==null?"":urlString);
        obj.put("content",content==null?"":content);
        obj.put("title",title==null?"":title);
        obj.put("md5",md5String==null?"":md5String);
        obj.put("force",forceString);
        obj.put("bucha",bucha==null?"":bucha);
        return obj.toString();
    }

}
