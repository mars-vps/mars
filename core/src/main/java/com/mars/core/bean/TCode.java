package com.mars.core.bean;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "t_code",indexes = {@Index(name = "code_index",columnList = "code")})
public class TCode implements BaseEntity<Long> {
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Long id;
    @Column(name = "code",unique = true)
    private String code;
    @Column(name = "userId")
    private long userId;
    @Column(name = "type")
    private int type;
    @Column(name = "status")
    private int status;
    @Column(name = "reward")
    private String reward;
    @Column(name = "outTime")
    private long outTime;
    @Column(name = "useTime")
    private long useTime;
    @Column(name = "createTime")
    private long createTime;
    @Column(name = "paramData")
    private String paramData;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TCode";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }

}
