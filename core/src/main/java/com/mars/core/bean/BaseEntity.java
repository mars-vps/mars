package com.mars.core.bean;

import java.io.Serializable;

/**
 * 基础的实体接口
 * 
 * 
 * @param <ID>
 */
public interface BaseEntity<ID extends Serializable> extends Serializable {
	ID getId();

	void setId(ID id);

	/** 供redis使用的唯一前缀 */
	String getUniquePrefix();

	default String getRedisUid(){
		return generateRedisUid(this);
	}

    static String generateRedisUid(BaseEntity entity){
        return entity.getUniquePrefix()+"#"+entity.getId();
    }

    default boolean sendToRedis(){
		return true;
	}
}