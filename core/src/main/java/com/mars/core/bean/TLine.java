package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

@Entity
@Data
@Table(name = "t_line")
public class TLine implements BaseEntity<Long> {
    /** id1 */
    @Id
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "host")
    private String host;
    @Column(name = "password")
    private String password;
    @Column(name = "type")
    private int type;
    @Column(name = "lv")
    private String lv;
    @Column(name = "status")
    private String status;
    @Column(name = "encrypt")
    private String encrypt;
    @Column(name = "port")
    private int port;
    @Column(name = "remark")
    private String remark;
    @Column(name = "weight",nullable = false,columnDefinition = "int(11) default 0")
    private int weight;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TLine";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }

    public String toJson(){
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        obj.put("name",name);
        obj.put("host",host);
        obj.put("password",password);
        obj.put("type",type);
        obj.put("lv",lv);
        obj.put("status",status);
        obj.put("encrypt",encrypt);
        obj.put("port",port);
        obj.put("remark",remark);
        obj.put("weight",weight);
        return obj.toString();
    }

    public void decode() {
        try {
            this.name = URLDecoder.decode(name,"utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public void encode() {
        try {
            this.name = URLEncoder.encode(name,"utf8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
