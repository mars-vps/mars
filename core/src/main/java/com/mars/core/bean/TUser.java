package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import com.mars.core.constant.ParamDataConst;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.utils.TimeUtils;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "t_user",indexes = {@Index(name = "inviteCode_Index",columnList = "inviteCode")})
public class TUser implements BaseEntity<Long> {


    /** userid */
    @Id
    private long id;

    /** device_id */
    @Column(name = "deviceId")
    private String deviceId;

    /** email */
    @Column(name = "email")
    private String email;

    /** mobile */
    @Column(name = "mobile")
    private String mobile;

    /** nickname */
    @Column(name = "nickname")
    private String nickname;

    /** password */
    @Column(name = "password")
    private String password;

    /** username */
    @Column(name = "username",unique = true)
    private String username;

    /** createtime */
    @Column(name = "createTime")
    private long createTime;

    /** lastLoginTime */
    @Column(name = "lastLoginTime")
    private long lastLoginTime;

    /** userType */
    @Column(name = "userType")
    private int userType;

    /** vipLv 会员类型*/
    @Column(name = "vipLv")
    private int vipLv;

    /** vipEndTime 会员到期时间*/
    @Column(name = "vipEndTime",nullable = false,columnDefinition = "bigint(20) default 0")
    private long vipEndTime;

    /** inviteCode 邀请码*/
    @Column(name = "inviteCode",nullable = false,unique = true)
    private String inviteCode;

    /** dailyVersion */
    @Column(name = "dailyVersion",columnDefinition = "bigint(20) default 0")
    private long dailyVersion;

    /** paramData */
    @Column(name = "paramData")
    private String paramData;



    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TUser";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }


    public String toJson(){
        JSONObject obj = new JSONObject();
        obj.put("id",id);
        obj.put("username",username);
        //obj.put("password",password);
        obj.put("email",email);
        obj.put("mobile",mobile);
        obj.put("nickname",nickname);
        obj.put("deviceId",deviceId);
        obj.put("createTime",createTime);
        obj.put("userType",userType);
        obj.put("vipLv",vipLv);
        obj.put("vipName", VIPLevelEnum.getVIPLevelEnumById(vipLv));
        obj.put("vipEndTime",vipEndTime);
        obj.put("paramData",paramData);
        obj.put("lastLoginTime",lastLoginTime);
        obj.put("inviteCode",inviteCode);
        return obj.toString();
    }

    public boolean canInvite(int limit) {
        long beginOfDay = TimeUtils.getBeginOfDay(System.currentTimeMillis());
        if(dailyVersion < beginOfDay){
            return true;
        }
        if(this.paramData==null || "".equals(paramData)){
            this.paramData = new JSONObject().toString();
            return true;
        }
        JSONObject jsonObject = JSONObject.parseObject(this.paramData);
        int intValue = jsonObject.getIntValue(ParamDataConst.INVITE);
        if(intValue<limit){
            return true;
        }
        return false;
    }

    public void addInviteTimes() {
        long beginOfDay = TimeUtils.getBeginOfDay(System.currentTimeMillis());
        if(dailyVersion < beginOfDay){
            dailyVersion = beginOfDay;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ParamDataConst.INVITE,1);
            jsonObject.put(ParamDataConst.ADS,0);
            this.paramData = jsonObject.toString();
            return;
        }
        if(this.paramData==null || "".equals(paramData)){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ParamDataConst.INVITE,1);
            this.paramData = jsonObject.toString();
            return;
        }
        JSONObject jsonObject = JSONObject.parseObject(this.paramData);
        int intValue = jsonObject.getIntValue(ParamDataConst.INVITE);
        jsonObject.put(ParamDataConst.INVITE,intValue+1);
        this.paramData = jsonObject.toString();
    }

    public boolean canAds(int limit) {
        long beginOfDay = TimeUtils.getBeginOfDay(System.currentTimeMillis());
        if(dailyVersion < beginOfDay){
            return true;
        }
        if(this.paramData==null || "".equals(paramData)){
            this.paramData = new JSONObject().toString();
            return true;
        }
        JSONObject jsonObject = JSONObject.parseObject(this.paramData);
        int intValue = jsonObject.getIntValue(ParamDataConst.ADS);
        if(intValue<limit){
            return true;
        }
        return false;
    }

    public void addAdsTimes() {
        long beginOfDay = TimeUtils.getBeginOfDay(System.currentTimeMillis());
        if(dailyVersion < beginOfDay){
            dailyVersion = beginOfDay;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ParamDataConst.INVITE,0);
            jsonObject.put(ParamDataConst.ADS,1);
            this.paramData = jsonObject.toString();
            return;
        }
        if(this.paramData==null || "".equals(paramData)){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ParamDataConst.ADS,1);
            this.paramData = jsonObject.toString();
            return;
        }
        JSONObject jsonObject = JSONObject.parseObject(this.paramData);
        int intValue = jsonObject.getIntValue(ParamDataConst.ADS);
        jsonObject.put(ParamDataConst.ADS,intValue+1);
        this.paramData = jsonObject.toString();
    }


}
