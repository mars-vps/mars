package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import com.mars.core.constant.ParamDataConst;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.utils.TimeUtils;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "t_promo_code",indexes = {@Index(name = "promoCode_Index",columnList = "promoCode")})
public class TPromoCode implements BaseEntity<Long> {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private long id;

    /** promoCode 优惠码*/
    @Column(name = "promoCode",nullable = false,unique = true)
    private String promoCode;

    @Column(name = "zhekou",nullable = false)
    private float zhekou;

    @Column(name = "endTime")
    private long endTime;

    @Column(name = "status")
    private int status;

    @Column(name = "remark")
    private String remark;





    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TPromoCode";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }




}
