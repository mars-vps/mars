package com.mars.core.bean;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Data
@Table(name = "t_notice")
public class TNotice implements BaseEntity<Long> {
    /** id */
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;
    @Column(name = "time")
    private long time;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TNotice";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }

    public String toJson(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",id);
        jsonObject.put("title",title);
        jsonObject.put("content",content);
        jsonObject.put("time",simpleDateFormat.format(new Date(time)));
        return jsonObject.toString();
    }
}
