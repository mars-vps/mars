package com.mars.core.bean;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "t_order")
public class TOrder implements BaseEntity<String> {

    public static final int STATE_INIT = 0;
    public static final int STATE_AUTH_SUCCESS = 1;
    public static final int STATE_AUTH_FAIL = 3;

    /** 订单号 */
    @Id
    private String id;

    /** 创建时间 */
    @Column(name = "createTime")
    private long createTime;

    /** 完成时间 */
    @Column(name = "finishTime")
    private long finishTime;

    /** 订单状态 */
    @Column(name = "state",nullable = false)
    private int state;

    /** 档位id */
    @Column(name = "chargeId",nullable = false)
    private int chargeId;

    /** 价格 */
    @Column(name = "payItem")
    private String payItem;

    /** 角色id */
    @Column(name = "roleId",nullable = false)
    private long roleId;

    /** 充值平台id */
    @Column(name = "platformId")
    private String platformId;

    /** 充值平台订单号 */
    @Column(name = "platformOrderId")
    private String platformOrderId;

    /**  */
    @Column(name = "platformUid")
    private String platformUid;

    /** 充值回调数据 */
    @Column(name = "orderData",length = 1024)
    private String orderData;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUniquePrefix() {
        return "TOrder";
    }

    @Override
    public boolean sendToRedis() {
        return false;
    }
}
