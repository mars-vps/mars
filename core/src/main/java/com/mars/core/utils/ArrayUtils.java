package com.mars.core.utils;

import java.lang.reflect.Array;
import java.util.*;

public class ArrayUtils {
	/**
	 * 创建一个二维数组
	 * 
	 * @param <T>
	 *            数组单元的类型
	 * @param rows
	 *            数组的第一维长度
	 * @param cols
	 *            数组的第二位长度
	 * @param clazz
	 *            数组单元的类型
	 * @return 如果类型实例化失败,则数组里的单元为null
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[][] createTwoDimensionArray(int rows, int cols,
			Class<T> clazz) {
		T[][] arr = (T[][]) Array.newInstance(clazz, rows, cols);
		for (int y = 0; y < rows; y++) {
			for (int x = 0; x < cols; x++) {
				try {
					arr[y][x] = clazz.newInstance();
				} catch (Exception e) {
					arr[y][x] = null;
				}
			}
		}
		return arr;
	}

	public static int[] intList2Array(List<Integer> list) {
		if (list != null) {
			int[] ary = new int[list.size()];
			for (int i = 0; i < list.size(); i++) {
				ary[i] = list.get(i);
			}
			return ary;
		}
		return null;
	}
	
	public static int[] intCol2Array(Collection<Integer> list) {
		return intList2Array(new ArrayList<Integer>(list));
	}
	
	public static boolean[] booleanList2Array(List<Boolean> list){
		if (list != null) {
			boolean[] ary = new boolean[list.size()];
			for (int i = 0; i < list.size(); i++) {
				ary[i] = list.get(i);
			}
			return ary;
		}
		return null;
	}
	
	/**
	 * 去掉数组里面为0 的数值，并返回新数据
	 * @param arrays
	 * @return
	 */
	public static int[] removeZero(int[] arrays){
		int[] temps = new int[arrays.length];
		int index = 0;
		// 去掉为0的
		for (int i = 0; i < arrays.length; i++) {
			if (arrays[i] != 0) {
				temps[index] = arrays[i];
				index++;
			}
		}
		return Arrays.copyOf(temps, index);
	}

	public static int[] intSet2Array(Set<Integer> set) {
		if (set != null) {
			int i = 0;
			int[] ary = new int[set.size()];
			for (Integer integer : set) {
				ary[i] = integer;
				i++;
			}
			return ary;
		}
		return null;
	}
	
	public static Collection<Integer> intArray2Collection(int[] intArray) {
		Collection<Integer> _c = new ArrayList<>(intArray.length);
		for (int i = 0; i < intArray.length; i++) {
			_c.add(intArray[i]);
		}
		return _c;
	}
	
	public static boolean isIllegalIndex(int length,int index) {
		return index < 0 || index >= length; 
	}
	
	/**
	 * 合并两个数组
	 * @param source
	 * @param merge
	 * @return
	 * @auther sky 
	 * @date 2013-9-18
	 */
	public static <T> T[] mergeArray(T[] source,T[] merge) {
		if(source == null && merge == null){
			return null;
		}
		if(source == null){
			return merge.clone();
		}
		if(merge == null){
			return source.clone();
		}
		T[] _result = Arrays.copyOf(source, source.length + merge.length);
		System.arraycopy(merge, 0, _result, source.length, merge.length);
		return _result;
	}
	
	public static <T> T[] mergeArray(T[][] mergeArray) {
		if(mergeArray == null || mergeArray.length == 0){
			return null;
		}
		T[] _result = null;
		for(int i=0;i<mergeArray.length;i++){
			_result = mergeArray(_result,mergeArray[i]);
		}
		return _result;
	}
	
	public static int[] mergeArray(int[] source,int[] merge) {
		if(source == null && merge == null){
			return null;
		}
		if(source == null){
			return merge.clone();
		}
		if(merge == null){
			return source.clone();
		}
		int[] _result = Arrays.copyOf(source, source.length + merge.length);
		System.arraycopy(merge, 0, _result, source.length, merge.length);
		return _result;
	}
	
	public static byte[] mergeArray(byte[] source,byte[] merge) {
		if(source == null && merge == null){
			return null;
		}
		if(source == null){
			return merge.clone();
		}
		if(merge == null){
			return source.clone();
		}
		byte[] _result = Arrays.copyOf(source, source.length + merge.length);
		System.arraycopy(merge, 0, _result, source.length, merge.length);
		return _result;
	}
	
	public static int[] mergeArray(int[][] mergeArray) {
		if(mergeArray == null || mergeArray.length == 0){
			return null;
		}
		int[] _result = null;
		for(int i=0;i<mergeArray.length;i++){
			_result = mergeArray(_result,mergeArray[i]);
		}
		return _result;
	}
	
	public static int[] toArray(Collection<Integer> collection) {
		if(collection == null || collection.size() == 0){
			return new int[0];
		}
		int[] _result = new int[collection.size()];
		int _index = 0;
		for(Iterator<Integer> iter=collection.iterator();iter.hasNext();){
			_result[_index] = iter.next();
			_index ++;
		}
		return _result;
	}
	
	public static long[] toLongArray(Collection<Long> collection) {
		if(collection == null || collection.size() == 0){
			return new long[0];
		}
		long[] _result = new long[collection.size()];
		int _index = 0;
		for(Iterator<Long> iter=collection.iterator();iter.hasNext();){
			_result[_index] = iter.next();
			_index ++;
		}
		return _result;
	}

	
	public static<T> boolean isEmptyCol(Collection<T> col) {
		return col == null || col.isEmpty();
	}
	
	public static <T> List<T> asArrayList(T[] arr) {
		List<T> _result = new ArrayList<>();
		if(arr != null){
			for(T _t : arr){
				_result.add(_t);
			}
		}
		return _result;
	}
	
	public static <T> List<T> asArrayList(Class<T> clazz,Object[] arr) {
		List<T> _result = new ArrayList<>();
		if(arr != null){
			for(Object _t : arr){
				_result.add((T)_t);
			}
		}
		return _result;
	}
	
	public static byte[] changeObjToRaw(List<Byte> list) {
		int _size = list.size();
		byte[] _arr = new byte[_size];
		for (int _i=0; _i<_size; _i++) {
			Byte _bObj = list.get(_i);
			if (_bObj != null) {
				_arr[_i] = _bObj;
			} else {
				_arr[_i] = 0;
			}
		}
		return _arr;
	}
	
	public static long[] changeObjToRawLong(List<Long> list) {
		int _size = list.size();
		long[] _arr = new long[_size];
		for (int _i=0; _i<_size; _i++) {
			Long _bObj = list.get(_i);
			if (_bObj != null) {
				_arr[_i] = _bObj;
			} else {
				_arr[_i] = 0;
			}
		}
		return _arr;
	}
	
	
	public static List<Long> changeRawToObjLong(long[] arr) {
		int _size = arr.length;
		List<Long> _list = new ArrayList<>();
		for (int _i=0; _i<_size; _i++) {
			_list.add(arr[_i]);
		}
		return _list;
	}
	
	public static int[] addArr(int[] raw,int[] addValue){
		if(addValue == null){
			return raw;
		}
		if(raw == null){
			return addValue.clone();
		}
		for(int i=0;i<addValue.length;i++){
			raw[i]+=addValue[i];
		}
		return raw;
	}

	public static boolean contains(byte[] arr,byte value) {
		for(byte _val : arr){
			if(_val == value){
				return true;
			}
		}
		return false;
	}
	
	public static boolean contains(int[] arr,int value) {
		for(int _val : arr){
			if(_val == value){
				return true;
			}
		}
		return false;
	}
	
	public static int[] longArrayCast2IntArray(long[] longArray) {
		if (longArray == null || longArray.length == 0) {
			return new int[0];
		}
		int[] _intArray = new int[longArray.length];
		for (int i = 0; i < longArray.length; i++) {
			_intArray[i] = (int) longArray[i];
		}
		return _intArray;
	}
	
	public static long[] changeObjToRawLong(Long[] data) {
		if (data == null || data.length == 0) {
			return null ;
		}
		int _size = data.length;
		long[] _arr = new long[_size];
		for (int _i=0; _i<_size; _i++) {
			Long _bObj = data[_i];
			if (_bObj != null) {
				_arr[_i] = _bObj;
			} else {
				_arr[_i] = 0;
			}
		}
		return _arr;
	}
	
	public static Long[] changeRawToObjLongArr(long[] data) {
		if (data == null || data.length == 0) {
			return null ;
		}
		int _size = data.length;
		Long[] _arr = new Long[_size];
		for (int _i=0; _i<_size; _i++) {
			Long _bObj = data[_i];
			if (_bObj != null) {
				_arr[_i] = _bObj;
			} else {
				_arr[_i] = 0L;
			}
		}
		return _arr;
	}
	
	public static <T> List<T> initNullList(int size){
		List<T> _result = new ArrayList<>();
		
		for (int i = 0 ;i < size; i ++) {
			_result.add(null) ;
		}
		
		return _result ;
		
	}
	
	public static int searchToFirstFind(int[] arr, int val) {
		for (int i = 0; i < arr.length; i++) {
			if (val == arr[i]) {
				return i;
			}
		}
		return -1;
	}


	public static boolean isFill(int[] arr,int value){
		for (int i : arr) {
			if(i != value){
				return false;
			}
		}
		return true ;

	}
}
