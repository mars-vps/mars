package com.mars.core.utils;

import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class MailUtil {
    // 发送邮件的账号
    public static String ownEmailAccount = "super.vps.mars@gmail.com";
    // 发送邮件的密码------》授权码
    public static String ownEmailPassword = "usrksjzawiwjmkck";
    // 发送邮件的smtp 服务器 地址
    public static String myEmailSMTPHost = "smtp.gmail.com";

//    public static String myEmailSMTPHost = "smtp.mxhichina.com";
//
//    private static final String myEmailSMTPHost = "smtpdm.aliyun.com";
//    private static final String ALIDM_SMTP_PORT = "25";


   // public static List<InternetAddress> listReceive = new ArrayList<>();

    static{
        try {
            //listReceive.add(new InternetAddress("819379605@qq.com", "", "utf-8"));
            //listReceive.add(new InternetAddress("188210538@qq.com", "", "utf-8"));
//            listReceive.add(new InternetAddress("188210538@qq.com", "", "utf-8"));
//            listReceive.add(new InternetAddress("819379605@qq.com", "", "utf-8"));
//            listReceive.add(new InternetAddress("714990766@qq.com", "", "utf-8"));
//            listReceive.add(new InternetAddress("418680850@qq.com", "", "utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void sendMail(String title, String content,List<InternetAddress> listReceive) throws Exception {
        // 1.创建一个程序与邮件服务器会话对象 Session  解决linux上发送邮件失败问题，需要下面的配置
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        // 设置信任所有的主机
        sf.setTrustAllHosts( true );
        Properties props = new Properties();
        props.setProperty( "mail.transport.protocol", "SMTP" );
        props.setProperty( "mail.host", myEmailSMTPHost );
        props.setProperty( "mail.smtp.auth", "true" );// 指定验证为true是否需要身份验证
        props.setProperty( "mail.smtp.ssl.enable", "true" );
        props.put( "mail.smtp.ssl.socketFactory",sf );

        // 创建验证器
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                // 密码验证，第一个参数：邮箱名不包括@126.com，第二个参数：授权码
                return new PasswordAuthentication( ownEmailAccount, ownEmailPassword );

            }
        };
        Session session = Session.getInstance( props, auth );
        // 开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug( true );
        // 2.创建一个Message，它相当于是邮件内容
        Message message = createSimpleMail(session, title, content,listReceive);

        // 3.创建 Transport用于将邮件发送
        Transport.send( message );
    }

    /**
     * @Title: createSimpleMail
     * @Description: 创建邮件对象
     * @author: chengpeng
     * @param @param session
     * @param @return
     * @param @throws Exception    设定文件
     * @return Message    返回类型
     * @throws
     */
    private static Message createSimpleMail(Session session, String title, String content,List<InternetAddress> listReceive) throws Exception {
        MimeMessage message = new MimeMessage(session);
        // 设置发送邮件地址,param1 代表发送地址 param2 代表发送的名称(任意的) param3 代表名称编码方式
        message.setFrom(new InternetAddress(ownEmailAccount, "super.vps.mars@gmail.com", "utf-8"));
        // 代表收件人
        message.setRecipients(Message.RecipientType.TO, listReceive.toArray(new InternetAddress[listReceive.size()]));
        // To: 增加收件人（可选）
        /*message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress("dd@receive.com", "USER_DD", "UTF-8"));
        // Cc: 抄送（可选）
        message.setRecipient(MimeMessage.RecipientType.CC, new InternetAddress("ee@receive.com", "USER_EE", "UTF-8"));
        // Bcc: 密送（可选）
        message.setRecipient(MimeMessage.RecipientType.BCC, new InternetAddress("ff@receive.com", "USER_FF", "UTF-8"));*/
        // 设置邮件主题
        message.setSubject(title);
        // 设置邮件内容
        message.setContent(content, "text/html;charset=utf-8");
        // 设置发送时间
        message.setSentDate(new Date());
        // 保存上面的编辑内容
        message.saveChanges();
        // 将上面创建的对象写入本地
        OutputStream out = new FileOutputStream("MyEmail.eml");
        message.writeTo(out);
        out.flush();
        out.close();
        return message;

    }


//    public static void main(String[] args) throws Exception {
//        Properties prop = new Properties();
//        // 设置邮件传输采用的协议smtp
//        prop.setProperty("mail.transport.protocol", "smtp");
//        // 设置发送人邮件服务器的smtp地址
//        // 这里以网易的邮箱smtp服务器地址为例
//        prop.setProperty("mail.smtp.host", myEmailSMTPHost);
//        // 设置验证机制
//        prop.setProperty("mail.smtp.auth", "true");
//
//        // SMTP 服务器的端口 (非 SSL 连接的端口一般默认为 25, 可以不添加, 如果开启了 SSL 连接,
//        // 需要改为对应邮箱的 SMTP 服务器的端口, 具体可查看对应邮箱服务的帮助,
//        // QQ邮箱的SMTP(SLL)端口为465或587, 其他邮箱自行去查看)
//
//        /*final String smtpPort = "465";
//        prop.setProperty("mail.smtp.port", smtpPort);
//        prop.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        prop.setProperty("mail.smtp.socketFactory.fallback", "false");
//        prop.setProperty("mail.smtp.socketFactory.port", smtpPort);*/
//
//        // 创建对象回话跟服务器交互
//        Session session = Session.getInstance(prop);
//        // 会话采用debug模式
//        session.setDebug(true);
//        // 创建邮件对象
//        Message message = createSimpleMail(session);
//
//        Transport trans = session.getTransport();
//        // 链接邮件服务器
//        trans.connect(ownEmailAccount, ownEmailPassword);
//        // 发送信息
//        trans.sendMessage(message, message.getAllRecipients());
//        // 关闭链接
//        trans.close();
//    }

    public static void main(String[] args){
        try {
            //sendMail("测试一下","测试一下内容");
            List<InternetAddress> listReceive = new ArrayList<>();
            listReceive.add(new InternetAddress("584478719@qq.com", "", "utf-8"));
            sendMail("test","test",listReceive);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

