package com.mars.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 统一定义系统使用的slf4j的Logger
 * 
 * 
 */
public class Loggers {
	/** web相关的日志 */
	public static final Logger webLogger = LoggerFactory.getLogger("com.mars.web");
	/** admin相关的日志 */
	public static final Logger adminLogger = LoggerFactory.getLogger("com.mars.admin");

	
}
