package com.mars.core.utils;

import com.alibaba.fastjson.JSONArray;

public interface JsonPropDataHolder {
	
	public void loadJsonProp(String value);
	
	public JSONArray toJsonProp();

}
