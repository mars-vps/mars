package com.mars.core.utils;

public class RespCode {
    public static int FAIL = 0;//发生未知错误，请联系管理员
    public static int SUCCESS = 1;//成功
    public static int HAS_USER = 1000;//用户已存在
    public static int NO_PARAMS = 1001;//参数异常
    public static int USER_NOT_EXIST = 1002;//用户不存在
    public static int PWD_ERROR = 1003;//密码错误
    public static int CODE_ERROR = 1004;//验证错误
    public static int LINE_NOT_EXIST = 1005;//线路不存在
    public static int TOKEN_NOT_EXIST = 1006;//TOKEN不存在
    public static int TOKEN_ERROR = 1007;//TOKEN不合法
    public static int EMAIL_ERROR = 1008;//邮箱不合法


    public static int SHOP_ERROR = 1009;//会员等级和商品等级不匹配，请用完时间后再购买
    public static int SANFANG_ERROR = 1010;//三方支付异常，请联系管理员
    public static int NOT_LIMIT = 1011;//没有权限
    public static int ORDER_NULL = 1012;//订单不存在
    public static int PAY_FAIL = 1013;//支付失败

    public static int TCODE_IS_NULL = 1014;//兑换码不存在
    public static int TCODE_OUTTIME = 1015;//兑换码过时
    public static int TCODE_USED = 1016;//兑换码已经使用过
    public static int ADS_LIMIT = 1017;//次数上限


    public static int PROMO_CODE_ERROR = 1018;//优化码无效


}
