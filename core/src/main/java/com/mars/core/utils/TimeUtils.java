package com.mars.core.utils;

import org.springframework.util.Assert;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * 时间的工具类
 * 
 * 
 */
public class TimeUtils {
	private static final Calendar calendar = Calendar.getInstance();

	/** 天 */
	public static final long DAY = TimeUnit.DAYS.toMillis(1);

	/** 一周的天数 */
	public static final int DAYOFWEEK_CARDINALITY = 7;
	/** 周 */
	public static final long WEEK = DAY * (long)DAYOFWEEK_CARDINALITY; 
	/** 小时和分钟数，格式如10:20 */
	private static final DateFormat hmFormat = new SimpleDateFormat("HH:mm");
	/** 时 */
	public static final long HOUR = TimeUnit.HOURS.toMillis(1);
	/** 每天小时数 */
	public static final int HOUR_DAY = (int) (DAY / HOUR);

	/** 毫秒 */
	public static final long MILLI_SECOND = TimeUnit.MILLISECONDS.toMillis(1);
	/** 分 */
	public static final long MIN = TimeUnit.MINUTES.toMillis(1);
	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	/** 秒 */
	public static final long SECOND = TimeUnit.SECONDS.toMillis(1);

	/** 每小时秒数 */
	public static final int SECONDS_HOUR = (int) (HOUR / SECOND);
	/** 每分钟秒数 */
	public static final int SECONDS_MIN = (int) (MIN / SECOND);
	public static TimeZone TIME_ZONE;
	/** 年月日 时分, 格式如: 2011-01-11 01:10 */
	private static final DateFormat ymdhmFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");

	public static final int[] WEEK_DAYS = {Calendar.MONDAY, Calendar.TUESDAY, 
		Calendar.WEDNESDAY, Calendar.THURSDAY, 
		Calendar.FRIDAY, Calendar.SATURDAY, Calendar.SUNDAY};

	static {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
		TIME_ZONE = TimeZone.getDefault();
	}

	/**
	 * 判断当前时间是否可以执行重置操作, 例如在凌晨 02:00 重置玩家征收次数
	 * 
	 * @param currTime
	 *            当前时间戳
	 * @param lastOpTime
	 *            上次操作时间
	 * @param resetTime
	 *            重置时间戳
	 * @return
	 * 
	 */
	public static boolean canDoResetOp(long currTime, long lastOpTime,
			long resetTime) {
		return (currTime > resetTime) && (lastOpTime < resetTime);
	}
	/**
	 * 返回 <b>年份-月份-日期</b> 格式的时间. 例如: 2012-12-24
	 * 
	 * @param time
	 * @return
	 */
	public static String formatYMDTime(long time) {
		DateFormat ymdFormat = new SimpleDateFormat(
				"yyyy-MM-dd");
		return ymdFormat.format(time);
	}

	
	/**
	 * 设置几分钟之后的时间点
	 * 
	 * @param minutes
	 * @return
	 */
	public static Date getAfterMinutes(int minutes) {
		Calendar c = Calendar.getInstance();

		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + minutes);

		return c.getTime();
	}

	/**
	 * 获取几天后的当前时间点
	 * 
	 * @param day
	 * @return
	 */
	public static Date getAfterToday(int day) {
		Calendar c = Calendar.getInstance();

		c.add(Calendar.DATE, day);

		return c.getTime();
	}

	/**
	 * 获取特定日期当天的零点时间
	 * 
	 * @return
	 */
	public static long getBeginOfDay(long time) {
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(time);
		_calendar.set(Calendar.HOUR_OF_DAY, 0);
		_calendar.set(Calendar.MINUTE, 0);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		return _calendar.getTimeInMillis();
	}
	
	public static int getDailyVersion(long time) {
		return getSecNow(getBeginOfDay(time));
	}

	public static int geWeekVersion(long time) {
		return getSecNow(getBeginOfWeek(time));
	}


	public static long getEndOfDay(long time){
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(time);
		_calendar.set(Calendar.HOUR_OF_DAY, 23);
		_calendar.set(Calendar.MINUTE, 59);
		_calendar.set(Calendar.SECOND, 59);
		_calendar.set(Calendar.MILLISECOND, 59);
		return _calendar.getTimeInMillis();
	}

	public static long getBeginOfWeek(long time) {
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(time);
		//西方认为周日是第一天
		if(_calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			_calendar.add(Calendar.DAY_OF_WEEK, -6);
		}else{
			_calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		}
		_calendar.set(Calendar.HOUR_OF_DAY, 0);
		_calendar.set(Calendar.MINUTE, 0);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		return _calendar.getTimeInMillis();
	}
	
	public static long getBeginOfMonth(long time) {
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(time);
		_calendar.set(Calendar.DAY_OF_MONTH, 1);
		_calendar.set(Calendar.HOUR_OF_DAY, 0);
		_calendar.set(Calendar.MINUTE, 0);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		return _calendar.getTimeInMillis();
	}
	
	/**
	 * 以格式{@link TimeUtils#ymdhmFormat}解析数据，返回其对应的Calendar的实例
	 * 
	 * @param source
	 * @return
	 * @throws ParseException
	 */
	public static Calendar getCalendarByYMDHM(String source)
			throws ParseException {
		Calendar calendar = Calendar.getInstance();
		Date date = ymdhmFormat.parse(source);
		calendar.setTime(date);
		return calendar;
	}

	/**
	 * 获得修正后的时间
	 * 
	 * @param originTime
	 * @param changeYear
	 * @param changeMonth
	 * @param changeDay
	 * @param changeHour
	 * @param changeMinute
	 * @param changeSecond
	 * @return
	 */
	public static long getChangeTime(long originTime, int changeYear,
			int changeMonth, int changeDay, int changeHour, int changeMinute,
			int changeSecond) {
		calendar.setTimeInMillis(originTime);
		int _unChange = 0;
		if (changeYear != _unChange) {
			calendar.add(Calendar.YEAR, changeYear);
		}
		if (changeMonth != _unChange) {
			calendar.add(Calendar.MONTH, changeMonth);
		}
		if (changeDay != _unChange) {
			calendar.add(Calendar.DAY_OF_MONTH, changeDay);
		}
		if (changeHour != _unChange) {
			calendar.add(Calendar.HOUR_OF_DAY, changeHour);
		}
		if (changeMinute != _unChange) {
			calendar.add(Calendar.MINUTE, changeMinute);
		}
		if (changeSecond != _unChange) {
			calendar.add(Calendar.SECOND, changeSecond);
		}
		return calendar.getTimeInMillis();
	}

	/**
	 * 根据创建时间和有效时间计算截止时间
	 * 
	 * @param start
	 *            物品的创建时间
	 * @param validTime
	 *            物品的有效时间长度
	 * @param timeUnit
	 *            有效时间的单位 {@link TimeUtils#MILLI_SECOND} ~ {@link TimeUtils#DAY}
	 * @return 物品的截止时间
	 */
	public static long getDeadLine(long start, long validTime, long timeUnit) {
		return start + validTime * timeUnit;
	}

	/**
	 * 根据创建时间和有效时间计算截止时间
	 * 
	 * @param start
	 *            物品的创建时间
	 * @param validTime
	 *            物品的有效时间长度
	 * @param timeUnit
	 *            有效时间的单位 {@link TimeUtils#MILLI_SECOND} ~ {@link TimeUtils#DAY}
	 * @return 物品的截止时间
	 */
	public static long getDeadLine(Timestamp start, long validTime,
			long timeUnit) {
		return TimeUtils.getDeadLine(start.getTime(), validTime, timeUnit);
	}

	/**
	 * 获得指定时间的小时数
	 * 
	 * @param time
	 * @return
	 */
	public static int getHourTime(long time) {
		calendar.setTimeInMillis(time);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 按照中国人的习惯，1为周一
	 * @param time
	 * @return
	 */
	public static int getDayOfWeek(long time){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeek == Calendar.SUNDAY){
			dayOfWeek = 7;
		}else {
			dayOfWeek = dayOfWeek - 1;
		}
		return dayOfWeek;
	}

	
	/**
	 * 以日期中的日为实际计算单位，计算两个时间点实际日的差距 比如 12-1 23:00 和12-2 01:00，相差1天，而不是小于24小时就算做0天
	 * 如果(now - st)为正，则表示now在st之后
	 * 
	 * @param st
	 * @param now
	 * @return
	 */
	public static int getSoFarWentDays(Calendar st, Calendar now) {

		int sign = st.before(now) ? 1 : -1;
		if (now.before(st)) {
			Calendar tmp = now;
			now = st;
			st = tmp;
		}
		int days = now.get(Calendar.DAY_OF_YEAR) - st.get(Calendar.DAY_OF_YEAR);
		if (st.get(Calendar.YEAR) != now.get(Calendar.YEAR)) {
			Calendar cloneSt = (Calendar) st.clone();
			while (cloneSt.get(Calendar.YEAR) != now.get(Calendar.YEAR)) {
				days += cloneSt.getActualMaximum(Calendar.DAY_OF_YEAR);
				cloneSt.add(Calendar.YEAR, 1);
			}
		}

		return days * sign;
	}
	
	/**
	 * 以日期中的日为实际计算单位，计算两个时间点实际日的差距 比如 12-1 23:00 和12-2 01:00，相差1天，而不是小于24小时就算做0天
	 * 如果(now - st)为正，则表示now在st之后
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getSoFarWentRealDays(long start, long end){
		Calendar st = Calendar.getInstance();
		st.setTimeInMillis(start);
		Calendar et = Calendar.getInstance();
		et.setTimeInMillis(end);
		
		return getSoFarWentDays(st, et) ;
	}
	
	public static int getSoFarWentDays(long start, long end) {
		return (int) ((getBeginOfDay(end) - getBeginOfDay(start)) / DAY) ;
	}

	public static int getSoFarWentHours(long time1, long time2) {
		Calendar st = Calendar.getInstance();
		st.setTimeInMillis(time1);

		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(time2);

		if (now.before(st)) {
			Calendar tmp = now;
			now = st;
			st = tmp;
		}

		st.clear(Calendar.MILLISECOND);
		st.clear(Calendar.SECOND);
		st.clear(Calendar.MINUTE);

		int diffHour = 0;
		Calendar cloneSt = (Calendar) st.clone();
		while (cloneSt.before(now)) {
			cloneSt.add(Calendar.HOUR, 1);
			diffHour++;
		}

		if (diffHour != 0) {
			return diffHour - 1;
		} else {
			return diffHour;
		}
	}

	/**
	 * 获取两个时间的小时相差数, 经修改getSoFarWentHours(long time1, long time2)而来.
	 * 例：8:59:59与9:00:00相差1小时
	 * 
	 * @param time1
	 *            时间1
	 * @param time2
	 *            时间2
	 * @return 两个时间的小时相差数
	 */
	public static int getSoFarWentHours2(long time1, long time2) {
		Calendar st = Calendar.getInstance();
		st.setTimeInMillis(time1);

		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(time2);

		if (now.before(st)) {
			Calendar tmp = now;
			now = st;
			st = tmp;
		}

		st.clear(Calendar.MILLISECOND);
		st.clear(Calendar.SECOND);
		st.clear(Calendar.MINUTE);

		now.clear(Calendar.MILLISECOND);
		now.clear(Calendar.SECOND);
		now.clear(Calendar.MINUTE);

		int diffHour = 0;
		Calendar cloneSt = (Calendar) st.clone();
		while (cloneSt.before(now)) {
			cloneSt.add(Calendar.HOUR, 1);
			diffHour++;
		}

		return diffHour;
	}

	/**
	 * 
	 * @param now
	 * @param lastTime
	 * @return
	 */
	public static long getDifferTime(long now, long lastTime) {
		return now - lastTime;
	}

	 
	/**
	 * 得到从time1 到time2 中,specTime所指定的时分秒的时刻,有几次
	 * 
	 * @param time1
	 * @param time2
	 * @param specTime
	 * @return
	 */
	public static int getSpecTimeCountBetween(long time1, long time2,
			long specTime) {
		Calendar st = Calendar.getInstance();
		st.setTimeInMillis(time1);

		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(time2);

		Calendar spec = Calendar.getInstance();
		spec.setTimeInMillis(specTime);

		if (now.before(st)) {
			Calendar tmp = now;
			now = st;
			st = tmp;
		}

		// 第一个时间的年月日和被比较时间的时间部分合成
		Calendar st_spec = mergeDateAndTime(st, spec);

		if (isSameDay(time1, time2)) {
			if (hasSpecTimeBetween(time1, time2, st_spec.getTimeInMillis())) {
				return 1;
			} else {
				return 0;
			}
		}

		int diffDay = 0;
		Calendar cloneSt = (Calendar) st_spec.clone();
		while (cloneSt.before(now)) {
			cloneSt.add(Calendar.DATE, 1);
			diffDay++;
		}

		if (st.after(st_spec)) {
			diffDay--;
		}

		return diffDay;
	}

	/**
	 * 设置指定时间的设置为给定的时间数(不改变的时间数可填-1)
	 * 
	 * @param time
	 * @param year
	 * @param month
	 * @param day
	 *            (月中的天数)
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 */
	public static long getTime(long time, int year, int month, int day,
			int hour, int minute, int second) {
		calendar.setTimeInMillis(time);
		int _unChange = -1;
		if (year != _unChange) {
			calendar.set(Calendar.YEAR, year);
		}
		if (month != _unChange) {
			calendar.set(Calendar.MONTH, month);
		}
		if (day != _unChange) {
			calendar.set(Calendar.DAY_OF_MONTH, day);
		}
		if (hour != _unChange) {
			calendar.set(Calendar.HOUR_OF_DAY, hour);
		}
		if (minute != _unChange) {
			calendar.set(Calendar.MINUTE, minute);
		}
		if (second != _unChange) {
			calendar.set(Calendar.SECOND, second);
		}
		return calendar.getTimeInMillis();
	}
	
	/**
	 * 获取当天零点时间
	 * 
	 * @return
	 */
	public static long getTodayBegin(long now) {
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(now);
		_calendar.set(Calendar.HOUR_OF_DAY, 0);
		_calendar.set(Calendar.MINUTE, 0);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		return _calendar.getTimeInMillis();
	}
	

	/**
	 * 获取now指示日期的某个时间点
	 * 
	 * @param now
	 * @param someTime
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static long getTodaySomeTime(long now, String someTime) {
		long time = 0;

		Calendar c = Calendar.getInstance();
		try {
			Date nowDate = new Date(now);

			Date dateTime = sdf.parse(someTime);

			nowDate.setHours(dateTime.getHours());
			nowDate.setMinutes(dateTime.getMinutes());
			nowDate.setSeconds(dateTime.getSeconds());

			c.setTime(nowDate);
			time = c.getTime().getTime();
		} catch (Exception e) {

		}

		return time;
	}
	/**
	 * 以格式{@link TimeUtils#ymdhmFormat}解析数据，返回其表示的毫秒数
	 * 
	 * @param source
	 * @return
	 * @throws ParseException
	 */
	public static long getYMDHMTime(String source) throws ParseException {
		Date date = ymdhmFormat.parse(source);
		return date.getTime();
	}
	
	/**
	 * specTime is in [st,now] or not?
	 * 
	 * @param st
	 * @param now
	 * @param specTime
	 * @return
	 */
	private static boolean hasSpecTimeBetween(long st, long now, long specTime) {
		if (st <= specTime && specTime <= now) {
			return true;
		}
		return false;
	}

	/**
	 * 是否是同一天
	 * 
	 * @param src
	 * @param target
	 * @return
	 */
	public static boolean isSameDay(long src, long target) {
		int offset = TIME_ZONE.getRawOffset(); // 只考虑了时区，没考虑夏令时
		return (src + offset) / DAY == (target + offset) / DAY;
	}

	/**
	 * 把日期和时间合并
	 * 
	 * @param date
	 *            代表一个日期，方法其只取日期部分
	 * @param time
	 *            代表一个时间，方法其只取时间部分
	 * @return
	 */
	public static Calendar mergeDateAndTime(Calendar date, Calendar time) {
		Calendar cal = Calendar.getInstance();
		cal.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH),
				date.get(Calendar.DATE), time.get(Calendar.HOUR_OF_DAY),
				time.get(Calendar.MINUTE), time.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/**
	 * 将分钟数转换为小时数和分钟数的数组 如80分钟转换为1小时20分
	 * 
	 * @param mins
	 * @return
	 */
	public static int[] toTimeArray(int mins) {
		int[] _result = new int[2];
		_result[0] = (int) (mins * MIN / HOUR);
		_result[1] = (int) (mins - _result[0] * HOUR / MIN);
		return _result;
	}

	/**
	 * 返回按时间单位计算后的ms时间，该时间必须足够小以致可用整型表示
	 * 
	 * @param time
	 * @param fromTimeUnit
	 * @return
	 */
	public static long translateTime(int time, long fromTimeUnit) {
		return TimeUtils.translateTime(time, fromTimeUnit, MILLI_SECOND);
	}

	/**
	 * 将指定的时间值转化为期望单位的时间值
	 * 
	 * @param time
	 * @param fromTimeUnit
	 * @param toTimeUnit
	 * @return
	 */
	public static long translateTime(long time, long fromTimeUnit,
			long toTimeUnit) {
		Assert.isTrue(time >= 0);
		long milliTime = time * fromTimeUnit / toTimeUnit;
		Assert.isTrue(milliTime <= Long.MAX_VALUE,
				String.format("The time value %d is too big!", time));
		return milliTime;
	}

	/**
	 * 获取当前小时的开始时间
	 * 
	 * @param now 当前时间
	 * @return
	 */
	public static long getHourEnd(long now,int interval ) {
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTimeInMillis(now);
//		_calendar.add(Calendar.HOUR_OF_DAY, 1);
		_calendar.set(Calendar.MINUTE, interval);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		return _calendar.getTimeInMillis();
	}
	
	public static int getSecNow(long now) {
		return (int)(now / SECOND);
	}
	
	
	public static long calcHMBaseTime(String formatStr) {
		String[] _timeStr = formatStr.split(":");
		int _hour = Integer.parseInt(_timeStr[0]);
		int _min = Integer.parseInt(_timeStr[1]);
		return _hour * HOUR + _min * MIN;
	}
	
	/**
	 * 计算指定时间，指定小时的时间
	 * @param hour
	 * 		24小时制
	 * @return
	 */
	public static long calculateHourBeginTimeOfToday(long now, int hour) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(now);
		
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTimeInMillis();
	}
	
	/**
	 * 星期几  Calendar values SUNDAY and so on
	 * 
	 * @param time
	 * @return This Calendar values SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	 *         THURSDAY, FRIDAY, and SATURDAY.
	 */
	public static int getWeekOf(long time){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		return  calendar.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * 根据月日获得时间
	 * @param now
	 * @param month
	 * @param day
	 * @return
	 */
	public static long getTimeByMonthDay(long now,int month,int day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(now);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		return calendar.getTimeInMillis();
	}
	
	public static long getTimeByYearMonthDay(long now,int year,int month,int day){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(now);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		return calendar.getTimeInMillis();
	}
	
	
	/**
	 * 计算(nowTime)的周几是几号 参数是周几
	 * 周期是从星期一~星期日
	 * 
	 * @param weekOfDay
	 * 			星级几 应该使用Calendar中的常量
	 * @return
	 * 			返回是几号的0点0分0秒时间的Calendar实例
	 */
	public static Calendar calcTheDataOfTheWeekOfDay(long nowTime, int weekOfDay) {
		Calendar _c = Calendar.getInstance();
		_c.setTimeInMillis(nowTime);
		//如果今天是周日 直接回退一周
		if (_c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			_c.add(Calendar.WEEK_OF_MONTH, -1);
		}
		_c.set(Calendar.DAY_OF_WEEK, weekOfDay);
		_c.set(Calendar.HOUR_OF_DAY, 0);
		_c.set(Calendar.MINUTE, 0);
		_c.set(Calendar.SECOND, 0);
		_c.set(Calendar.MILLISECOND, 0);
		//回退以后取周日的话还要是下周的
		if (_c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			_c.add(Calendar.WEEK_OF_MONTH, 1);
		}
		return _c;
	}


	/**
	 * 区下一个最近的时间点
	 *
	 * @param now
	 * @param timePoint
	 * 		这个应该是有序的 小时数
	 * @return
	 */
	public static long getNextTimePointOfNow(long now, int timePoint[]) {
		int _nowHour = TimeUtils.getHourTime(now);
		int _nextRefreshHour = 0;
		for (int _needRefreshHour : timePoint) {
			if (_nowHour < _needRefreshHour) {
				_nextRefreshHour = _needRefreshHour;
				break;
			}
		}
		//今天的时间都过了 明天的第一个时间
		if (_nextRefreshHour <= 0) {
			//明天的开始
			long _beginTimeOfTomrrow = TimeUtils.getBeginOfDay(now + TimeUtils.DAY);
			return _beginTimeOfTomrrow + timePoint[0] * TimeUtils.HOUR;
		} else {
			//今天的
			return TimeUtils.getBeginOfDay(now) + _nextRefreshHour * TimeUtils.HOUR;
		}
	}

	/**
	 * 获得距离下一小时的秒数
	 *
	 * @param time
	 * @return
	 */
	public static int getSecondToNextHour(long time) {
		calendar.setTimeInMillis(time);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		return SECONDS_HOUR - minute*SECONDS_MIN-second;
	}

}