package com.mars.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;

/**
 * JSON工具类
 *
 *
 */
@SuppressWarnings("unchecked")
public class JsonUtils {

	/** 默认Boolean */
	public final static boolean DEFAULT_BOOLEAN = false;

	/** 默认Double */
	public final static double DEFAULT_DOUBLE = 0d;

	/** 默认Float */
	public final static float DEFAULT_FLOAT = 0f;

	/** 默认Int {@value} */
	public final static int DEFAULT_INT = 0;

	/** 默认Long */
	private final static long DEFAULT_LONG = 0l;

	/** 默认字符串 */
	public final static String DEFAULT_STRING = "";

	/** Never ever modify this value */
	public static final String KEY_ONE = "key1";
	public static final String KEY_TWO = "key2";
	public static final String KEY_THREE = "key3";
	public static final String KEY_FOUR = "key4";
	public static final String KEY_FIVE = "key5";
	public static final String KEY_SIX = "key6";



	/** 日志 */
	public static final Logger logger = LoggerFactory.getLogger("jsonutils");

	public static JSONObject toJsonObject(String jsonStr) {
		JSONObject _json;
		if (jsonStr == null || jsonStr.isEmpty()) {
			_json = new JSONObject();
		} else {
			_json = JSONObject.parseObject(jsonStr);
		}
		return _json;
	}

	/**
	 * 判断JsonObject是否包含特定key
	 *
	 * @param jsonObj
	 * @param key
	 * @return
	 */
	public static boolean containsKey(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return true;
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return false;
	}

	/**
	 * 获取boolean类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static boolean getBoolean(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getBoolean(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_BOOLEAN;
	}

	/**
	 * 获取double类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static double getDouble(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getDouble(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_DOUBLE;
	}

	/**
	 * 获取float类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static float getFloat(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return (float) jsonObj.getDoubleValue(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_FLOAT;
	}

	/**
	 * 获取int类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static int getInt(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getIntValue(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_INT;
	}

	public static int getInt(String jsonInfo, Object key){
		JSONObject _jsonObj = JSONObject.parseObject(jsonInfo) ;

		return getInt(_jsonObj, key) ;
	}

	/**
	 * 获取JSONArray类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static JSONArray getJSONArray(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getJSONArray(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return null;
	}

	/**
	 * 获取JSONObject类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static JSONObject getJSONObject(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getJSONObject(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return null;
	}

	/**
	 * 获取long类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static long getLong(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return Long.parseLong(jsonObj.getString(_key));
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_LONG;
	}

	/**
	 * 获取obj类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static Object getObject(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.get(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return null;
	}

	/**
	 * 获取字符串类型数据
	 *
	 * @param jsonObj
	 *            Json源
	 * @param key
	 * @return
	 */
	public static String getString(JSONObject jsonObj, Object key) {
		try {
			String _key = String.valueOf(key);
			if (jsonObj.containsKey(_key)) {
				return jsonObj.getString(_key);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error(ErrorsUtil.error(
						"",
						"#GS.JsonUtils.getString", "Json format error"), e);
			}
		}
		return DEFAULT_STRING;
	}

	public static JSONArray listToJson(Collection<? extends JsonPropDataHolder> collection){
		JSONArray _array = new JSONArray();
		if (collection == null) {
			return _array;
		}
		for(JsonPropDataHolder _holder : collection){
			_array.add(_holder.toJsonProp());
		}
		return _array;
	}

	public static int[] jsonToIntArr(JSONArray array){
		int[] arr = new int[array.size()];
		for(int i=0;i<array.size();i++){
			arr[i] = array.getIntValue(i);
		}
		return arr;
	}

	public static float[] jsonToFloatArr(JSONArray array){
		float[] arr = new float[array.size()];
		for(int i=0;i<array.size();i++){
			arr[i] = array.getFloatValue(i);
		}
		return arr;
	}

	public static String[] jsonToStringArr(JSONArray array){
		String[] arr = new String[array.size()];
		for(int i=0;i<array.size();i++){
			arr[i] = array.getString(i);
		}
		return arr;
	}


	public static <E extends JsonPropDataHolder> String arrayToJson(E[] arr){
		JSONArray _array = new JSONArray();
		for(E _holder : arr){
			_array.add(_holder.toJsonProp());
		}
		return _array.toString();
	}

	public static <E extends JsonPropDataHolder> E[] jsonToArray(String json,Class<E> clazz){
		if(StringUtils.isEmpty(json)){
			return (E[]) Array.newInstance(clazz, 0);
		}
		JSONArray _json = JSONArray.parseArray(json);
		E[] _arr = (E[]) Array.newInstance(clazz, _json.size());
		for(int i=0;i<_json.size();i++){
			String _str = _json.getString(i);
			try {
				E _t = clazz.newInstance();
				_t.loadJsonProp(_str);
				_arr[i] = _t;
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return _arr;
	}

	public static Map<Integer, Integer> jsonToIntMap(String json){
		Map<Integer, Integer> _result = new HashMap<Integer, Integer>();
		if(StringUtils.isEmpty(json)){
			return _result;
		}
		JSONObject _obj = JSONObject.parseObject(json);
		for(Object _key : _obj.keySet()){
			int _id = Integer.parseInt((String)_key);
			_result.put(_id, _obj.getIntValue((String)_key));
		}
		return _result;
	}
	public static Map<Long, Long> jsonToLongMap(String json){
		Map<Long, Long> _result = new HashMap<Long, Long>();
		if(StringUtils.isEmpty(json)){
			return _result;
		}
		JSONObject _obj = JSONObject.parseObject(json);
		for(Object _key : _obj.keySet()){
			Long _id = Long.parseLong((String)_key);
			_result.put(_id, _obj.getLongValue((String)_key));
		}
		return _result;
	}

	public static String intMapToJson(Map<Integer, Integer> map){
		JSONObject _obj = new JSONObject();
		if(map != null){
			for(Entry<Integer, Integer> _entry : map.entrySet()){
				_obj.put(_entry.getKey().toString(), _entry.getValue());
			}
		}
		return _obj.toString();
	}
	public static String longMapToJson(Map<Long, Long> map){
		JSONObject _obj = new JSONObject();
		if(map != null){
			for(Entry<Long, Long> _entry : map.entrySet()){
				_obj.put(_entry.getKey().toString(), _entry.getValue());
			}
		}
		return _obj.toString();
	}

	public static String arrayToJson(int[] arr){
		return JSON.toJSONString(arr);
	}

	public static JSONArray arrayToJsonArray(int[] arr){
		return (JSONArray)JSON.toJSON(arr);
	}

	public static String collectionIntegerToJson(Collection<Integer> IntColl) {
		return arrayToJson(ArrayUtils.intCol2Array(IntColl));
	}

	public static int[] jsonToArray(String json){
		if(StringUtils.isEmpty(json)){
			return new int[0];
		}
		JSONArray _json = JSONArray.parseArray(json);
		int[] _arr = new int[_json.size()];
		for(int i=0;i<_json.size();i++){
			_arr[i] = _json.getIntValue(i);
		}
		return _arr;
	}

	public static String longArrayToJson(long[] arr){
		return JSON.toJSONString(arr);
	}

	public static long[] jsonToLongArray(String json){
		if(StringUtils.isEmpty(json)){
			return new long[0];
		}
		JSONArray _json = JSONArray.parseArray(json);
		long[] _arr = new long[_json.size()];
		for(int i=0;i<_json.size();i++){
			_arr[i] = _json.getLong(i);
		}
		return _arr;
	}

	public static <K,V extends JsonPropDataHolder> Map<K,V> mapFromJson(String json,Class<V> clazz,Function<V,K> keyFunction){
		return listToMap(listFromJson(json,clazz),keyFunction);
	}

    public static <T extends JsonPropDataHolder> List<T> listFromJson(String json,Class<T> clazz){
        List<T> _list = new ArrayList<T>();
        fromJson(_list,json,clazz);
        return _list;
    }

	private static  <T extends JsonPropDataHolder> void fromJson(Collection<T> col,String json,Class<T> clazz){
        if(!StringUtils.isEmpty(json)){
            JSONArray _array = JSONArray.parseArray(json);
            for(int i=0;i<_array.size();i++){
                String _str = _array.getString(i);
                try {
                    T _t = clazz.newInstance();
                    _t.loadJsonProp(_str);
                    col.add(_t);
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	public static <T extends JsonPropDataHolder> Set<T> setFromJson(String json,Class<T> clazz){
		Set<T> _set = new HashSet<>();
        fromJson(_set,json,clazz);
		return _set;
	}

	public static <T extends JsonPropDataHolder> T singleFromJson(String json,Class<T> clazz){
		try {
			T _t = clazz.newInstance();
			_t.loadJsonProp(json);
			return _t;
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Integer> intListFromJson(String json){
		List<Integer> _list = new ArrayList<Integer>();
		if(StringUtils.isEmpty(json)){
			return _list;
		}
		JSONArray _array = JSONArray.parseArray(json);
		for(int i=0;i<_array.size();i++){
			_list.add(_array.getIntValue(i));
		}
		return _list;
	}

	public static List<Long> longListFromJson(String json){
		List<Long> _list = new ArrayList<Long>();
		if(StringUtils.isEmpty(json)){
			return _list;
		}
		JSONArray _array = JSONArray.parseArray(json);
		for(int i=0;i<_array.size();i++){
			_list.add(_array.getLong(i));
		}
		return _list;
	}

	public static Set<Integer> intSetFromJson(String json){
		Set<Integer> _list = new HashSet<Integer>();
		if(StringUtils.isEmpty(json)){
			return _list;
		}
		JSONArray _array = JSONArray.parseArray(json);
		for(int i=0;i<_array.size();i++){
			_list.add(_array.getIntValue(i));
		}
		return _list;
	}

	public static JSONArray intSetToJson(Set<Integer> intSet) {
		JSONArray _array = new JSONArray();
		if (intSet == null) {
			return _array;
		}
		for (Integer value : intSet) {
			_array.add(value);
		}
		return _array;
	}

	public static Set<Long> longSetFromJson(String json){
		Set<Long> _list = new HashSet<Long>();
		if(StringUtils.isEmpty(json)){
			return _list;
		}
		JSONArray _array = JSONArray.parseArray(json);
		for(int i=0;i<_array.size();i++){
			_list.add(_array.getLong(i));
		}
		return _list;
	}

	public static JSONArray longSetToJson(Set<Long> intSet) {
		JSONArray _array = new JSONArray();
		if (intSet == null) {
			return _array;
		}
		for (Long value : intSet) {
			_array.add(value);
		}
		return _array;
	}

	public static boolean isJSONArray(String json){
		if (StringUtils.isEmpty(json)) {
			return false ;
		}

		return json.startsWith("[") && json.endsWith("]") ;
	}

	public static void addInt(JSONObject json,JSONObject toAdd){
		for(String key : toAdd.keySet()){
			if (json.containsKey(key)) {
				json.put(key,json.getIntValue(key) + toAdd.getIntValue(key));
			}else{
				json.put(key, toAdd.getIntValue(key));
			}
		}
	}

	public static JSONArray toJsonArray(Object ... objects){
		JSONArray arr = new JSONArray();
		for(Object o : objects)
			arr.add(o);
		return arr;
	}

	public static JSONArray toJsonArray(Collection<?> collection){
		JSONArray arr = new JSONArray();
		for(Object o : collection)
			arr.add(o);
		return arr;
	}

	public static <E> JSONArray toJsonArray(Collection<E> collection, Function<E,String> function){
		JSONArray arr = new JSONArray();
		collection.forEach(e->arr.add(function.apply(e)));
		return arr;
	}

	public static <E> JSONArray toJsonArrayWithObj(Collection<E> collection, Function<E,JSONObject> function){
		JSONArray arr = new JSONArray();
		collection.forEach(e->arr.add(function.apply(e)));
		return arr;
	}

	public static <E> JSONArray toJsonArrayWithArray(Collection<E> collection, Function<E,JSONArray> function){
		JSONArray arr = new JSONArray();
		collection.forEach(e->arr.add(function.apply(e)));
		return arr;
	}

	public static <E> JSONArray toJsonArray(E[] array, Function<E,String> function){
		JSONArray arr = new JSONArray();
		for(E e : array){
			arr.add(function.apply(e));
		}
		return arr;
	}

	public static <E> List<E> listFromJson(JSONArray array, Function<String,E> function){
		List<E> result = new ArrayList<>();
		array.forEach(e->result.add(function.apply((String)e)));
		return result;
	}

    public static <K,V> Map<K,V> listToMap(List<V> list,Function<V,K> keyFunction){
        Map<K,V> map = new HashMap<>();
        list.forEach(v->map.put(keyFunction.apply(v),v));
        return map;
    }

    public static <K,V> Set<K> listToSet(List<V> list,Function<V,K> keyFunction){
        Set<K> set = new HashSet<>();
        list.forEach(v->set.add(keyFunction.apply(v)));
        return set;
    }

    public static class JsonObjWrapper{
		public JSONObject jsonObject = new JSONObject(true);
		public JsonObjWrapper put(String key,Object value){
			jsonObject.put(key,value);
			return this;
		}
		public JSONObject getJsonObject() {
			return jsonObject;
		}

        @Override
        public String toString() {
            return jsonObject.toString();
        }
    }


    public static JSONObject parseStringToJson(String str){
		try {
			return JSONObject.parseObject(str);
		}catch (Exception e){
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("oldData",str);
			return jsonObject;
		}
	}
}
