package com.mars.core.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * MD5 加密编码工具类（从龙之刃拷贝过来）
 * 
 * 
 */
public class MD5Util {

	/**
	 * 验证MD5密码是否正确
	 *
	 * @param inputString
	 * @return
	 */
	public static boolean authMD5String(String md5, String inputString) {
		if (md5.equals(encodeByMD5(inputString))) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean authMD5StringIgnoreCase(String md5, String inputString) {
		if (md5.equalsIgnoreCase(encodeByMD5(inputString))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 将输入的字符串进行MD5加密（编码）
	 * 
	 * @param inputString
	 * @return
	 */
	public static String createMD5String(String inputString) {
		return encodeByMD5(inputString);
	}

	/**
	 * 对字符串进行MD5编码
	 * 
	 * @param originStr
	 * @return
	 */
	public static String encodeByMD5(String originStr) {
		return DigestUtils.md5Hex(originStr);
	}



}
