package com.mars.core.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EncryptDecrypt {
    private static final int[] KEY = {23, 22, 24, 4, 51, 26, 37, 27, 24, 6, 26, 38, 29, 35, 18, 21, 14, 3, 12, 4, 41, 39, 18, 44, 54, 21, 33, 35, 31, 22, 34, 53, 51, 44, 8, 12, 3, 0, 28, 1, 48, 9, 51, 57, 20, 44, 27, 3, 16, 48};
    private static final Map MMP = new HashMap(12);
    private static final String SALT = "dfsad@#%$@TDGDF%$#%@#%WFRGFDHJKcvxznmfdsgdfgs2432534fgdf46t";

    static {
        for (short s = 10; s < 16; s = (short) (s + 1)) {
            Map localMap = MMP;
            char c = (char) (s + 65 - 10);
            localMap.put(Short.valueOf(s), Character.valueOf(c));
            MMP.put(Character.valueOf(c), Short.valueOf(s));
        }
    }

    public static byte[] decrypt(byte[] paramArrayOfByte) {
        return encrypt(paramArrayOfByte);
    }

    public static byte[] decryptFromHex(String paramString) {
        byte[] arrayOfByte = new byte[paramString.length() / 2];
        int j = 0;
        int i = 0;
        while (j < paramString.length()) {
            int k;
            if ((paramString.charAt(j) >= '0') && (paramString.charAt(j) <= '9'))
                k = (short) ((short) (paramString.charAt(j) - '0') << 4);
            else
                k = (short) (((Short) MMP.get(Character.valueOf(paramString.charAt(j)))).shortValue() << 4);
            int m = j + 1;
            if ((paramString.charAt(m) >= '0') && (paramString.charAt(m) <= '9'))
                m = (short) (paramString.charAt(m) - '0');
            else
                m = ((Short) MMP.get(Character.valueOf(paramString.charAt(m)))).shortValue();
            arrayOfByte[i] = ((byte) (k + m));
            j += 2;
            i += 1;
        }
        return decrypt(arrayOfByte);
    }

    /** 解密
     * @param paramString
     * @return
     */
    public static String decryptStringFromHex(String paramString) {
        return new String(decryptFromHex(paramString));
    }

    public static String decryptToString(byte[] paramArrayOfByte) {
        return new String(encrypt(paramArrayOfByte));
    }

    private void doDecrypt() {
        System.err.println("-------------解密-------------");
        String str = new Scanner(System.in).next();
        System.out.println(decryptStringFromHex(str));
    }

    private void doEncrypt() {
        System.err.println("-------------加密-------------");
        String str = new Scanner(System.in).next();
        System.out.println(encryptStringToHex(str));
    }

    public static byte[] encrypt(byte[] paramArrayOfByte) {
        byte[] arrayOfByte1 = new byte[KEY.length];
        int j = 0;
        int i = 0;
        while (i < KEY.length) {
            arrayOfByte1[i] = ((byte) "dfsad@#%$@TDGDF%$#%@#%WFRGFDHJKcvxznmfdsgdfgs2432534fgdf46t".charAt(KEY[i]));
            i += 1;
        }
        byte[] arrayOfByte2 = new byte[paramArrayOfByte.length];
        i = 0;
        while (j < paramArrayOfByte.length) {
            int k = paramArrayOfByte[j];
            arrayOfByte2[j] = ((byte) (arrayOfByte1[(i % arrayOfByte1.length)] ^ k));
            j += 1;
            i += 1;
        }
        return arrayOfByte2;
    }

    public static byte[] encryptString(String paramString) {
        return encrypt(paramString.getBytes());
    }

    /**加密
     * @param paramString
     * @return
     */
    public static String encryptStringToHex(String paramString) {
        return encryptToHex(paramString.getBytes());
    }

    public static String encryptToHex(byte[] paramArrayOfByte) {
        paramArrayOfByte = encrypt(paramArrayOfByte);
        StringBuffer localStringBuffer = new StringBuffer("");
        int i = 0;
        while (i < paramArrayOfByte.length) {
            short s1 = (short) (paramArrayOfByte[i] & 0xF);
            short s2 = (short) ((paramArrayOfByte[i] & 0xF0) >>> 4);
            if (s2 < 10)
                localStringBuffer.append(s2);
            else
                localStringBuffer.append(MMP.get(Short.valueOf(s2)));
            if (s1 < 10)
                localStringBuffer.append(s1);
            else
                localStringBuffer.append(MMP.get(Short.valueOf(s1)));
            i += 1;
        }
        return localStringBuffer.toString();
    }

    public static int[] generateKey(int paramInt) {
        int j = "dfsad@#%$@TDGDF%$#%@#%WFRGFDHJKcvxznmfdsgdfgs2432534fgdf46t".length();
        int[] arrayOfInt = new int[paramInt];
        int i = 0;
        while (i < paramInt) {
            double d1 = Math.random();
            double d2 = j;
            Double.isNaN(d2);
            arrayOfInt[i] = ((int) (d1 * d2));
            i += 1;
        }
        return arrayOfInt;
    }

    public void test() {
    }

    public static void main(String[] args){
        String code =  "adsfdfhgfhjghjkewrqawe";

        //加密
        String secret = encryptStringToHex(code);

        System.out.println(secret);
        //解密
        System.out.println(decryptStringFromHex("770863"));



    }


}

/* Location:           W:\WorkSpace\Work\android\反编译\ApkTool\Dex\dex2jar-2.0\classes-dex2jar.jar
 * Qualified Name:     me.xhss.rabbit.util.EncryptDecrypt
 * JD-Core Version:    0.6.2
 */