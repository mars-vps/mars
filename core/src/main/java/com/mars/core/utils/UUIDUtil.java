package com.mars.core.utils;

import java.util.UUID;

public class UUIDUtil {

    public static String randomUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }

    public static String randomUUID16(){
        return Long.toHexString(UUID.randomUUID().getLeastSignificantBits());
    }

    public static String randomString(){
        return Long.toHexString(System.currentTimeMillis());
    }

    public static void main(String[] args){

    }
}
