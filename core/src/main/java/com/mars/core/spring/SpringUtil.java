package com.mars.core.spring;

import org.springframework.context.ApplicationContext;

public class SpringUtil {

    public static ApplicationContext CTX;


    public static boolean isProfile(String profile){
        return getProfile().equals(profile);
    }

    public static String getProfile(){
        return CTX.getEnvironment().getActiveProfiles()[0];
    }

    public static <T> T getBean(String name){
        return (T)CTX.getBean(name);
    }
    public static <T> T getBean(Class<T> clazz){
        return CTX.getBean(clazz);
    }

}
