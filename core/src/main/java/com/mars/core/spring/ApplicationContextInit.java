package com.mars.core.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextInit implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtil.CTX = applicationContext;
        /*StringRedisSerializer serializer = new StringRedisSerializer();
        // 使用Jackson2JsonRedisSerialize 替换默认序列化(默认采用的是JDK序列化)
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        SpringUtil.getRedisTemplate().setKeySerializer(serializer);
        SpringUtil.getRedisTemplate().setDefaultSerializer(jackson2JsonRedisSerializer);
        SpringUtil.getRedisTemplate().setValueSerializer(jackson2JsonRedisSerializer);
        SpringUtil.getRedisTemplate().setHashKeySerializer(jackson2JsonRedisSerializer);
        SpringUtil.getRedisTemplate().setHashValueSerializer(jackson2JsonRedisSerializer);*/

    }
}
