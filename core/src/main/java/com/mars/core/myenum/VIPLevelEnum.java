package com.mars.core.myenum;

public enum VIPLevelEnum {
    vip0(0,"火星公民"),
    vip1(1,"火星人杰VIP1"),
    vip2(2,"火星骑士VIP2"),
    vip3(3,"火星勋爵VIP3"),
    vip4(4,"火星男爵VIP4"),
    vip5(5,"火星子爵VIP5"),
    vip6(6,"火星伯爵VIP6"),
    vip7(7,"火星侯爵VIP7"),
    vip8(8,"火星公爵VIP8"),
    vip9(9,"火星王侯VIP9"),
    vip10(10,"火星大帝VIP10"),
    ;

    private int id;

    private String name;

    VIPLevelEnum(int id, String name){
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static  String getVIPLevelEnumById(int id){
        for (VIPLevelEnum item:values()){
            if(item.getId() == id){
                return item.getName();
            }
        }
        return "";
    }
}
