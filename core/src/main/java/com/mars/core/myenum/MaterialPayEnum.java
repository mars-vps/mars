package com.mars.core.myenum;

public enum MaterialPayEnum {
    ALIPAY_WEB(1,"ALIPAY_WEB","支付宝",0,1),
    ALIPAY_WAP(2,"ALIPAY_WAP","支付宝",0,1),
    WXPAY(3,"WXPAY","微信",0,1),
    ALIPAY_SM(4,"alipaysm","支付宝",0,2),
    WXPAY_SM(5,"wxpay","支付宝（测试）",0,2),
    QQPAY_SM(6,"qqpay","支付宝（测试）",0,2),
    ALIPAY_LY(7,"alipay","支付宝",0,3),
    ;

    private int id;

    private String des;

    private String name;

    private int status;

    private int subType;

    MaterialPayEnum(int id,String des,String name,int status,int subType){
        this.id = id;
        this.des = des;
        this.name = name;
        this.status = status;
        this.subType = subType;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static MaterialPayEnum getMaterialPayEnumById(int id){
        for (MaterialPayEnum item:values()){
            if(item.getId() == id){
                return item;
            }
        }
        return null;
    }

    public static MaterialPayEnum getMaterialPayEnumByDes(String des){
        for (MaterialPayEnum item:values()){
            if(item.getDes().equals(des)){
                return item;
            }
        }
        return null;
    }

    public int getStatus() {
        return status;
    }

    public int getSubType() {
        return subType;
    }
}
