package com.mars.core.template;


import com.mars.core.utils.StringUtils;
import lombok.Data;

//
@Data
public class LineTemplate implements JsonTemplate {

    private int id;
    private String name;
    private String host;
    private int type;
    private String lv;
    private String status;
    private String encrypt;
    private int port;
    private String remark;
    private int weight;

    private String pUuid;
    private int pAlterId;
    private String pProtocol;
    private String pType;
    private String pHost;
    private String pPath;
    private String pXProtocol;

    //



    private String password;
    @Override
    public int getId() {
        return id;
    }

    @Override
    public void check() {
        StringBuffer sb = new StringBuffer();
        if(!StringUtils.isEmpty(pUuid)){
            sb.append(pUuid);
        }
        sb.append("::");
        if( pAlterId>0){
            sb.append(pAlterId);
        }
        sb.append("::");

        if(!StringUtils.isEmpty(pProtocol)){
            sb.append(pProtocol);
        }
        sb.append("::");

        if(!StringUtils.isEmpty(pType)){
            sb.append(pType);
        }
        sb.append("::");

        if(!StringUtils.isEmpty(pHost)){
            sb.append(pHost);
        }
        sb.append("::");
        if(!StringUtils.isEmpty(pPath)){
            sb.append(pPath);
        }
        sb.append("::");
        if(!StringUtils.isEmpty(pXProtocol)){
            sb.append(pXProtocol);
        }
        password = sb.toString();
    }

    @Override
    public String toString() {
        return "LineTemplate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", type=" + type +
                ", lv='" + lv + '\'' +
                ", status='" + status + '\'' +
                ", encrypt='" + encrypt + '\'' +
                ", port=" + port +
                ", remark='" + remark + '\'' +
                ", weight=" + weight +
                ", password='" + password + '\'' +
                '}';
    }
}
