package com.mars.core.template;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.service.TemplateService;
import com.mars.core.utils.JsonUtils;
import com.mars.core.utils.StringUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * json模板读取的接口以及工具类
 * @author farseer.sky
 */
public interface JsonTemplate {

    int getId();

    void check();

    Map<Class<?>,Parser> parserMap = new HashMap<>();

    static void registerParser(Class<?> clazz, Parser parser){
        parserMap.put(clazz,parser);
    }

    static <T extends JsonTemplate> T parse(Class<T> clazz, JSONObject json){
        return parse(clazz,json,null);
    }

    static <T extends JsonTemplate> T parse(Class<T> clazz, JSONObject json, String index){
        Parser parser = parserMap.get(clazz);
        if(parser == null){
            parser = new Parser() {
                @Override
                public <T extends JsonTemplate> T parse(Class<T> clazz, JSONObject json, String index) {
                    try{
                        T t = clazz.newInstance();
                        parseField(t,clazz,json,index);
                        t.check();
                        return t;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    return null;
                }
            };
            parserMap.put(clazz,parser);
        }
        return parser.parse(clazz,json,index);
    }

    static <T extends JsonTemplate> void parseField(T t, Class<?> clazz, JSONObject json, String index) throws Exception{
        if(clazz.getSuperclass() != Object.class && JsonTemplate.class.isAssignableFrom(clazz.getSuperclass())){
            parseField(t,clazz.getSuperclass(),json,index);
        }
        for(Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            String fieldName = index != null ? field.getName() + index : field.getName();
            if(json.containsKey(fieldName)){
                if (field.getType() == int.class){
                    field.set(t,json.getIntValue(fieldName));
                }else if(field.getType() == long.class){
                    field.set(t,json.getLongValue(fieldName));
                }else if(field.getType() == float.class){
                    field.set(t,json.getFloatValue(fieldName));
                }else if(field.getType() == double.class){
                    field.set(t,json.getDoubleValue(fieldName));
                }else if(field.getType() == String.class){
                    field.set(t,json.getString(fieldName));
                }else if(field.getType() == int[].class){
                    Seperator seperator = field.getDeclaredAnnotation(Seperator.class);
                    if(seperator == null){
                        field.set(t, JsonUtils.jsonToIntArr(json.getJSONArray(fieldName)));
                    }else{
                        if(StringUtils.isEmpty(json.getString(fieldName))){
                            field.set(t,new int[0]);
                        }else{
                            field.set(t,StringUtils.getIntArray(json.getString(fieldName),seperator.value()));
                        }
                    }
                }else if(field.getType() == float[].class){
                    Seperator seperator = field.getDeclaredAnnotation(Seperator.class);
                    if(seperator == null){
                        field.set(t,JsonUtils.jsonToFloatArr(json.getJSONArray(fieldName)));
                    }else{
                        field.set(t,StringUtils.getFloatArray(json.getString(fieldName),seperator.value()));
                    }
                }else if(field.getType() == String[].class){
                    Seperator seperator = field.getDeclaredAnnotation(Seperator.class);
                    if(seperator == null){
                        field.set(t,JsonUtils.jsonToStringArr(json.getJSONArray(fieldName)));
                    }else{
                        field.set(t,StringUtils.getStringList(json.getString(fieldName),seperator.value()));
                    }
                }else if(field.getType() == JSONArray.class){
                    field.set(t,json.getJSONArray(fieldName));
                }else if(field.getType() == List.class){
                    Class<? extends JsonTemplate> sub = field.getDeclaredAnnotation(JsonClass.class).value();
                    JSONArray array = json.getJSONArray(fieldName);
                    List list = new ArrayList();
                    array.forEach(o->list.add(JsonTemplate.parse(sub,(JSONObject) o,null)));
                    field.set(t,list);
                }
            }else{
                if(field.getType() == List.class){
                    ListLength lengthAnno = field.getDeclaredAnnotation(ListLength.class);
                    if(lengthAnno != null){
                        int length = lengthAnno.length();
                        String serial = lengthAnno.serial();
                        Class<? extends JsonTemplate> sub = field.getDeclaredAnnotation(JsonClass.class).value();
                        List list = new ArrayList();
                        for(int i=1;i<=length;i++){
                            JsonTemplate temp = JsonTemplate.parse(sub,json,serial.equals("char") ? (char)((int)'A' + i - 1)+"" : i+"");
                            if(temp == null){
                                continue;
                            }
                            list.add(temp);
                        }
                        field.set(t,list);
                    }
                }
            }
        }
    }

    static <T extends JsonTemplate> void checkTemplateExist(JsonTemplate invoker, Class<T> clazz, int id){
        if(TemplateService.INSTANCE.getTemplate(clazz,id) == null){
            error(invoker,clazz.getSimpleName()+" not exist:"+id);
        }
    }

    static void error(JsonTemplate invoker, String content){
        String error = "["+invoker.getClass().getSimpleName()+"-"+invoker.getId()+"]"+content;
        System.out.println(error);
        throw new RuntimeException(error);
    }

    interface Parser{

        <T extends JsonTemplate> T parse(Class<T> clazz, JSONObject json, String index);

    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD})
    @interface JsonClass {

        /**
         * 注释内容
         */
        Class<? extends JsonTemplate> value();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD})
    @interface Seperator {

        /**
         * 注释内容
         */
        String value();
    }


    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD})
    @interface ListLength {
        int length();

        String serial() default "num";
    }

}
