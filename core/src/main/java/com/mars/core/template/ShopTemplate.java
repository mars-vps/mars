package com.mars.core.template;


import lombok.Data;

//已废弃
@Data
public class ShopTemplate implements JsonTemplate {

    private int shopid;//商品id
    private int shopType;//商品类型     1：普通会员   2：高级会员
    private float rmb;//人民币
    private int rewardType;//充值奖励类型     1：分钟   2：小时    3：天
    private int chargeReward;//充值奖励值
    private String shopName;
    private String shopDes;
    @Override
    public int getId() {
        return shopid;
    }

    @Override
    public void check() {
        if (shopType == 0 || chargeReward == 0) {
            throw new IllegalArgumentException("Bad shopType or chargeReward:" + this);
        }
    }
}
