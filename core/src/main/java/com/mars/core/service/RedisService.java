package com.mars.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    private static String USER_KEY = "online_user";


    @Autowired
    private RedisTemplate redisTemplate;


    public void addRedis(String username, String msg) {
        redisTemplate.opsForHash().put(USER_KEY,username,msg);
    }

    public String getUserByUsername(String username) {
        return (String) redisTemplate.opsForHash().get(USER_KEY,username);
    }

    public void addRedisAndTime(String key,String value,long time) {
        redisTemplate.opsForValue().set(key,value,time,TimeUnit.MINUTES);
    }
    public void removeByKey(String key) {
        redisTemplate.delete(key);
    }
    public String getCode(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }
}
