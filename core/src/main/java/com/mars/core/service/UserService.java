package com.mars.core.service;

import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TUser;
import com.mars.core.dao.TUserDao;
import com.mars.core.utils.Loggers;
import com.mars.core.utils.RespCode;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class UserService {

    private String USER_ID_GENERATOR_KEY = "USERID";


    @Autowired
    private TUserDao userDao;
    @Autowired
    private RedisTemplate redisTemplate;

    public static RedisAtomicLong userIdGenerator;

    public void init(){
        if(!redisTemplate.hasKey(USER_ID_GENERATOR_KEY)){
            Long userId = userDao.getMaxId();
            if (userId == null) {
                userId = 0l;
            }
            userIdGenerator = new RedisAtomicLong(USER_ID_GENERATOR_KEY,redisTemplate.getConnectionFactory(),userId);
        }else {
            userIdGenerator = new RedisAtomicLong(USER_ID_GENERATOR_KEY,redisTemplate.getConnectionFactory());
        }
    }

    public String register(String username,String password){
        JSONObject obj = new JSONObject();
        try{
            TUser tUser = new TUser();
            tUser.setId(userIdGenerator.incrementAndGet());
            tUser.setUsername(username);
            tUser.setNickname(username);
            tUser.setPassword(password);
            tUser.setCreateTime(System.currentTimeMillis());

            String inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
            TUser userByInviteCode = findUserByInviteCode(inviteCode);
            while (userByInviteCode!=null){
                inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
                userByInviteCode = findUserByInviteCode(inviteCode);
            }
            tUser.setInviteCode(inviteCode);
            TUser user = userDao.save(tUser);
            if(user == null){
                obj.put("status",RespCode.HAS_USER);
                obj.put("msg","username has register");
                return obj.toString();
            }
            obj.put("status",RespCode.SUCCESS);
            obj.put("msg","register success");
            return obj.toString();
        }
        catch (Exception e){
            Loggers.webLogger.error(e.toString());
            obj.put("status",RespCode.HAS_USER);
            obj.put("msg","username has register");
            return obj.toString();
        }
    }




    public TUser findUserByUserId(long userId){
        return userDao.getTUserById(userId);
    }


    public TUser findUserByUserName(String userName){
        return userDao.getTUserByName(userName);
    }

    public TUser findUserByInviteCode(String inviteCode){
        return userDao.getTUserByInviteCode(inviteCode);
    }


    public void saveOrUpdate(TUser userById) {
        userDao.save(userById);
    }

    public List<TUser> findUserByVipLv(int vipLv) {
        if(vipLv<0){
            return userDao.getAllUser();
        }
        return userDao.findUserByVipLv(vipLv);
    }

    public List<TUser> findUserByNameAndVipLv(String userName, int vipLv) {
        if(vipLv<0){
            return userDao.findUserByNameLike(userName);
        }
        return userDao.findUserByNameAndVipLv(userName,vipLv);
    }

}
