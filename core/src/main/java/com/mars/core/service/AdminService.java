package com.mars.core.service;

import com.mars.core.bean.TAdmin;
import com.mars.core.dao.TAdminDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    @Autowired
    private TAdminDao adminDao;


    public TAdmin findByNameAndPassword(TAdmin admin) {
        return adminDao.findByNameAndPassword(admin.getName(),admin.getPassword());
    }
}
