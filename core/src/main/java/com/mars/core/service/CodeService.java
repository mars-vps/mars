package com.mars.core.service;

import com.mars.core.bean.TCode;
import com.mars.core.dao.TCodeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CodeService {

    @Autowired
    private TCodeDao codeDao;

    public TCode getCodeByCode(String code){
        return codeDao.getCodeByCode(code);
    }

    public void saveOrUpdate(TCode tCode) {
        codeDao.save(tCode);
    }

    public List<TCode> getAllData() {
        List<TCode> all = codeDao.getAllData();
        if(all==null||all.isEmpty()){
            return new ArrayList<TCode>();
        }
        return all;
    }

    public List<TCode> getDataByTypeAndStatus(int type, int status) {
        return codeDao.getCodeByStatusAndType(type,status);
    }

    public List<TCode> getDataByStatus(int status) {
        return codeDao.getCodeByStatus(status);
    }

    public List<TCode> getDataByType(int type) {
        return codeDao.getCodeByType(type);
    }

    public TCode getTCodeById(Long id) {
        return codeDao.getTCodeById(id);
    }

    public void deleteCode(Long id) {
        codeDao.deleteById(id);
    }

}
