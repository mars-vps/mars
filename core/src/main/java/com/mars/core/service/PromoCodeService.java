package com.mars.core.service;

import com.mars.core.bean.TCode;
import com.mars.core.bean.TPromoCode;
import com.mars.core.dao.TCodeDao;
import com.mars.core.dao.TPromoCodeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PromoCodeService {

    @Autowired
    private TPromoCodeDao promoCodeDao;

    public TPromoCode getPromoCodeByCode(String code){
        return promoCodeDao.getPromoCodeByCode(code);
    }

    public void saveOrUpdate(TPromoCode tCode) {
        promoCodeDao.save(tCode);
    }

    public List<TPromoCode> getAllData() {
        List<TPromoCode> allData = promoCodeDao.getAllData();
        if(allData==null||allData.isEmpty()){
            return new ArrayList<TPromoCode>();
        }
        return allData;
    }


    public List<TPromoCode> getDataByStatus(int status) {
        return promoCodeDao.getPromoCodeByStatus(status);
    }


    public TPromoCode getTPromoCodeById(Long id) {
        return promoCodeDao.getTPromoCodeById(id);
    }

    public void deleteCode(Long id) {
        promoCodeDao.deleteById(id);
    }

}
