package com.mars.core.service;

import com.mars.core.bean.TShop;
import com.mars.core.dao.TShopDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShopService {

    @Autowired
    private TShopDao shopDao;


    public TShop getShopById(long shopId){
        if(shopDao.existsById(shopId)){
            return shopDao.getShopById(shopId);
        }
        return null;
    }

    public List<TShop> getAllShops(){
        return shopDao.getAllShops(TShop.STATE_ONLINE);
    }

    public List<TShop> getAllData() {
        List<TShop> all = shopDao.getAllData();
        if(all==null||all.isEmpty()){
            return new ArrayList<TShop>();
        }
        return all;
    }


    public void saveOrUpdate(TShop shop) {
        shopDao.save(shop);
    }

    public void deleteCode(Long id) {
        shopDao.deleteById(id);
    }

}
