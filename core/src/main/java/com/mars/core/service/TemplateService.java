package com.mars.core.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.spring.ApplicationContextInit;
import com.mars.core.spring.SpringUtil;
import com.mars.core.template.JsonTemplate;
import com.mars.core.utils.FileUtil;
import com.mars.core.utils.Loggers;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.context.SpringContextUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
@Data
public class TemplateService {
    public static TemplateService INSTANCE;

    public static boolean loadInner = false;
    public static String jsonPath = "json/";
    public static String jsonPath1 = "D:/ddd/";
    public static String jsonPath2 = "/data/deploy/loadFile/";

    private Map<Class<? extends JsonTemplate>, Map<Integer, ? extends JsonTemplate>> allTemps;

    @PostConstruct
    public void startup(){
        INSTANCE = this;
        allTemps = new HashMap<>();
        try {
            //load(ShopTemplate.class, "ConfShop.json");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public <T extends JsonTemplate> void load(Class<T> clazz, String file) throws Exception {
        Map<Integer, T> map = new HashMap<>();
        allTemps.put(clazz, map);
        JSONArray arr = load(file);
        arr.forEach(o -> {
            T temp = JsonTemplate.parse(clazz, (JSONObject) o);
            map.put(temp.getId(), temp);
        });
    }

    public <T extends JsonTemplate> Map<Integer, T> getFileData(Class<T> clazz, String file) throws Exception {
        Map<Integer, T> map = new HashMap<>();
        JSONArray arr = loadaaa(file);
        arr.forEach(o -> {
            T temp = JsonTemplate.parse(clazz, (JSONObject) o);
            map.put(temp.getId(), temp);
        });
        return map;
    }

    public <T extends JsonTemplate> Map<Integer, T> initData(Class<T> clazz, JSONArray arr) throws Exception {
        Map<Integer, T> map = new HashMap<>();
        arr.forEach(o -> {
            T temp = JsonTemplate.parse(clazz, (JSONObject) o);
            map.put(temp.getId(), temp);
        });
        return map;
    }



    public <T extends JsonTemplate> Map<Integer, T> getMap(Class<T> clazz) {
        return (Map<Integer, T>) allTemps.get(clazz);
    }

    public <T extends JsonTemplate> T getTemplate(Class<T> clazz, int id) {
        Map<Integer, T> map = getMap(clazz);
        return map == null ? null : map.get(id);
    }


    public JSONArray load(String path) throws Exception {
        path = jsonPath + path;
        return loadInner ? FileUtil.loadJsonFile(path) : FileUtil.loadOuterFile(path);
    }

    public JSONArray loadaaa(String path) throws Exception {
        if(SpringUtil.isProfile("dev")){
            path = jsonPath1 + path;
        }else if(SpringUtil.isProfile("prod")){
            path = jsonPath2 + path;
        }
        Loggers.adminLogger.info("loadFile,path:{}",path);
        return loadInner ? FileUtil.loadJsonFile(path) : FileUtil.loadOuterFile(path);
    }


}
