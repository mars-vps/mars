package com.mars.core.service;

import com.mars.core.bean.TSystem;
import com.mars.core.dao.TSystemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SystemService {

    @Autowired
    private TSystemDao systemDao;

    public String getVersionData(){
        TSystem system = systemDao.getVersionData();
        if(system == null){
            system = new TSystem();
        }
        return system.toJson();
    }

    public TSystem getReleaseData(){
        return systemDao.getVersionData();
    }

    public List<TSystem> getAllVersionData() {
        List<TSystem> allVersionData = systemDao.getAllVersionData();
        if(allVersionData==null){
            return new ArrayList<TSystem>();
        }
        return allVersionData;
    }
    public TSystem getData(){
        return systemDao.getVersionData();
    }

    public void saveOrUpdate(TSystem system) {
        if(system.getId()==null || system.getId()<=0){
            Integer maxId = systemDao.getMaxId();
            if(maxId!=null&&maxId>0){
                system.setId(maxId+1);
            }else{
                system.setId(1);
            }
        }
        systemDao.save(system);
    }


    public TSystem getDataById(Integer id) {
        return systemDao.getDataById(id);
    }

    public void deleteDataById(Integer id) {
        systemDao.deleteById(id);
    }

}
