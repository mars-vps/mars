package com.mars.core.service;

import com.mars.core.bean.TOrder;
import com.mars.core.dao.TOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private TOrderDao orderDao;

    public void save(TOrder order){
        orderDao.save(order);
    }


    public TOrder getOrderById(String orderNo){
        if(orderDao.existsById(orderNo)){
            return orderDao.getOrderById(orderNo);
        }
        return null;
    }

    public List<TOrder> getOrderByUserId(long userId){
        return orderDao.findByRoleId(userId);
    }


    public List<TOrder> getAllData() {
        List<TOrder> all = orderDao.getAllData();
        if(all==null||all.isEmpty()){
            return new ArrayList<TOrder>();
        }
        return all;
    }


    public List<TOrder> getOrderByRoleId(String roleId) {
        List<TOrder> all = new ArrayList<>();
        List<TOrder> orderById = orderDao.getOrderByRoleId(Long.parseLong(roleId));
        if(orderById!=null){
            all.addAll(orderById);
        }
        return all;
    }

    public List<TOrder> getOrderByPlatformUid(String platformUid) {
        List<TOrder> all = new ArrayList<>();
        List<TOrder> orderById = orderDao.getOrderByPlatformUid(platformUid);
        if(orderById!=null && !orderById.isEmpty()){
            all.addAll(orderById);
        }
        return all;
    }

    public List<TOrder> getOrderByState(int state) {
        List<TOrder> all = new ArrayList<>();
        List<TOrder> orderById = orderDao.getOrderByState(state);
        if(orderById!=null && !orderById.isEmpty()){
            all.addAll(orderById);
        }
        return all;
    }

    public List<TOrder> getOrderByPlatformUidAndState(String platformUid,int state) {
        List<TOrder> all = new ArrayList<>();
        List<TOrder> orderById = orderDao.getOrderByPlatformUidAndState(platformUid,state);
        if(orderById!=null && !orderById.isEmpty()){
            all.addAll(orderById);
        }
        return all;
    }


    public List<TOrder> getOrders() {
        return orderDao.getOrders();
    }
}
