package com.mars.core.service;

import com.mars.core.bean.TNotice;
import com.mars.core.dao.TNoticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoticeService {

    @Autowired
    private TNoticeDao noticeDao;

    public TNotice getReleaseNotice(){
        Long maxId = noticeDao.getMaxId();
        if(maxId==null){
            return null;
        }
        return noticeDao.getTNoticeById(maxId);
    }

    public List<TNotice> getAllData() {
        List<TNotice> all = noticeDao.getAllData();
        if(all==null||all.isEmpty()){
            return new ArrayList<TNotice>();
        }
        return all;
    }


    public void saveOrUpdate(TNotice notice) {
        if(notice.getId()==null || notice.getId()<=0){
            Long maxId = noticeDao.getMaxId();
            if(maxId!=null&&maxId>0){
                notice.setId(maxId+1);
            }else{
                notice.setId(1l);
            }
        }
        noticeDao.save(notice);
    }


    public TNotice getTNoticeById(Long id) {
        return noticeDao.getTNoticeById(id);
    }

    public void deleteLine(Long id) {
        noticeDao.deleteById(id);
    }

}
