package com.mars.core.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TLine;
import com.mars.core.dao.TLineDao;
import com.mars.core.myenum.VIPLevelEnum;
import com.mars.core.utils.Base64;
import com.mars.core.utils.Loggers;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Service
public class LineService {

    @Autowired
    private TLineDao lineDao;

    public String getAllLine(int versionCode){
        JSONArray arr = new JSONArray();
        Iterable<TLine> all = lineDao.getLines();
        all.forEach(line->{
            line.decode();
            JSONObject obj = new JSONObject();
            obj.put("id",line.getId());
            obj.put("name",line.getName());
            obj.put("port",line.getPort());
            obj.put("host",line.getHost());
            obj.put("lv",line.getLv());
            obj.put("vipName", VIPLevelEnum.getVIPLevelEnumById(Integer.parseInt(line.getLv())));
            obj.put("type",line.getType());
            if(line.getType()>=5){
                if(versionCode>=134000){
                    arr.add(obj);
                }
            }else{
                arr.add(obj);
            }

        });
        return arr.toString();
    }
    public TLine getLineById(long lineId){
        return lineDao.getLineById(lineId);
    }


    public List<TLine> getAllLines() {
        List<TLine> all = lineDao.getAllLine();
        if(all==null||all.isEmpty()){
            return new ArrayList<TLine>();
        }
        all.forEach(t->t.decode());
        return all;
    }


    public void saveOrUpdate(TLine line) {
        line.encode();
        if(line.getId()==null || line.getId()<=0){
            Long maxId = lineDao.getMaxId();
            if(maxId!=null&&maxId>0){
                line.setId(maxId+1);
            }else{
                line.setId(1l);
            }
        }
        lineDao.save(line);
    }


    public TLine findLineById(Long id) {
        TLine lineById = lineDao.getLineById(id);
        if(lineById!=null){
            lineById.decode();
        }
        return lineById;
    }

    public void deleteLine(Long id) {
        lineDao.deleteById(id);
    }

    public void deleteAllLine() {
        lineDao.deleteAll();
    }



    @SuppressWarnings("unchecked")
    public void v2rayList(HttpServletResponse response) throws Exception {
        List<TLine> phoneList = lineDao.getAllLine();

        // 导出文件
        response.setContentType("text/plain");
        String fileName = "v2rayList";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".txt");
        BufferedOutputStream buff = null;
        StringBuffer write = new StringBuffer();
        String enter = "\r\n";
        ServletOutputStream outSTr = null;
        try {
            outSTr = response.getOutputStream(); // 建立
            buff = new BufferedOutputStream(outSTr);

            // 把内容写入文件
            if (phoneList.size() > 0) {
                for (int i = 0; i < phoneList.size(); i++) {
                    String info = getInfo(phoneList.get(i));
                    if(StringUtils.isEmpty(info)){
                        continue;
                    }
                    write.append(info);
                    write.append(enter);
                }
            }
            String encode = Base64.encode(write.toString().getBytes());
            buff.write(encode.getBytes("UTF-8"));
            buff.flush();
            buff.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                buff.close();
                outSTr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static String getInfo(TLine line){
        String fm = "{\"v\":2,\"ps\":\"%s\",\"add\":\"%s\",\"port\":%d,\"id\":\"%s\",\"aid\":%d,\"net\":\"%s\",\"type\":\"%s\",\"host\":\"\",\"path\":\"%s\",\"tls\":\"%s\"}";
        String password = line.getPassword();
        String[] split = password.split("::",-1);
        if(split.length<=1){
            return "";
        }
        String format = null;
        try {
            String encodedName = URLDecoder.decode(line.getName(),"utf8");//URLEncoder.encode(line.getName()).replaceAll("\\+", "%20");
            format = String.format(fm,encodedName, line.getHost(), line.getPort(), split[0], Integer.parseInt(split[1]), split[2], split[3], split[5],split[6]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "vmess://"+ Base64.encode(format.getBytes());
    }

    public static void main(String[] args) {
        TLine l = new TLine();
        l.setName("台湾 Hinet");
        l.setHost("vtljeueadfegrtg.necovm.cf");
        l.setPort(34301);
        l.setPassword("f299184b-19e0-41a4-9b4b-65fa8f0e691c::64::ws::none::::/jj::");
        String info = getInfo(l);
        System.out.println(info+"\r\n");

    }


    public List<TLine> getLinesByRemark(String key) {
        List<TLine> all = lineDao.getLinesByRemark(key);
        if(all==null||all.isEmpty()){
            return new ArrayList<TLine>();
        }
        all.forEach(t->t.decode());
        return all;
    }
}
