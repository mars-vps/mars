package com.mars.web.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.mars.core.bean.TUser;
import com.mars.core.service.UserService;
import com.mars.core.utils.Loggers;
import com.mars.core.utils.MD5Util;
import com.mars.core.utils.RespCode;
import com.mars.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

public class AuthenticationInterceptor implements HandlerInterceptor {
    public static String code1 = "D73543C56C6F295593F260CC65AB6EB3DF23ED8D";
    public static String code2 = "879FD622685944CA4CF46284B857CC9122565DC5";

    @Autowired
    UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {






        String token = httpServletRequest.getHeader("token");// 从 http 请求头中取出 token
                 // 如果不是映射到方法直接通过
                 if(!(object instanceof HandlerMethod)){
                         return true;
                     }
                 String versionCode = httpServletRequest.getHeader("versionCode");
                 if(StringUtils.isEmpty(versionCode)){
                     versionCode = "0";
                 }
                 String sec = httpServletRequest.getHeader("sec");
                 long currentTimeMillis = System.currentTimeMillis();

                 HandlerMethod handlerMethod=(HandlerMethod)object;
                 Method method=handlerMethod.getMethod();
                 String name = method.getName();
                 if(!StringUtils.isEmpty(name) && !(name.contains("smSubmitOrder") || name.contains("smcallback") || name.contains("lySubmitOrder") || name.contains("paySuccess") || name.contains("yszc") || name.equals("share"))){
                     if(!checkSSec(sec,Long.parseLong(versionCode),currentTimeMillis)){
                         Loggers.webLogger.info("sec: "+sec+" , versionCode:" + versionCode +" , checkSSec:"+checkSSec(sec,Long.parseLong(versionCode),currentTimeMillis) + " , currentTimeMillis:"+ currentTimeMillis +" , methodName:"+method.getName());
                         return false;
                     }
                 }

                 String uu = token==null?"null":JWT.decode(token).getAudience().get(0);
                 Loggers.webLogger.info("methodName: '"+method.getName()+"' , userId:" + uu + " , ip: "+ getIpAddr(httpServletRequest) +" , token:"+token);

                 //检查是否有passtoken注释，有则跳过认证
                 if (method.isAnnotationPresent(PassToken.class)) {
                         PassToken passToken = method.getAnnotation(PassToken.class);
                         if (passToken.required()) {
                                 return true;
                             }
                     }
                 //检查有没有需要用户权限的注解
                 if (method.isAnnotationPresent(UserLoginToken.class)) {
                         UserLoginToken userLoginToken = method.getAnnotation(UserLoginToken.class);
                         if (userLoginToken.required()) {
                                 // 执行认证
                                 if (token == null) {
                                         //throw new RuntimeException("无token，请重新登录");
                                     JSONObject obj = new JSONObject();
                                     obj.put("status", RespCode.TOKEN_NOT_EXIST);
                                     obj.put("msg","无token，请重新登录");
                                        onFail(httpServletResponse,obj.toString());
                                         return false;
                                     }
                                 // 获取 token 中的 user id
                                 long userId;
                                 try {
                                         userId = Long.parseLong(JWT.decode(token).getAudience().get(0));
                                     } catch (JWTDecodeException j) {
                                         JSONObject obj = new JSONObject();
                                         obj.put("status",RespCode.TOKEN_ERROR);
                                         obj.put("msg","token不合法");
                                         onFail(httpServletResponse,obj.toString());
                                         return false;
                                     }
                                 TUser user = userService.findUserByUserId(userId);
                                 if (user == null) {
                                         //throw new RuntimeException("用户不存在，请重新登录");
                                     JSONObject obj = new JSONObject();
                                     obj.put("status",RespCode.TOKEN_ERROR);
                                     obj.put("msg","token不合法");
                                     onFail(httpServletResponse,obj.toString());
                                     return false;
                                 }

                                 checkInviteCode(user);
                                 // 验证 token
                                 JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(user.getPassword())).build();
                                 try {
                                         jwtVerifier.verify(token);
                                     } catch (JWTVerificationException e) {
                                         //throw new RuntimeException("401");
                                     JSONObject obj = new JSONObject();
                                     obj.put("status",RespCode.TOKEN_ERROR);
                                     obj.put("msg","token不合法");
                                     onFail(httpServletResponse,obj.toString());
                                     return false;
                                    }
                                 return true;
                             }
                     }
                 return true;
    }

    private void checkInviteCode(TUser user) {
        if(user.getInviteCode()==null || "".equals(user.getInviteCode())){
            String inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
            TUser userByInviteCode = userService.findUserByInviteCode(inviteCode);
            while (userByInviteCode!=null){
                inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
                userByInviteCode = userService.findUserByInviteCode(inviteCode);
            }
            user.setInviteCode(inviteCode);
            userService.saveOrUpdate(user);
        }
    }


    public void onFail(HttpServletResponse response, String msg) throws IOException {
        response.reset();
        //设置编码格式
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        pw.write(msg);
        pw.flush();
        pw.close();
    }


    public String getIpAddr(HttpServletRequest request) {


        // 优先取 X-Real-IP
        String ip = request.getHeader("X-Real-IP");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
            ip = request.getHeader("x-forwarded-for");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
        {
            ip = request.getRemoteAddr();
            if ("0:0:0:0:0:0:0:1".equals(ip))
            {
                ip = "0:0:0:0:0:0:0:1";
            }
        }
        if ("unknown".equalsIgnoreCase(ip))
        {
            ip = "unknown";
            return ip;
        }
        int index = ip.indexOf(',');
        if (index >= 0)
        {
            ip = ip.substring(0, index);
        }

        return ip;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    private boolean checkSSec(String sec, long versionCode,long currentTimeMillis) {
        //MD5(MD5(SHA1签名) + System.current / 60000 + versionCode / 10)
        //D7:35:43:C5:6C:6F:29:55:93:F2:60:CC:65:AB:6E:B3:DF:23:ED:8D
        //87:9F:D6:22:68:59:44:CA:4C:F4:62:84:B8:57:CC:91:22:56:5D:C5
        if(StringUtils.isEmpty(sec) || versionCode<=0){
            return false;
        }
        String md5String = MD5Util.createMD5String(MD5Util.createMD5String(code1.toLowerCase()) +  currentTimeMillis/ 60000 + versionCode / 10);
        String md5String2 = MD5Util.createMD5String(MD5Util.createMD5String(code2.toLowerCase()) + currentTimeMillis / 60000 + versionCode / 10);


        String md5String3 = MD5Util.createMD5String(MD5Util.createMD5String(code1.toLowerCase()) +  (currentTimeMillis+60000)/ 60000 + versionCode / 10);
        String md5String4 = MD5Util.createMD5String(MD5Util.createMD5String(code2.toLowerCase()) + (currentTimeMillis+60000) / 60000 + versionCode / 10);

        String md5String5 = MD5Util.createMD5String(MD5Util.createMD5String(code1.toLowerCase()) +  (currentTimeMillis-60000)/ 60000 + versionCode / 10);
        String md5String6 = MD5Util.createMD5String(MD5Util.createMD5String(code2.toLowerCase()) + (currentTimeMillis-60000) / 60000 + versionCode / 10);



        if(!md5String.equals(sec) && !md5String2.equals(sec) && !md5String3.equals(sec) && !md5String4.equals(sec) && !md5String5.equals(sec) && !md5String6.equals(sec)){
            return false;
        }
        return true;
    }
}
