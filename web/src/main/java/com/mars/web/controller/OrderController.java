package com.mars.web.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.*;
import com.mars.core.constant.PPConst;
import com.mars.core.dao.TSystemDao;
import com.mars.core.myenum.MaterialPayEnum;
import com.mars.core.service.*;
import com.mars.core.spring.SpringUtil;
import com.mars.core.utils.*;
import com.mars.web.interceptor.PassToken;
import com.mars.web.interceptor.UserLoginToken;
import com.mars.web.utils.HttpClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
@CrossOrigin(allowedHeaders = "*",origins = "*",maxAge = 3600,allowCredentials = "true")
@Controller
@EnableScheduling
@RequestMapping("/order")
public class OrderController {

    private static final String app_id =  "q9aktt5g16qaobkprn38";
    private static final String appSecret = "Tole";
    private static final String bsaeUrl = "https://api.idontknowhowtosayitijustwanteverythingisgoingsmoothly.xyz/api/payment/";
    private static final int SM_PID =  10300;
    private static final String SM_TOKEN =  "1aOyY22Y88zWag0gbMMH1Wt8hG5bOImH";
    private static final String SM_BASEURL =  "https://smpay.io/";

    private  static  String cookie = "";


    private static Logger logger = Loggers.webLogger;

    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CodeService codeService;
    @Autowired
    private ShopService shopService;
    @Autowired
    private SystemService systemService;
    @Autowired
    private PromoCodeService promoCodeService;

    @RequestMapping(value = "/callback")
    @ResponseBody
    @PassToken
    public String callback(@RequestBody String data) {
        logger.info("callback data:{}",data);


        /*a{"outTradeNo":"9e57d7cda0a843008e1a1fc98f122712","payAmount":0.01,
                "payType":"ALIPAY_WEB","tradeNo":"202004132253200554",
                "tradeStatus":"TRADE_SUCCESS","time":"2020-04-13 22:54:23"}*/
        JSONObject result = new JSONObject();
        JSONObject obj = JSONObject.parseObject(data);
        /*String sign = getSign(obj);
        if(!obj.getString("sign").equals(sign)){
            logger.error("callback error,sign:{},localSign:{}",obj.getString("sign"),sign);
            return "fail";
        }*/
        String orderNo = obj.getString("outTradeNo");
        TOrder order = orderService.getOrderById(orderNo);
        if(order==null){
            logger.error("callback error,order is null; orderNo:{}",orderNo);
            return "fail";
        }
        //订单状态
        if(order.getState() != TOrder.STATE_INIT){
            //已经是其他状态，返回成功
            logger.info("TOrder:"+order+",state not init:"+data);
            result.put("status",200);
            return result.toString();
        }
        TUser player = userService.findUserByUserId(order.getRoleId());
        if(player==null){
            logger.error("callback error,user is null;userId:{}",order.getRoleId());
            return "fail";
        }
        checkInviteCode(player);
        TShop shopTemplate = shopService.getShopById(order.getChargeId());
        if(shopTemplate == null || shopTemplate.getStatus() == TShop.STATE_OFFLINE){
            logger.error("callback error,ShopTemplate is null;ShopTemplateId:{}",order.getChargeId());
            return "fail";
        }


        float rmb = Float.parseFloat(order.getPayItem());
        if(obj.getFloatValue("payAmount") < rmb){
            logger.info("price is wrong,pay:{},should pay:{}",obj.getFloatValue("payAmount"),rmb);
            return "fail";
        }

        order.setOrderData(data);
        order.setFinishTime(System.currentTimeMillis());
        String tradeStatus = obj.getString("tradeStatus");
        if(tradeStatus.equals("TRADE_SUCCESS") || tradeStatus.equals("WAIT_FOR_CALLBACK")){
            order.setState(TOrder.STATE_AUTH_SUCCESS);
        }else{
            order.setState(TOrder.STATE_AUTH_FAIL);
        }

        orderService.save(order);

        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            fahuo(player,shopTemplate);
        }
        result.put("status",200);
        return result.toString();
    }

    @RequestMapping(value = "/smcallback")
    @ResponseBody
    @PassToken
    public String smcallback(@RequestParam() int pid,
                             @RequestParam() String trade_no,
                             @RequestParam() String out_trade_no,
                             @RequestParam() String type,
                             @RequestParam() String name,
                             @RequestParam() String money,
                             @RequestParam() String trade_status,
                             @RequestParam() String sign,
                             @RequestParam() String sign_type
                             ) {
        logger.info("smcallback pid:{},trade_no:{},out_trade_no:{},type:{},name:{},money:{},trade_status:{},sign:{},sign_type:{},",pid,trade_no,out_trade_no,type,name,money,trade_status,sign,sign_type);

        boolean auth = checkSign(pid,trade_no,out_trade_no,type,name,money,trade_status,sign,sign_type);
        if(!auth){
            logger.error("smcallback error,sign error.");
            return "fail";
        }

        String orderNo = out_trade_no;
        TOrder order = orderService.getOrderById(orderNo);
        if(order==null){
            logger.error("smcallback error,order is null; orderNo:{}",orderNo);
            return "fail";
        }
        //订单状态
        if(order.getState() != TOrder.STATE_INIT){
            //已经是其他状态，返回成功
            /*logger.info("TOrder:"+order+",state not init:"+data);
            result.put("status",200);*/
            logger.error("order status is not TOrder.STATE_INIT.curState:{}",order.getState());
            return "SUCCESS";
        }
        TUser player = userService.findUserByUserId(order.getRoleId());
        if(player==null){
            logger.error("callback error,user is null;userId:{}",order.getRoleId());
            return "fail";
        }
        checkInviteCode(player);
        TShop shopTemplate = shopService.getShopById(order.getChargeId());
        if(shopTemplate == null || shopTemplate.getStatus() == TShop.STATE_OFFLINE){
            logger.error("callback error,ShopTemplate is null;ShopTemplateId:{}",order.getChargeId());
            return "fail";
        }


        float rmb = Float.parseFloat(order.getPayItem());
        if(Float.parseFloat(money) < rmb){
            logger.info("price is wrong,pay:{},should pay:{}",money,rmb);
            return "fail";
        }

        JSONObject result = new JSONObject();
        result.put("pid",pid);
        result.put("trade_no",trade_no);
        result.put("out_trade_no",out_trade_no);
        result.put("type",type);
        result.put("name",name);
        result.put("money",money);
        result.put("trade_status",trade_status);
        result.put("sign",sign);
        result.put("sign_type",sign_type);
        order.setOrderData(result.toString());
        order.setFinishTime(System.currentTimeMillis());
        order.setPlatformOrderId(trade_no);
        if(trade_status.equals("TRADE_SUCCESS")){
            order.setState(TOrder.STATE_AUTH_SUCCESS);
        }else{
            order.setState(TOrder.STATE_AUTH_FAIL);
        }

        orderService.save(order);

        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            fahuo(player,shopTemplate);
        }

        return "SUCCESS";
    }

    private static boolean checkSign(int pid, String trade_no, String out_trade_no, String type, String name, String money, String trade_status, String sign, String sign_type) {
        JSONObject obj = new JSONObject();
        obj.put("pid",pid);
        obj.put("trade_no",trade_no);
        obj.put("out_trade_no",out_trade_no);
        obj.put("type",type);
        obj.put("name",name);
        obj.put("money",money);
        obj.put("trade_status",trade_status);
        obj.put("sign",sign);
        obj.put("sign_type",sign_type);
        String smSign = getSMSign(obj);
        if(sign.equals(smSign)){
            return true;
        }
        return false;
    }


    @RequestMapping(value = "/paySuccess")
    @PassToken
    public String paySuccess(@RequestParam(required = false) int pid,
                             @RequestParam(required = false) String trade_no,
                             @RequestParam(required = false) String out_trade_no,
                             @RequestParam(required = false) String type,
                             @RequestParam(required = false) String name,
                             @RequestParam(required = false) String money,
                             @RequestParam(required = false) String trade_status,
                             @RequestParam(required = false) String sign,
                             @RequestParam(required = false) String sign_type) {
        logger.info("smcallback pid:{},trade_no:{},out_trade_no:{},type:{},name:{},money:{},trade_status:{},sign:{},sign_type:{},",pid,trade_no,out_trade_no,type,name,money,trade_status,sign,sign_type);

        return "statistics/success";
    }

    private void fahuo(TUser player ,TShop item) {
        checkInviteCode(player);
        player.setVipLv(item.getShopType());
        long ttl = 0l;
        switch (item.getRewardType()){
            case 1://分钟
                ttl = 60*1000l*item.getChargeReward();
                break;
            case 2://小时
                ttl = 60*60*1000l*item.getChargeReward();
                break;
            case 3://天
                ttl = 24*60*60*1000l*item.getChargeReward();
                break;
            default:break;
        }
        if(ttl<=0){
            logger.error("fahuo error,ShopTemplate:{}",item);
            return;
        }

        long currentTimeMillis = System.currentTimeMillis();
        if(player.getVipEndTime()>currentTimeMillis){
            player.setVipEndTime(player.getVipEndTime()+ttl);
        }
        else {
            player.setVipEndTime(currentTimeMillis+ttl);
        }
        userService.saveOrUpdate(player);
    }

    @RequestMapping(value = "/checkPromoCode")
    @UserLoginToken
    @ResponseBody
    public String checkPromoCode(@RequestParam("promoCode") String promoCode) {
        JSONObject result = new JSONObject();
        if(StringUtils.isEmpty(promoCode)){
            result.put("code",RespCode.PROMO_CODE_ERROR);
            result.put("msg","优惠码错误或无效");
            return result.toString();
        }
        TPromoCode promoCodeByCode = promoCodeService.getPromoCodeByCode(promoCode);
        if(promoCodeByCode==null){
            result.put("code",RespCode.PROMO_CODE_ERROR);
            result.put("msg","优惠码错误或无效");
            return result.toString();
        }
        if(promoCodeByCode.getStatus()==0 || promoCodeByCode.getEndTime()<=System.currentTimeMillis()){
            result.put("code",RespCode.PROMO_CODE_ERROR);
            result.put("msg","优惠码已过期");
            return result.toString();
        }
        result.put("code",RespCode.SUCCESS);
        result.put("msg",promoCodeByCode.getRemark());
        return result.toString();
    }



    @RequestMapping(value = "/genOrder", method = RequestMethod.GET)
    @UserLoginToken
    @ResponseBody
    //@PassToken
    public String genOrder(@RequestParam("userId") long userId,
                           @RequestParam("shopId") int shopId,
                           @RequestParam("payType") int payType,
                           @RequestParam(value = "promoCode",required = false) String promoCode) {
        logger.info("genOrder:userId:{},shopId:{},payType:{}",userId,shopId,payType);
        JSONObject result = new JSONObject();
        TUser player = userService.findUserByUserId(userId);
        if(player==null){
            logger.error("genOrder error,role is not exist:{}",userId);
            result.put("status", RespCode.USER_NOT_EXIST);
            result.put("msg","玩家不存在");
            return result.toString();
        }
        checkInviteCode(player);
        MaterialPayEnum item = MaterialPayEnum.getMaterialPayEnumById(payType);
        if(item==null){
            logger.error("genOrder error,payType  worng:{}",payType);
            result.put("status",RespCode.NO_PARAMS);
            result.put("msg","参数异常");
            return result.toString();
        }
        if(shopId<=0){
            logger.error("genOrder error,shopId  worng:{}",shopId);
            result.put("status",RespCode.NO_PARAMS);
            result.put("msg","参数异常");
            return result.toString();
        }

        TShop shopTemplate = shopService.getShopById(shopId);
        if(shopTemplate == null || shopTemplate.getStatus() == TShop.STATE_OFFLINE){
            logger.error("genOrder error,shopTemplate is null,shopId:{}",shopId);
            result.put("status",RespCode.NO_PARAMS);
            result.put("msg","参数异常");
            return result.toString();
        }

        float chajia = 0.00f;

        if(player.getVipLv()!=0){
            //用户购买低于本身等级的产品，直接不让买
            if(shopTemplate.getShopType()<player.getVipLv()){
                logger.error("genOrder error,shopTemplate.getShopType()!=player.getVipLv():{}",shopTemplate);
                result.put("status",RespCode.SHOP_ERROR);
                result.put("msg","您不可购买低于本身会员等级的产品，请到期后购买");
                return result.toString();
            }
            //用户会员等级低于产品等级，费用增加当前会员到最后
            if(shopTemplate.getShopType()>player.getVipLv()){
                int days = getFarToEnd(player.getVipEndTime());
                if(days<7){
                    logger.error("genOrder error,shopTemplate.getShopType()!=player.getVipLv():{}",shopTemplate);
                    result.put("status",RespCode.SHOP_ERROR);
                    result.put("msg","当前会员余期不足一周，请到期后再来购买。");
                    return result.toString();
                }
                JSONArray jsonArray = new JSONArray();
                TSystem releaseData = systemService.getReleaseData();
                if(releaseData==null || (StringUtils.isEmpty(releaseData.getBucha()))){
                    jsonArray.add(0.15);
                    jsonArray.add(0.45);
                    jsonArray.add(1);
                }else {
                    jsonArray = JSONArray.parseArray(releaseData.getBucha());
                }
                float perDayCha = 0.0f;
                for(int i=player.getVipLv();i<shopTemplate.getShopType();i++){
                    perDayCha += jsonArray.getFloatValue(i - 1);
                }
                chajia = perDayCha * days;
                /*logger.error("genOrder error,shopTemplate.getShopType()!=player.getVipLv():{}",shopTemplate);
                result.put("status",RespCode.SHOP_ERROR);
                result.put("msg","当前未开通会员升级功能，请等待当前会员到期后再来购买");
                return result.toString();*/
            }
        }
        float totalRmb = getPromoPrice(promoCode,shopTemplate.getRmb()) + chajia;
        BigDecimal b = new BigDecimal(totalRmb);
        float totalRmbb = b.setScale(2, BigDecimal.ROUND_HALF_DOWN).floatValue();
        //生成服务器订单
        TOrder order = createOrder(player.getId(),shopId,totalRmbb,item);
        orderService.save(order);
        result.put("status",RespCode.SUCCESS);
        result.put("msg","success");
        if(item.getSubType()==1){
            result.put("url","/order/submitOrder");
        }else if(item.getSubType()==2){
            result.put("url","/order/smSubmitOrder");
        }else if(item.getSubType()==3){
            result.put("url","/order/lySubmitOrder");
        }
        result.put("orderNo",order.getId());
        return result.toString();
    }

    private float getPromoPrice(String promoCode, float rmb) {
        if(StringUtils.isEmpty(promoCode)){
            return rmb;
        }
        TPromoCode promoCodeByCode = promoCodeService.getPromoCodeByCode(promoCode);
        if(promoCodeByCode==null){
            return rmb;
        }
        if(promoCodeByCode.getStatus()==0 || promoCodeByCode.getEndTime()<=System.currentTimeMillis()){
            return rmb;
        }
        return promoCodeByCode.getZhekou() * rmb;
    }


    private int getFarToEnd(long vipEndTime){
        long curTime = System.currentTimeMillis();
        if ((curTime+TimeUtils.DAY)>=vipEndTime){
            return 0;
        }
        return (int)((vipEndTime-curTime)/TimeUtils.DAY);
    }


    @RequestMapping(value = "/getGoods", method = RequestMethod.GET,produces="application/json;charset=utf-8")
    //@UserLoginToken
    @ResponseBody
    @PassToken
    public String getGoods() {
        JSONArray result = new JSONArray();
        List<TShop> allShops = shopService.getAllShops();
        for (TShop temp:allShops) {
            JSONObject tem = new JSONObject();
            tem.put("id",temp.getId());
            tem.put("shopName",temp.getShopName());
            tem.put("rmb",temp.getRmb());
            tem.put("shopDes",temp.getShopDes());
            result.add(tem);
        }
        JSONObject obj = new JSONObject();
        obj.put("shopList",result);

        JSONArray arr = new JSONArray();
        for (MaterialPayEnum item:MaterialPayEnum.values()) {
            if(item.getStatus() == 1){
                JSONObject tem = new JSONObject();
                tem.put("payType",item.getId());
                tem.put("name",item.getName());
                arr.add(tem);
            }
        }
        obj.put("payList",arr);

        return obj.toString();
    }

    @RequestMapping(value = "/getOrdersByUserId", method = RequestMethod.GET)
    @UserLoginToken
    @ResponseBody
    public String getOrdersByUserId(@RequestParam("userId") long userId) {
        logger.info("getOrdersByUserId,userId:{}",userId);
        JSONArray result = new JSONArray();
        List<TOrder> orderByUserId = orderService.getOrderByUserId(userId);
        if(orderByUserId!=null && !orderByUserId.isEmpty()){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            for (TOrder order:orderByUserId) {
                JSONObject obj = new JSONObject();
                obj.put("orderNo",order.getId());
                obj.put("platformOrderId",order.getPlatformOrderId());
                obj.put("payType",order.getPlatformUid());
                obj.put("status",order.getState());
                obj.put("rmb",order.getPayItem());
                TShop template = shopService.getShopById(order.getChargeId());
                obj.put("shopName",template==null?"该商品已下架":template.getShopName());
                long time = order.getFinishTime()>0?order.getFinishTime():order.getCreateTime();
                obj.put("time",simpleDateFormat.format(new Date(time)));
                result.add(obj);
            }
        }
        return result.toString();
    }


    @RequestMapping(value = "/submitOrder", method = RequestMethod.GET)
    @UserLoginToken
    public String submitOrder(@RequestParam("orderNo") String orderNo, Model model) {
        logger.info("submitOrder,orderNo:{}",orderNo);
        TOrder order = orderService.getOrderById(orderNo);
        if(order == null){
            logger.error("sumitOrder error,order is null,orderNo:{}",orderNo);
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "statistics/fail";
        }
        TShop shopById = shopService.getShopById(order.getChargeId());
        if(shopById == null){
            logger.error("sumitOrder error,shop is null,shopId:{}",order.getChargeId());
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "statistics/fail";
        }

        //向三方请求生成订单
        JSONObject obj = generateOrder(order.getId(),Float.parseFloat(order.getPayItem()),order.getPlatformUid());
        if(obj==null){
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "statistics/fail";
        }
        logger.info("sumitOrder result:{}",obj);
        if(obj.getIntValue("code")!=200){
            /*if(order.getPlatformUid().equals(MaterialPayEnum.WXPAY.getDes())){
                model.addAttribute("code",RespCode.SUCCESS);
                model.addAttribute("msg","http://www.baidu.com");
                return "statistics/wxpay";1
            }*/
            logger.error("sumitOrder error,result:{}",obj);
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "statistics/fail";
        }

        //三方生成订单成功
        JSONObject data = obj.getJSONObject("result");
        order.setPlatformOrderId(data.getString("tradeNo"));
        order.setState(TOrder.STATE_INIT);
        //保存本地服务器订单
        orderService.save(order);

        model.addAttribute("code",RespCode.SUCCESS);
        model.addAttribute("msg",data.getString("url"));
        String chajia = "0.00";
        BigDecimal b = new BigDecimal(shopById.getRmb());
        if(Float.parseFloat(order.getPayItem())>shopById.getRmb()){
            BigDecimal a = new BigDecimal(Float.parseFloat(order.getPayItem()));
            BigDecimal subtract = a.subtract(b);
            chajia = subtract.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
        }
        model.addAttribute("data1",shopById.getShopName()+"：");
        model.addAttribute("price1",b.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
        model.addAttribute("data2","VIP跨级补差价：");
        model.addAttribute("price2",chajia);
        model.addAttribute("total",order.getPayItem());


        if(order.getPlatformUid().equals(MaterialPayEnum.WXPAY.getDes())){
            return "statistics/wxpay";
        }else{
            return "statistics/pay";
        }


    }



    @RequestMapping(value = "/smSubmitOrder", method = RequestMethod.GET)
    @UserLoginToken
    public String smSubmitOrder(@RequestParam("orderNo") String orderNo, Model model) {
        logger.info("smSubmitOrder,orderNo:{}",orderNo);
        TOrder order = orderService.getOrderById(orderNo);
        if(order == null){
            logger.error("sumitOrder error,order is null,orderNo:{}",orderNo);
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "购买出错，请稍后再试";
        }
        TShop shopById = shopService.getShopById(order.getChargeId());
        if(shopById == null){
            logger.error("sumitOrder error,shop is null,shopId:{}",order.getChargeId());
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "购买出错，请稍后再试";
        }
        String result = generateSMOrder(String.valueOf(order.getChargeId()), order.getId(), order.getPayItem(), order.getPlatformUid());
        logger.info("smSubmitOrder result:{}",result);
        //三方生成订单成功
        //order.setPlatformOrderId(data.getString("tradeNo"));
        order.setState(TOrder.STATE_INIT);
        //保存本地服务器订单
        orderService.save(order);
        return "redirect:"+result;
    }

    @RequestMapping(value = "/lySubmitOrder", method = RequestMethod.GET)
    @UserLoginToken
    public String lySubmitOrder(@RequestParam("orderNo") String orderNo, Model model) {
        logger.info("lySubmitOrder,orderNo:{}",orderNo);
        TOrder order = orderService.getOrderById(orderNo);
        if(order == null){
            logger.error("sumitOrder error,order is null,orderNo:{}",orderNo);
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "购买出错，请稍后再试";
        }
        TShop shopById = shopService.getShopById(order.getChargeId());
        if(shopById == null){
            logger.error("sumitOrder error,shop is null,shopId:{}",order.getChargeId());
            model.addAttribute("code",RespCode.SANFANG_ERROR);
            model.addAttribute("msg","购买出错，请稍后再试");
            return "购买出错，请稍后再试";
        }
        String result = generateLYOrder(order.getPayItem(), order.getPlatformUid());
        logger.info("lySubmitOrder result:{}",result);
        if(StringUtils.isEmpty(result)){
            return "购买出错，请稍后再试";
        }
        JSONObject jsonObject = JSONObject.parseObject(result);
        //三方生成订单成功
        order.setPlatformOrderId(jsonObject.getString("pid"));
        order.setState(TOrder.STATE_INIT);
        //保存本地服务器订单
        orderService.save(order);
        model.addAttribute("url",jsonObject.getString("url"));
        return "statistics/tiaozhuan";
    }

    @RequestMapping(value = "/queryOrder", method = RequestMethod.GET)
    @ResponseBody
    @UserLoginToken
    public String queryOrder(@RequestParam("orderNo") String orderNo) {
        logger.info("queryOrder,orderNo:{}",orderNo);
        JSONObject result = new JSONObject();
        TOrder order = orderService.getOrderById(orderNo);
        if(order == null){
            logger.error("sumitOrder error,order is null,orderNo:{}",orderNo);
            result.put("status",RespCode.ORDER_NULL);
            result.put("msg","order is not exist");
            return result.toString();
        }
        if(order.getState()==TOrder.STATE_INIT){
            MaterialPayEnum materialPayEnumByDes = MaterialPayEnum.getMaterialPayEnumByDes(order.getPlatformUid());
            //MP支付处理
            if(materialPayEnumByDes!=null &&materialPayEnumByDes.getSubType()==1){
                JSONObject obj = queryOrder(orderNo,Float.parseFloat(order.getPayItem()),order.getPlatformUid());
                if(obj==null){
                    result.put("status",RespCode.PAY_FAIL);
                    result.put("msg","pay fail");
                    return result.toString();
                }
                if(obj.containsKey("result")){
                    JSONObject result1 = JSONObject.parseObject(obj.getString("result"));
                    if(result1.getString("tradeStatus").equals("TRADE_SUCCESS")){
                        result.put("status",RespCode.SUCCESS);
                        result.put("msg",obj.getString("msg"));
                        changeOrder(order,obj);
                        return result.toString();
                    }
                    else {
                        result.put("status",RespCode.PAY_FAIL);
                        result.put("msg","pay fail");
                        return result.toString();
                    }
                }else{
                    result.put("status",RespCode.SANFANG_ERROR);
                    result.put("msg",obj.getString("msg"));
                    return result.toString();
                }
            }
            //SM支付处理
            else if(materialPayEnumByDes!=null &&materialPayEnumByDes.getSubType()==2){
                //{"msg":"查询订单号成功！",
                // "code":1,
                // "out_trade_no":"a9d7916c0fc7413f99b90c059bfb224b",
                // "money":"1.00",
                // "addtime":"2020-08-04 11:42:49",
                // "name":"test",
                // "trade_no":"2020080411424989972",
                // "pid":10300,
                // "type":"wxpay",
                // "status":0}
                JSONObject  obj = querySMOrder(orderNo);
                if(obj!=null &&  obj.getIntValue("code")==1 && obj.getIntValue("status")==1){
                    result.put("status",RespCode.SUCCESS);
                    result.put("msg",obj.getString("msg"));
                    changeSMOrder(order,obj);
                    return result.toString();
                }else {
                    result.put("status",RespCode.PAY_FAIL);
                    result.put("msg","pay fail");
                    return result.toString();
                }
            }
            else if(materialPayEnumByDes!=null &&materialPayEnumByDes.getSubType()==3){
                //{"msg":"查询订单号成功！",
                // "code":1,
                // "out_trade_no":"0B46EF59",
                // "money":"1",
                // "addtime":"2020-11-16 13:21:52",
                // "name":"虚拟物品",
                // "trade_no":"2020111613215269536",
                // "pid":"10019",
                // "type":"alipay",
                // "status":"0"}
                JSONObject  obj = queryLYOrder(orderNo);
                if(obj!=null &&  obj.getIntValue("ret")==1){
                    if(obj.getIntValue("result")==1){
                        result.put("status",RespCode.SUCCESS);
                        result.put("msg","pay success");
                        changeLYOrder(order,obj);
                        return result.toString();
                    }
                    else {
                        if(System.currentTimeMillis() >= (order.getCreateTime()+TimeUtils.HOUR)){
                            order.setState(TOrder.STATE_AUTH_FAIL);
                        }
                        orderService.save(order);
                        result.put("status",RespCode.PAY_FAIL);
                        result.put("msg","pay fail");
                        return result.toString();
                    }
                }else {
                    if(System.currentTimeMillis() >= (order.getCreateTime()+TimeUtils.HOUR)){
                        order.setState(TOrder.STATE_AUTH_FAIL);
                    }
                    orderService.save(order);
                    result.put("status",RespCode.PAY_FAIL);
                    result.put("msg","pay fail");
                    return result.toString();
                }
            }
            //不存在的支付
            else {
                result.put("status",RespCode.PAY_FAIL);
                result.put("msg","pay fail");
                return result.toString();
            }
        }
        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            result.put("status",RespCode.SUCCESS);
            result.put("msg","pay success");
            return result.toString();
        }else{
            result.put("status",RespCode.PAY_FAIL);
            result.put("msg","pay fail");
            return result.toString();
        }
    }


    @RequestMapping(value = "/useCode", method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String useCode(@RequestParam("code") String code, @RequestParam("userId") long userId) {
        logger.info("useCode,code:{},userId:{}",code,userId);
        JSONObject result = new JSONObject();
        TCode tCode = codeService.getCodeByCode(code);
        if(tCode == null){
            logger.error("useCode error,code is not exist,code:{}",code);
            result.put("status",RespCode.TCODE_IS_NULL);
            result.put("msg","code is not exist");
            return result.toString();
        }
        if(tCode.getOutTime()>0 && System.currentTimeMillis()>=tCode.getOutTime()){
            logger.error("useCode error,code has out time,code:{}",code);
            result.put("status",RespCode.TCODE_OUTTIME);
            result.put("msg","code has time out");
            return result.toString();
        }
        if(tCode.getStatus()!=1){
            logger.error("useCode error,code has used,code:{}",code);
            result.put("status",RespCode.TCODE_OUTTIME);
            result.put("msg","code has used");
            return result.toString();
        }

        TUser userByUserId = userService.findUserByUserId(userId);
        if(userByUserId == null){
            logger.error("useCode error,user is not exist,userId:{}",userId);
            result.put("status",RespCode.USER_NOT_EXIST);
            result.put("msg","user is not exist");
            return result.toString();
        }

        String[] split = tCode.getReward().split("\\|");
        if(userByUserId.getVipLv()>0 && userByUserId.getVipLv()!=Integer.parseInt(split[0])){
            logger.error("useCode error,user vipLV is limit,userVIP:{},rewardLv:{}",userByUserId.getVipLv(),Integer.parseInt(split[0]));
            result.put("status",RespCode.SHOP_ERROR);
            result.put("msg","user vipLV is limit");
            return result.toString();
        }

        long vipEndTime = userByUserId.getVipEndTime();
        long curTime = System.currentTimeMillis();

        long ttl = 0;
        String msg = "use seccess,Congratulations : VIP ";
        switch (Integer.parseInt(split[1])){
            case 1://分钟
                ttl = 60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Minute";
                break;
            case 2://小时
                ttl = 60*60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Hour";
                break;
            case 3://天
                ttl = 24*60*60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Day";
                break;
            default:break;
        }


        if(vipEndTime<=curTime){
            userByUserId.setVipEndTime(curTime+ttl);
        }else{
            userByUserId.setVipEndTime(vipEndTime+ttl);
        }
        userByUserId.setVipLv(Integer.parseInt(split[0]));
        userService.saveOrUpdate(userByUserId);

        tCode.setUseTime(System.currentTimeMillis());
        tCode.setUserId(userId);
        tCode.setStatus(2);
        codeService.saveOrUpdate(tCode);

        result.put("status",RespCode.SUCCESS);
        result.put("msg",msg);
        return result.toString();

    }

    private void changeOrder(TOrder order,JSONObject obj) {
        JSONObject data = obj.getJSONObject("result");
        order.setOrderData(obj.toString());
        order.setFinishTime(System.currentTimeMillis());
        order.setState(data.getString("tradeStatus").equalsIgnoreCase("TRADE_SUCCESS")? TOrder.STATE_AUTH_SUCCESS:TOrder.STATE_AUTH_FAIL);
        orderService.save(order);
        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            TUser player = userService.findUserByUserId(order.getRoleId());
            TShop shopTemplate = shopService.getShopById(order.getChargeId());
            fahuo(player,shopTemplate);
        }
    }

    private void changeSMOrder(TOrder order,JSONObject obj) {
        //{"msg":"查询订单号成功！",
        // "code":1,
        // "out_trade_no":"a9d7916c0fc7413f99b90c059bfb224b",
        // "money":"1.00",
        // "addtime":"2020-08-04 11:42:49",
        // "name":"test",
        // "trade_no":"2020080411424989972",
        // "pid":10300,
        // "type":"wxpay",
        // "status":0}
        order.setOrderData(obj.toString());
        order.setFinishTime(System.currentTimeMillis());
        order.setState(TOrder.STATE_AUTH_SUCCESS);
        orderService.save(order);
        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            TUser player = userService.findUserByUserId(order.getRoleId());
            TShop shopTemplate = shopService.getShopById(order.getChargeId());
            fahuo(player,shopTemplate);
        }
    }
    private void changeLYOrder(TOrder order,JSONObject obj) {
        order.setOrderData(obj.toString());
        order.setFinishTime(System.currentTimeMillis());
        order.setState(TOrder.STATE_AUTH_SUCCESS);
        orderService.save(order);
        if(order.getState() == TOrder.STATE_AUTH_SUCCESS){
            TUser player = userService.findUserByUserId(order.getRoleId());
            TShop shopTemplate = shopService.getShopById(order.getChargeId());
            fahuo(player,shopTemplate);
        }
    }

    public static JSONObject queryOrder(String orderNo, float parseFloat, String payType) {
        JSONObject obj = new JSONObject();
        obj.put("appId",app_id);
        obj.put("outTradeNo",orderNo);
        obj.put("payAmount",parseFloat);
        obj.put("payType",payType);
        String sign = getSign(obj);
        obj.put("sign",sign);
        String url = bsaeUrl + "query";
        String result = HttpRequest.sendPost(url, obj.toString());
        if(result==null||"".equals(result)){
            return null;
        }
        JSONObject re = JSONObject.parseObject(result);
        return re;
    }

    public static JSONObject querySMOrder(String orderNo) {

        StringBuffer sb = new StringBuffer();
        sb.append("act").append("=").append("order").append("&");
        sb.append("key").append("=").append(SM_TOKEN).append("&");
        sb.append("out_trade_no").append("=").append(orderNo).append("&");
        sb.append("pid").append("=").append(SM_PID);

        String result = HttpRequest.sendGet(SM_BASEURL+"api.php", sb.toString());
        if(result==null||"".equals(result)){
            return null;
        }
        JSONObject re = JSONObject.parseObject(result);
        return re;
    }
    public static JSONObject queryLYOrder(String orderNo) {
        TOrder orderById = SpringUtil.getBean(OrderService.class).getOrderById(orderNo);
        if(orderById == null){
            return null;
        }
        if(StringUtils.isEmpty(orderById.getPlatformOrderId())){
            return null;
        }
        JSONObject obj = new JSONObject();
        obj.put("pid",orderById.getPlatformOrderId());
        obj.put("tradeno",orderById.getPlatformOrderId());
        String result = null;
        try {
            if(StringUtils.isEmpty(cookie)){
                cookie = HttpRequest.setLyCookie();
            }
            result = HttpClientUtils.sendPost(PPConst.URL_QUERY_LY,obj,cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(result==null||"".equals(result)){
            return null;
        }
        JSONObject re = JSONObject.parseObject(result);
        return re;
    }

    private TOrder createOrder(Long roleId, int shopId, float rmb,MaterialPayEnum item) {
        String bill_no = UUIDUtil.randomUUID();
        TOrder order= new TOrder();
        order.setId(bill_no);
        order.setRoleId(roleId);
        order.setChargeId(shopId);
        order.setPayItem(String.valueOf(rmb));
        order.setCreateTime(System.currentTimeMillis());
        order.setFinishTime(0);
        order.setPlatformId("materialPay");
        order.setPlatformUid(item.getDes());
        return order;
    }

    public static JSONObject generateOrder(String bill_no, float rmb, String payType) {
        JSONObject obj = new JSONObject();
        obj.put("appId",app_id);
        obj.put("outTradeNo",bill_no);
        obj.put("payAmount",rmb);
        obj.put("payType",payType);
        String sign = getSign(obj);
        obj.put("sign",sign);
        obj.put("returnUrl","https://api.vps-mars.monster/order/paySuccess");
        String url = bsaeUrl + "create";
        String result = HttpRequest.sendPost(url, obj.toString());
        if(result==null || "".equals(result)){
            return null;
        }
        JSONObject re = JSONObject.parseObject(result);
        return re;
    }

    public static String generateSMOrder(String shopName,String bill_no, String rmb, String payType) {

        JSONObject obj = new JSONObject();
        obj.put("pid",SM_PID);
        obj.put("type",payType);
        obj.put("out_trade_no",bill_no);
        obj.put("notify_url","https://api.vps-mars.monster/order/smcallback");
        obj.put("return_url","https://api.vps-mars.monster/order/paySuccess");
        obj.put("name",shopName);
        obj.put("money",rmb);
        obj.put("sitename","MarsVpn");
        String smSign = getSMSign(obj);

        StringBuffer sb = new StringBuffer();
        sb.append("pid=").append(SM_PID).append("&");
        sb.append("type=").append(payType).append("&");
        sb.append("out_trade_no=").append(bill_no).append("&");
        sb.append("notify_url=https://api.vps-mars.monster/order/smcallback&");
        sb.append("return_url=https://api.vps-mars.monster/order/paySuccess&");
        sb.append("name=").append(shopName).append("&");
        sb.append("money=").append(rmb).append("&");
        sb.append("sitename=MarsVpn&");
        sb.append("sign=").append(smSign).append("&");
        sb.append("sign_type=MD5");

        return SM_BASEURL + "submit.php?"+sb.toString();
    }
    public static String generateLYOrder(String rmb, String payType) {
        if(StringUtils.isEmpty(cookie)){
            cookie = HttpRequest.setLyCookie();
            System.out.println(cookie);
        }
        JSONObject obj = new JSONObject();
        obj.put("amount",rmb);
        obj.put("price",rmb);
        obj.put("type",payType);
        String result = HttpRequest.sendPostLY(PPConst.URL_PAY_LY, obj.toString(), cookie);

        return result;
    }


    private static String getSign(JSONObject obj) {
        StringBuffer sb = new StringBuffer();
        ArrayList<String> strings = new ArrayList<>(obj.keySet());
        Collections.sort(strings);
        for(String s : strings){
            if(s.equals("name") || s.equals("sign")){
                continue;
            }
            sb.append(s).append("=").append(obj.get(s)).append("&");
        }
        String data = sb.substring(0,sb.length()-1);
        System.out.println(data);
        return MD5Util.createMD5String(MD5Util.createMD5String(data).toLowerCase()+appSecret).toLowerCase();
    }
    private static String getSMSign(JSONObject obj) {
        ArrayList<String> strings = new ArrayList<>(obj.keySet());
        Collections.sort(strings);
        String sign = "";
        for (String s: strings) {
            if(s.equals("sign") || s.equals("sign_type")){
                continue;
            }
            sign += (s +"="+ obj.getString(s) + "&");
        }
        sign = sign.substring(0,sign.length()-1);
        //验证签名
        return MD5Util.createMD5String(sign+SM_TOKEN);
    }

    public static void main(String[] args) throws Exception{
        //expire_in=1607507174; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;ip=d8813907eb747ea7346a062e8cb6a546; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;key=60571ab36a007ed353424eaba8e8ac3e5d2213edf336d; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;email=vps.mars%40gmail.com; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;uid=3; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;__cfduid=d8ae8bd386c2b34e944b17b8a185153c71607420774; expires=Thu, 07-Jan-21 09:46:14 GMT; path=/; domain=.whales.zone; HttpOnly; SameSite=Lax;
        //{"code":0,"pid":"74AD63D3","url":"https://checkout.sknetpay.com/?trade_no=c6cad8940f4df3ded8d71e7785aa3562"}
        //{"code":0,"pid":"91EBB24D","url":"https://checkout.sknetpay.com/?trade_no=b84166309d67487638fad794469f0144"}
        //String code = "expire_in=1607507174; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;ip=d8813907eb747ea7346a062e8cb6a546; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;key=60571ab36a007ed353424eaba8e8ac3e5d2213edf336d; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;email=vps.mars%40gmail.com; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;uid=3; expires=Wed, 09-Dec-2020 09:46:14 GMT; Max-Age=86400; path=/;__cfduid=d8ae8bd386c2b34e944b17b8a185153c71607420774; expires=Thu, 07-Jan-21 09:46:14 GMT; path=/; domain=.whales.zone; HttpOnly; SameSite=Lax;";
        String code = "__cfduid=d20e3f375a873824dbfaa82ac02329d071607425902; uid=3; email=vps.mars%40gmail.com; key=b110768828cfe96b5dad85772a154d9ac5f06a1f37801; ip=b8f2be597d153694c54331496f30394a; expire_in=1607512380";
        JSONObject obj = JSONObject.parseObject("{\"url\":\"https:\\/\\/checkout.sknetpay.com\\/?trade_no=8ef302dd9a81381cbd148a7c9d579f9e\",\"code\":0,\"pid\":\"AB4DDAEA\"}");

        System.out.println(obj.get("url"));


        /*obj.put("amount",1);
        obj.put("price",1);
        obj.put("type","alipay");*/
        /*obj.put("pid","91EBB24D");
        obj.put("tradeno","91EBB24D");
        String s = HttpClientUtils.sendPost(PPConst.URL_QUERY_LY, obj, code);
        *//*String result = HttpRequest.sendPostLYQuery(PPConst.URL_QUERY_LY, obj.toString(), code);
        System.out.println(result);*//*
        System.out.println(s);*/
    }

    private void checkInviteCode(TUser user) {
        if(user.getInviteCode()==null || "".equals(user.getInviteCode())){
            String inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
            TUser userByInviteCode = userService.findUserByInviteCode(inviteCode);
            while (userByInviteCode!=null){
                inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
                userByInviteCode = userService.findUserByInviteCode(inviteCode);
            }
            user.setInviteCode(inviteCode);
            userService.saveOrUpdate(user);
        }
    }


    //每隔60分钟执行一次
    @Scheduled(cron = "0 0/60 * * * ?")
    public void scheduler60Min() {
        cookie = HttpRequest.setLyCookie();
        logger.info("time:{},newCookie:{}",System.currentTimeMillis(),cookie);
    }
    //每隔5分钟执行一次
    @Scheduled(cron = "0 0/5 * * * ?")
    public void scheduler5Min() {
        try{
            List<TOrder> orders = orderService.getOrders();
            if(orders!=null){
                Loggers.webLogger.error("scheduler1Min size:{}",orders.size());
            }
            for (TOrder o:orders) {
                queryOrder(o.getId());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}
