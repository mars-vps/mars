package com.mars.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.mars.core.bean.TLine;
import com.mars.core.bean.TSystem;
import com.mars.core.bean.TUser;
import com.mars.core.constant.ParamDataConst;
import com.mars.core.service.*;
import com.mars.core.utils.*;
import com.mars.web.interceptor.PassToken;
import com.mars.web.interceptor.UserLoginToken;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
@CrossOrigin(allowedHeaders = "*",origins = "*",maxAge = 3600,allowCredentials = "true")
@Controller
@RequestMapping("/user")
public class UserController {

    public static int DEFAULT_INVITE_TIMES = 3;
    public static String DEFAULT_INVITE_REWARD = "1|2|1";

    public static int DEFAULT_ADS_TIMES = 3;
    public static String DEFAULT_ADS_REWARD = "1|1|10";


    public static String KEY = "ca-app-pub-3419815942319599~3710501994";
    public static String BANNER = "ca-app-pub-3419815942319599/8997453851";
    public static String NATIVE = "ca-app-pub-3419815942319599/7492800498";
    public static String INTERSTITIAL = "ca-app-pub-3419815942319599/3745127173";
    public static String REVIEW = "ca-app-pub-3419815942319599/1118963831";
    public static String SPLASH = "";



    private static Logger logger = Loggers.webLogger;

    @Autowired
    private UserService userService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private LineService lineService;
    @Autowired
    private SystemService systemService;

    @PostConstruct
    public void init(){
        userService.init();
    }

    /**
     * 注册
     * @param password
     * @return
     *
     */
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String register(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password, @RequestParam(name = "inviteCode",required = false) String inviteCode) {
        logger.info("register,username:{},password:{},inviteCode:{}",username,password,inviteCode);
        JSONObject obj = new JSONObject();
        /*TUser userByUserName = userService.findUserByUserName(username);
        if(userByUserName!=null){
            logger.info("register fail,username has register,username:{},password:{}",username,password);
            obj.put("status", RespCode.HAS_USER);
            obj.put("msg","username has register");
            return obj.toString();
        }*/
        String register = userService.register(username, password);
        JSONObject jsonObject = JSONObject.parseObject(register);
        if(jsonObject.getIntValue("status")==1){
            if(inviteCode!=null && !"".equals(inviteCode)){
                TUser userByInviteCode = userService.findUserByInviteCode(inviteCode);
                int times = systemService.getData()==null?0:systemService.getData().getInviteTimes();
                if(times<=0){
                    times = DEFAULT_INVITE_TIMES;
                }
                String reward = systemService.getData()==null?"1|2|1":systemService.getData().getInviteReward();
                if(reward == null || "".equals(reward)){
                    reward = DEFAULT_INVITE_REWARD;
                }
                if(userByInviteCode!=null && userByInviteCode.canInvite(times)){
                    sendReward(userByInviteCode,reward);
                    userByInviteCode.addInviteTimes();
                    userService.saveOrUpdate(userByInviteCode);
                }
            }
        }
        return register;
    }


    /**
     * 看广告领奖
     * @param userId
     * @return
     *
     */
    @RequestMapping(value = "/adsReward",method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String adsReward(@RequestParam(name = "userId",required = false) long userId) {
        logger.info("adsReward,userId:{}",userId);
        JSONObject obj = new JSONObject();
        TUser userByUserId = userService.findUserByUserId(userId);
        if(userByUserId==null){
            logger.info("adsReward fail,");
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","adsReward error,userId is wrong");
            return obj.toString();
        }
        int times = systemService.getData()==null?0:systemService.getData().getAdsTimes();
        if(times<=0){
            times = DEFAULT_ADS_TIMES;
        }
        if(!userByUserId.canAds(times)){
            logger.info("adsReward fail,");
            obj.put("status",RespCode.ADS_LIMIT);
            obj.put("msg","adsReward error,time limit");
            return obj.toString();
        }
        String reward = systemService.getData()==null?"1|2|1":systemService.getData().getAdsReward();
        if(reward == null || "".equals(reward)){
            reward = DEFAULT_ADS_REWARD;
        }
        sendReward(userByUserId,reward);
        userByUserId.addAdsTimes();
        userService.saveOrUpdate(userByUserId);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","adsReward SUCESS");

        return obj.toString();
    }


    private void checkInviteCode(TUser user) {
        if(user.getInviteCode()==null || "".equals(user.getInviteCode())){
            String inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
            TUser userByInviteCode = userService.findUserByInviteCode(inviteCode);
            while (userByInviteCode!=null){
                inviteCode = StringUtils.createBigSmallLetterStrOrNumberRadom();
                userByInviteCode = userService.findUserByInviteCode(inviteCode);
            }
            user.setInviteCode(inviteCode);
            userService.saveOrUpdate(user);
        }
    }

    /**
     * 登陆
     * @param password
     * @return
     *
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String login(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password) {
        logger.info("login,username:{},password:{}",username,password);
        JSONObject obj = new JSONObject();
        if(isEmpty(username) || isEmpty(password)){
            logger.error("login err,username or password is empty,username:{},password:{}",username,password);
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","login err,username or password error");
            return obj.toString();
        }
        TUser user = userService.findUserByUserName(username);
        if(user==null){
            logger.error("login err,user is null,username:{},password:{}",username,password);
            obj.put("status",RespCode.USER_NOT_EXIST);
            obj.put("msg","login err,username or password error");
            return obj.toString();
        }
        if(!user.getPassword().equals(password)){
            logger.error("login err,password wrong ,username:{},password:{}",username,password);
            obj.put("status",RespCode.PWD_ERROR);
            obj.put("msg","login err,username or password error");
            return obj.toString();
        }
        user.setLastLoginTime(System.currentTimeMillis());
        checkUserVip(user);
        checkDailyData(user);
        checkInviteCode(user);
        userService.saveOrUpdate(user);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","login success");
        obj.put("user",user.toJson());
        obj.put("token",tokenService.getToken(user));
        return obj.toString();
    }

    private void checkDailyData(TUser user) {
        long beginOfDay = TimeUtils.getBeginOfDay(System.currentTimeMillis());
        if(user.getDailyVersion()<beginOfDay){
            user.setDailyVersion(beginOfDay);
            String paramData = user.getParamData();
            JSONObject jsonObject = null;
            if(paramData==null || "".equals(paramData)){
                jsonObject = new JSONObject();
            }else {
                jsonObject = JSONObject.parseObject(paramData);
            }
            jsonObject.put(ParamDataConst.ADS,0);
            jsonObject.put(ParamDataConst.INVITE,0);
            user.setParamData(jsonObject.toString());
            userService.saveOrUpdate(user);
        }
    }


    /**
     * 获取用户信息
     * @return
     *
     */
    @RequestMapping(value = "/getUserInfo",method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String getUserInfo(@RequestParam(name = "userId") long userId) {
        JSONObject obj = new JSONObject();
        TUser user = userService.findUserByUserId(userId);
        if(user==null){
            logger.error("login err,user is null,userId:{}",userId);
            obj.put("status",RespCode.USER_NOT_EXIST);
            obj.put("msg","login err,user is null");
            return obj.toString();
        }
        user.setLastLoginTime(System.currentTimeMillis());
        checkUserVip(user);
        userService.saveOrUpdate(user);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","login success");
        obj.put("user",user.toJson());
        obj.put("token",tokenService.getToken(user));
        return obj.toString();
    }


    private boolean checkUserVip(TUser user) {
        if(user.getVipLv()>0){
            if(System.currentTimeMillis()>=user.getVipEndTime()){
                user.setVipLv(0);
                user.setVipEndTime(System.currentTimeMillis());
                return true;
            }
        }
        return false;
    }

    public boolean isEmpty(String param){
        return param==null || "".equals(param.trim());
    }


    /**
     * 修改个人信息
     * @param user
     * @return
     */
    @RequestMapping(value = "/editUser",method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String editUser(TUser user) {
        JSONObject obj = new JSONObject();
        logger.info("editEmail:user{}",user.toJson());
        TUser userById = userService.findUserByUserId(user.getId());
        if(userById == null){
            logger.error("editUser err,user is null,user:{}",user.toJson());
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","editUser err, user is null");
            return obj.toString();
        }
        if(!isEmpty(user.getDeviceId())){
            userById.setDeviceId(user.getDeviceId());
        }
        if(!isEmpty(user.getEmail())){
            userById.setEmail(user.getEmail());
        }
        if(!isEmpty(user.getMobile())){
            userById.setMobile(user.getMobile());
        }
        if(!isEmpty(user.getNickname())){
            userById.setNickname(user.getNickname());
        }
        userService.saveOrUpdate(userById);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","edit success");
        obj.put("user",userById.toJson());
        return obj.toString();
    }


    /**
     * 改密码
     * @param
     * @return
     */
    @RequestMapping(value = "/editPassword",method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String editPassword(@Param("id") long userId,@Param("oldPaswword") String oldPaswword,@Param("newPassword") String newPassword) {
        JSONObject obj = new JSONObject();
        logger.info("editPassword:userId{},oldPaswword:{},newPassword:{}",userId,oldPaswword,newPassword);
        if(isEmpty(oldPaswword) || isEmpty(newPassword)){
            logger.error("editPassword err,password is null");
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","editPassword err, password is null");
            return obj.toString();
        }
        TUser userById = userService.findUserByUserId(userId);
        if(userById == null){
            logger.error("editPassword err,user is null,user:{}",userId);
            obj.put("status",RespCode.USER_NOT_EXIST);
            obj.put("msg","editPassword err, user is null");
            return obj.toString();
        }
        if(!userById.getPassword().equals(oldPaswword)){
            logger.error("editPassword err,oldPassword wrong,oldPaswword:{}",oldPaswword);
            obj.put("status",RespCode.PWD_ERROR);
            obj.put("msg","editPassword err, oldPassword wrong");
            return obj.toString();
        }
        userById.setPassword(newPassword);
        userService.saveOrUpdate(userById);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","edit success");
        obj.put("token",tokenService.getToken(userById));
        return obj.toString();
    }

    /**
     * 忘记密码，发邮件验证码
     * @param
     * @return
     */
    @RequestMapping(value = "/forgotPassword",method = RequestMethod.GET)
    @ResponseBody
    @PassToken
    public String forgotPassword(String userName){
        JSONObject obj = new JSONObject();
        if(isEmpty(userName)){
            logger.error("forgotPassword err,userName is null");
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","forgotPassword err,userName is null");
            return obj.toString();
        }
        TUser userByUserName = userService.findUserByUserName(userName);
        if(userByUserName == null){
            obj.put("status",RespCode.USER_NOT_EXIST);
            obj.put("msg","user is not exist");
            return obj.toString();
        }
        if(isEmpty(userByUserName.getEmail())){
            obj.put("status",RespCode.EMAIL_ERROR);
            obj.put("msg","user email is wrong");
            return obj.toString();
        }
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p = Pattern.compile(regEx1);
        Matcher m = p.matcher(userByUserName.getUsername());
        if(!m.matches()){
            obj.put("status",RespCode.EMAIL_ERROR);
            obj.put("msg","user email is wrong");
            return obj.toString();
        }

        try {
            List<InternetAddress> listReceive = new ArrayList<>();
            listReceive.add(new InternetAddress(userByUserName.getEmail(), "", "utf-8"));
            String format = String.format("%04d", new Random().nextInt(9999));
            MailUtil.sendMail("Change Password","Verification code：【"+format+"】,Effective within 5 minutes, please complete the password change as soon as possible",listReceive);
            redisService.addRedisAndTime(userName,format,5);
        } catch (Exception e) {
            e.printStackTrace();
            obj.put("status",RespCode.FAIL);
            obj.put("msg","sendmail error");
            return obj.toString();
        }
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","sendmail success");
        return obj.toString();
    }

    /**
     * 验证码改密码
     * @param
     * @return
     */
    @RequestMapping(value = "/editPwdByCode",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String editPwdByCode(String userName,String code,String newPassword){
        JSONObject obj = new JSONObject();
        if(isEmpty(userName) || isEmpty(code)|| isEmpty(newPassword)){
            logger.error("editPwdByCode err,password is null");
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","editPwdByCode err, password is null");
            return obj.toString();
        }
        String code1 = redisService.getCode(userName);
        if(code1==null || !code1.equals(code)){
            obj.put("status",RespCode.CODE_ERROR);
            obj.put("msg","code error or out time");
            return obj.toString();
        }
        TUser userByUserName = userService.findUserByUserName(userName);
        if(userByUserName == null){
            obj.put("status",RespCode.USER_NOT_EXIST);
            obj.put("msg","user is not exist");
            return obj.toString();
        }
        userByUserName.setPassword(newPassword);
        checkInviteCode(userByUserName);
        userService.saveOrUpdate(userByUserName);
        redisService.removeByKey(userName);
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","editPwdByCode success");
        obj.put("token",tokenService.getToken(userByUserName));
        return obj.toString();
    }


    /**
     * 获取线路详情
     * @param
     * @return
     */
    /**@RequestMapping(value = "/getLineById",method = RequestMethod.POST)
    @ResponseBody
    @UserLoginToken
    public String getLineById(@Param("id") long userId,@Param("lineId") long lineId) {
        JSONObject obj = new JSONObject();
        logger.info("getLineById:userId{},lineId:{},",userId,lineId);
        if(userId<=0 || lineId<0){
            logger.error("getLineById err,userId:{},lineId:{}",userId,lineId);
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","getLineById err, userId:{"+userId+"},lineId:{"+lineId+"}");
            return obj.toString();
        }
        TLine lineById = lineService.getLineById(lineId);
        if(lineById == null){
            logger.error("getLineById err,line is null,userId:{},lineId:{}",userId,lineId);
            obj.put("status",RespCode.LINE_NOT_EXIST);
            obj.put("msg","getLineById err, line is null,userId:{"+userId+"},lineId:{"+lineId+"}");
            return obj.toString();
        }
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","success");
        obj.put("code",EncryptDecrypt.encryptStringToHex(lineById.toJson()));
        return obj.toString();
    }*/

    @RequestMapping(value = "/getLineById",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String getLineById(@RequestParam(value = "userId",required = false) long userId, @RequestParam("lineId") long lineId) {
        logger.info("getLineById,userId:{},lineId:{}",userId,lineId);
        JSONObject obj = new JSONObject();
        logger.info("getLineById:lineId:{},",lineId);
        if(lineId<0){
            logger.error("getLineById err,lineId:{}",lineId);
            obj.put("status",RespCode.NO_PARAMS);
            obj.put("msg","getLineById err, lineId:{"+lineId+"}");
            return obj.toString();
        }
        TLine lineById = lineService.getLineById(lineId);
        if(lineById == null){
            logger.error("getLineById err,line is null,lineId:{}",lineId);
            obj.put("status",RespCode.LINE_NOT_EXIST);
            obj.put("msg","getLineById err, line is null,lineId:{"+lineId+"}");
            return obj.toString();
        }
        if(Integer.parseInt(lineById.getLv())==0){
            obj.put("status",RespCode.SUCCESS);
            obj.put("msg","success");
            obj.put("code", EncryptDecrypt.encryptStringToHex(lineById.toJson()));
            return obj.toString();
        }

        TUser player = userService.findUserByUserId(userId);

        if(player==null){
            logger.error("user is null,userId:{}",userId);
            obj.put("status",RespCode.NOT_LIMIT);
            obj.put("msg","You do not have permission to connect this line");
            return obj.toString();
        }

        checkInviteCode(player);
        if(checkUserVip(player)){
            userService.saveOrUpdate(player);
        }

        if(player.getVipLv()<Integer.parseInt(lineById.getLv())){
            logger.error("user has not permission,userId:{}",userId);
            obj.put("status",RespCode.NOT_LIMIT);
            obj.put("msg","You do not have permission to connect this line");
            return obj.toString();
        }

        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","success");
        obj.put("code",EncryptDecrypt.encryptStringToHex(lineById.toJson()));
        return obj.toString();
    }

    /**
     * 获取线路列表
     * @param
     * @return
     */
    @RequestMapping(value = "/getLines",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String getLines(HttpServletRequest httpServletRequest) {
        String versionCode = httpServletRequest.getHeader("versionCode");
        JSONObject obj = new JSONObject();
        String allLine = lineService.getAllLine(Integer.parseInt(versionCode));
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","success");
        obj.put("lines",allLine);
        return obj.toString();
    }
    /**
     * 获取版本号
     * @param
     * @return
     */
    @RequestMapping(value = "/getVersion",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String getVersion() {
        JSONObject obj = new JSONObject();
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","success");
        obj.put("data",systemService.getVersionData());
        return obj.toString();
    }
    /**
     * 获取分享链接
     * @param
     * @return
     */
    @RequestMapping(value = "/getShareUrl",method = RequestMethod.POST)
    @ResponseBody
    @PassToken
    public String getShareUrl() {
        JSONObject obj = new JSONObject();
        obj.put("status",RespCode.SUCCESS);
        obj.put("msg","success");
        TSystem data = systemService.getData();
        String url = "";
        if(data==null || data.getShareUrl()==null || "".equals(data.getShareUrl())){
            url = "https://play.google.com/store/apps/details?id=com.mars.vpn";
        }else{
            url = data.getShareUrl();
        }
        obj.put("shareUrl",url);
        return obj.toString();
    }

    /**
     * 隐私政策
     * @param
     * @return
     */
    @RequestMapping(value = "/yszc",method = RequestMethod.GET)
    @PassToken
    public String share() {
        return "yszc/yszc";
    }




    public String sendReward(TUser userByUserId,String reward){
        JSONObject result = new JSONObject();
        if(reward==null || "".equals(reward)){
            result.put("status",RespCode.NO_PARAMS);
            result.put("msg","reward is null");
            return result.toString();
        }
        String[] split = reward.split("\\|");
        if(userByUserId.getVipLv()>0 && userByUserId.getVipLv()!=Integer.parseInt(split[0])){
            logger.error("useCode error,user vipLV is limit,userVIP:{},rewardLv:{}",userByUserId.getVipLv(),Integer.parseInt(split[0]));
            result.put("status",RespCode.SHOP_ERROR);
            result.put("msg","user vipLV is limit");
            return result.toString();
        }
        long vipEndTime = userByUserId.getVipEndTime();
        long curTime = System.currentTimeMillis();

        long ttl = 0;
        String msg = "use seccess,Congratulations : VIP ";
        switch (Integer.parseInt(split[1])){
            case 1://分钟
                ttl = 60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Minute";
                break;
            case 2://小时
                ttl = 60*60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Hour";
                break;
            case 3://天
                ttl = 24*60*60*1000*Long.parseLong(split[2]);
                msg += Integer.parseInt(split[2])+" Day";
                break;
            default:break;
        }


        if(vipEndTime<=curTime){
            userByUserId.setVipEndTime(curTime+ttl);
        }else{
            userByUserId.setVipEndTime(vipEndTime+ttl);
        }
        userByUserId.setVipLv(Integer.parseInt(split[0]));
        result.put("status",RespCode.SUCCESS);
        result.put("msg",msg);
        return result.toString();
    }

    /**
     *  获取广告相关接口
     * @param
     * @return
     */
    @GetMapping(value = "/getAds")
    @ResponseBody
    @PassToken
    public String getAds() {
        JSONObject obj = new JSONObject();
        obj.put("key",KEY);
        obj.put("ad-banner",BANNER);
        obj.put("ad-native",NATIVE);
        obj.put("ad-interstitial",INTERSTITIAL);
        obj.put("ad-review",REVIEW);
        obj.put("ad-ad-splash",SPLASH);
        return obj.toString();
    }

}
