package com.mars.web.controller;

import com.mars.core.bean.TNotice;
import com.mars.core.service.NoticeService;
import com.mars.core.utils.Loggers;
import com.mars.web.interceptor.PassToken;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@CrossOrigin(allowedHeaders = "*",origins = "*",maxAge = 3600,allowCredentials = "true")
@Controller
@RequestMapping("/notice")
public class noticeController {

    private static Logger logger = Loggers.webLogger;

    @Autowired
    private NoticeService noticeService;

    @RequestMapping(value = "/getReleaseNotice", method = RequestMethod.GET)
    @ResponseBody
    @PassToken
    public String getReleaseNotice(){
        TNotice releaseNotice = noticeService.getReleaseNotice();
        if(releaseNotice!=null){
            return releaseNotice.toJson();
        }
        return "";
    }




}
