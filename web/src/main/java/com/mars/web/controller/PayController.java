package com.mars.web.controller;

import com.mars.web.interceptor.PassToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 */
@CrossOrigin(allowedHeaders = "*",origins = "*",maxAge = 3600,allowCredentials = "true")
@Controller
@RequestMapping("/")
public class PayController {
    /**
     * 支付
     * @param
     * @return
     */
    @RequestMapping(value = "/usdt.php",method = RequestMethod.GET)
    @PassToken
    public String usdt() {
        return "yszc/usdt";
    }
}
